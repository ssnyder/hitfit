#########################################################################
# Top-level.
#


zmass = 91190
bmass = 4600


#########################################################################
# Event_Constrainer parms.
#

use_e = true

# Center-of-mass energy.  Used to force a step cut
# if the fit goes into an unphysical region.
e_com = 14000000

# If this is true and the event does not have a neutrino,
# then the fit will be done without the overall transverse
# momentum constraint (and thus the missing Et information
# will be ignored).  If the event does have a neutrino, this parameter
# is ignored.
ignore_met = false


#########################################################################
# Constrained_Fitter parms.
#

# If true, print a trace of the fit to cout.
printfit = false

# If true, check the chisq formula by computing it directly from G.
# This requires that G_i be invertable.
use_G = false

# Convergence threshold for sum of constraints.
constraint_sum_eps = 0.01

# Convergence threshold for change in chisq.
chisq_diff_eps = 0.01

# Maximum number of iterations permitted.
maxit = 1000

# Maximum number of cut steps permitted.
maxcut = 20

# Fraction by which to cut steps.
cutsize = 0.5

# Smallest fractional cut step permitted.
min_tot_cutsize = 1e-10

# When use_G is true, the maximum relative difference permitted between
# the two chisq calculations.
chisq_test_eps = 1e-5


#########################################################################
# Base_Fitter parms.
#

# If true, check the constraint gradient calculations by also
# doing them numerically.
test_gradient = false

# When test_gradient is true, step size to use for numeric differentiation.
test_step = 0.002

# When test_gradient is true, maximum relative difference permitted
# between returned and numerically calculated gradients.
test_eps = 0.035
