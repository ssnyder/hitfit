//
// $Id: Run1_Jetcorr.hpp,v 1.1 2006-10-05 08:29:18 chriss Exp $
//
// File: hitfit/Run1_Jetcorr.hpp
// Purpose: Apply run 1 jet corrections.
// Created: Aug, 2000, sss, based on run 1 mass analysis code.
//


#ifndef HITFIT_RUN1_JETCORR_HPP
#define HITFIT_RUN1_JETCORR_HPP


#include "hitfit/Vector_Resolution.hpp"
#include <vector>


namespace hitfit {


class Lepjets_Event;
class Defaults;


class Run1_Jetcorr_Args
//
// Purpose: Hold on to parameters for the Run1_Jetcorr class.
//
//   float jet_mass_thresh-If a jet's mass exceeds this, rescale its
//                        3-momentum so that its mass is zero.
//                        This works around a cafix bug.
//   float lrespar0     - Frank's lepton resolution parameterization.
//   float lrespar1     -Frank's lepton resolution parameterization.
//   float lrespar2     -Frank's lepton resolution parameterization.
//   bool fh_scale_data_to_mc - Use scale_match.
//   bool fh_scale_data -Do scaling for data.
//   bool fh_scale_mc   -Do scaling for MC.
//   bool fh_scale_corr_met -Adjust missing Et when doing scaling.
//   float etadep_etaN  - Maximum eta for Nth eta-dependent jet resolution.
//   string etadep_resN - Nth eta-dependent jet resolution, as a string
//                       to Vector_Resolution.
//
{
public:
  // Constructor.  Initialize from a Defaults object.
  Run1_Jetcorr_Args (const Defaults& defs);

  // Retrieve the eta-dependent jet resolution for ETA.
  const Vector_Resolution& find_etadep_jetres (double eta) const;

  // Retrieve parameter values.
  double jet_mass_thresh () const;
  double lrespar0 () const;
  double lrespar1 () const;
  double lrespar2 () const;
  bool fh_scale_data_to_mc () const;
  bool fh_scale_data () const;
  bool fh_scale_mc () const;
  bool fh_scale_corr_met () const;

private:
  // Vector of eta-dependent jet resolutions.
  struct Etadep_Jetres
  {
    double etamax;
    Vector_Resolution res;
    Etadep_Jetres (double eta, std::string s) : etamax (eta), res (s) {}
  }; 
  std::vector<Etadep_Jetres> m_etadep_jetres;

  // Hold on to parameter values.
  double m_jet_mass_thresh;
  double m_lrespar0;
  double m_lrespar1;
  double m_lrespar2;
  bool m_fh_scale_data_to_mc;
  bool m_fh_scale_data;
  bool m_fh_scale_mc;
  bool m_fh_scale_corr_met;
};


// Apply run 1 jet corrections to EV.
void run1_jetcorr (const Run1_Jetcorr_Args& args,
                   Lepjets_Event& ev);


} // namespace hitfit


#endif // not HITFIT_RUN1_JETCORR

