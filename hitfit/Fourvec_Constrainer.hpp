//
// $Id: Fourvec_Constrainer.hpp,v 1.1 2006-10-05 08:29:18 chriss Exp $
//
// File: hitfit/Fourvec_Constrainer.hpp
// Purpose: Do a kinematic fit for a set of 4-vectors, given a set
//          of mass constraints.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//
// Do a constrained fit on a set of 4-vectors.
// The input is in a Fourvec_Event instance.  This consists
// of a collection of objects, each of which has a 4-momentum
// and an integer label.  (Also uncertainties, etc.)
//
// We also have a set of mass constraints, based on sums of objects
// with specified labels.  A constraint can either require that the
// invariant mass of a set of objects is constant, or that the masses
// of two sets be equal to each other.  For example, the constraint
//
//    (1 2) = 80
//
// says that the sum of all objects with labels 1 and 2 should have
// a mass of 80.  And the constraint
//
//    (1 2) = (3 4)
//
// says that the sum of all objects with labels 1 and 2 should
// have an invariant mass the same as the sum of all objects with
// labels 3 and 4.
//
// All the objects are fixed to constant masses for the fit.
// (These masses are attributes of the objects in the Fourvec_Event.)
// This is done by scaling either the 4-vector's 3-momentum or energy,
// depending on the setting of the `use_e' parameter.
//
// If there is no neutrino present in the event, two additional constraints
// are automatically added for total pt balance, unless the parameter
// ignore_met has been set.
//
// When the fit completes, this object can compute an invariant mass
// out of some combination of the objects, including an uncertainty.
// The particular combination is specified through the method mass_contraint();
// it gets a constraint string like normal; the lhs of the constraint
// is the mass that will be calculated.  (The rhs should be zero.)
//


#ifndef HITFIT_FOURVEC_CONSTRAINER_HPP
#define HITFIT_FOURVEC_CONSTRAINER_HPP


#include <string>
#include <vector>
#include <iosfwd>
#include "hitfit/private/Constraint.hpp"
#include "hitfit/Chisq_Constrainer.hpp"
#include "hitfit/matutil.hpp"


namespace hitfit {


class Defaults;
class Fourvec_Event;
class Pair_Table;


class Fourvec_Constrainer_Args
//
// Purpose: Hold on to parameters for the Fourvec_Constrainer class.
//
// Parameters controlling the operation of the fitter:
//   bool use_e         - If true, then when rescaling the 4-vectors
//                        for a given mass, keep the measured energy
//                        and scale the 3-momentum.  Otherwise, keep
//                        the 3-momentum and scale the energy.
//   float e_com        - The center-of-mass energy.
//                        (Used only to keep the fit from venturing
//                        into completely unphysical regions.)
//   bool ignore_met    - If this is true and the event does not
//                        have a neutrino, then the fit will be done
//                        without the overall transverse momentum
//                        constraint (and thus the missing Et information
//                        will be ignored).  If the event does have
//                        a neutrino, this parameter is ignored.
//
{
public:
  // Constructor.  Initialize from a Defaults object.
  Fourvec_Constrainer_Args (const Defaults& defs);

  // Retrieve parameter values.
  bool use_e () const;
  double e_com () const;
  bool ignore_met () const;


  // Arguments for subobjects.
  const Chisq_Constrainer_Args& chisq_constrainer_args () const;


private:
  // Hold on to parameter values.
  bool m_use_e;
  double m_e_com;
  bool m_ignore_met;

  Chisq_Constrainer_Args m_chisq_constrainer_args;
};


//*************************************************************************


class Fourvec_Constrainer
//
// Purpose: Do a kinematic fit for a set of 4-vectors, given a set
//          of mass constraints.
//
{
public:
  // Constructor.
  // ARGS holds the parameter settings for this instance.
  Fourvec_Constrainer (const Fourvec_Constrainer_Args& args);

  // Specify an additional constraint S for the problem.
  // The format for S is described above.
  void add_constraint (std::string s);

  // Specify the combination of objects that will be returned by
  // constrain() as the mass.  The format of S is the same as for
  // normal constraints.  The LHS specifies the mass to calculate;
  // the RHS should be zero.
  // This should only be called once.
  void mass_constraint (std::string s);

  // Do a constrained fit for EV.  Returns the requested mass and
  // its error in M and SIGM, and the pull quantities in PULLX and
  // PULLY.  Returns the chisq; this will be < 0 if the fit failed
  // to converge.
  double constrain (Fourvec_Event& ev,
                    double& m,
                    double& sigm,
                    Column_Vector& pullx,
                    Column_Vector& pully);

  // Alternate version for call from pypy.
  double constrain1 (Fourvec_Event& ev,
                     double* m,
                     double* sigm);

  // Dump the internal state.
  friend std::ostream& operator<< (std::ostream& s,
                                   const Fourvec_Constrainer& c);
  std::ostream& dump (std::ostream& s) const;


private:
#ifndef __REFLEX__
  // Parameter settings.
  const Fourvec_Constrainer_Args& m_args;
#endif
  Fourvec_Constrainer& operator= (const Fourvec_Constrainer&);

  // The constraints for this problem.
  std::vector<Constraint> m_constraints;

  // The constraint giving the mass to be calculated.  This
  // should have no more than one entry.
  std::vector<Constraint> m_mass_constraint;
};


} // namespace hitfit


#endif // not HITFIT_FOURVEC_CONSTRAINER_HPP
