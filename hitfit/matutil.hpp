//
// $Id: matutil.hpp,v 1.1 2006-10-05 08:29:19 chriss Exp $
//
// File: hitfit/matutil.hpp
// Purpose: Define matrix types for the hitfit package, and supply a few
//          additional operations.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//
// This file defines the types `Matrix', `Column_Vector', `Row_Vector',
// and `Diagonal_Matrix', to be used in hitfit code.  These are based
// on the corresponding CLHEP matrix classes (except that we need
// to create our own row vector class, since CLHEP doesn't have that
// concept).  We also provide a handful of operations that are missing
// in CLHEP.
//


#ifndef HITFIT_MATUTIL_HPP
#define HITFIT_MATUTIL_HPP

// We want bounds checking.
#define MATRIX_BOUND_CHECK

//#include "CLHEP/config/CLHEP.h"
#include "CLHEP/Matrix/Matrix.h"
#include "CLHEP/Matrix/Vector.h"
#include "CLHEP/Matrix/DiagMatrix.h"


namespace hitfit {


// We use these CLHEP classes as-is.
typedef CLHEP::HepMatrix Matrix;
typedef CLHEP::HepVector Column_Vector;
typedef CLHEP::HepDiagMatrix Diagonal_Matrix;


// CLHEP doesn't have a row-vector class, so make our own.
// This is only a simple wrapper around Matrix that tries to constrain
// the shape to a row vector.  It will raise an assertion if you try
// to assign to it something that isn't a row vector.
class Row_Vector
  : public Matrix
//
// Purpose: Simple Row_Vector wrapper for CLHEP matrix class.
//
{
public:
   // Constructor.  Gives an uninitialized 1 x cols matrix.
  explicit Row_Vector (int cols);

   // Constructor.  Gives a 1 x cols matrix, initialized to zero.
   // The INIT argument is just a dummy; give it a value of 0.
  explicit Row_Vector (int cols, int init);

  // Copy constructor.  Will raise an assertion if M doesn't have
  // exactly one row.
  Row_Vector (const Matrix& m);

  // Element access.
  // ** Note that the indexing starts from (1). **
  const double & operator() (int col) const;
  double & operator() (int col);

  // Element access.
  // ** Note that the indexing starts from (1,1). **
  const double & operator()(int row, int col) const;
  double & operator()(int row, int col);

  // Assignment.  Will raise an assertion if M doesn't have
  // exactly one row.
  Row_Vector& operator= (const Matrix& m);
};


// Additional operations.


// Reset all elements of a matrix to 0.
void clear (CLHEP::HepGenMatrix& m);

// Check that M has dimensions 1x1.  If so, return the single element
// as a scalar.  Otherwise, raise an assertion failure.
double scalar (const CLHEP::HepGenMatrix& m);


} // namespace hitfit



#endif // not HITFIT_MATUTIL_HPP

