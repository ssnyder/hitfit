//
// $Id: Lepjets_Event_Jet.hpp,v 1.1 2006-10-05 08:29:18 chriss Exp $
//
// File: hitfit/Lepjets_Event_Jet.hpp
// Purpose: Represent a `jet' in a Lepjets_Event.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//
// This is like Lepjets_Event_Jet, except that we store some
// additional information:
//
//   - svx tag flag
//   - slt tag flag
//   -   slt lepton 4-vector
//   -   slt lepton energy deposition
//


#ifndef HITFIT_LEPJETS_EVENT_JET_HPP
#define HITFIT_LEPJETS_EVENT_JET_HPP
//#include "CLHEP/Random/RandomEngine.h"

#include "hitfit/fourvec.hpp"
#include "hitfit/Vector_Resolution.hpp"
#include "hitfit/Lepjets_Event_Lep.hpp"
#include <iosfwd>



namespace hitfit {


enum Jet_Labels {
  isr_label = 0,
  lepb_label = 11,
  hadb_label = 12,
  hadw1_label = 13,
  hadw2_label = 14,
  higgs_label = 15,
  unknown_label = 20
};


class Lepjets_Event_Jet
  : public Lepjets_Event_Lep
//
// Purpose: Represent a `jet' in a Lepjets_Event.
//
{
public:
  // Constructor.
  Lepjets_Event_Jet (const Fourvec& p,
                     int type,
                     const Vector_Resolution& res,
                     bool svx_tag = false,
                     bool slt_tag = false,
                     const Fourvec& tag_lep = Fourvec(),
                     double slt_edep = 0);

  // Access the svx tag flag.
  bool svx_tag () const;
  bool& svx_tag ();

  // Access the slt tag flag.
  bool slt_tag () const;
  bool& slt_tag ();

  // Access the tag lepton 4-vector.
  Fourvec& tag_lep ();
  const Fourvec& tag_lep () const;

  // Access the tag lepton energy deposition.
  double slt_edep () const;
  double& slt_edep ();

  // Access the uncorrected jet energy.
  double e0 () const;
  double& e0 ();

  // Print this object.
  std::ostream& dump (std::ostream& s, bool full = false) const;


private:
  // The object state.
  bool m_svx_tag;
  bool m_slt_tag;
  Fourvec m_tag_lep;
  double m_slt_edep;
  double m_e0;
};


// Print this object.
std::ostream& operator<< (std::ostream& s, const Lepjets_Event_Jet& ev);


} // namespace hitfit


#endif // not HITFIT_LEPJETS_EVENT_JET_HPP

