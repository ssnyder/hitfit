#ifndef MN_hitfit_FCN_H_
#define MN_hitfit_FCN_H_
#include "Minuit/FCNBase.h"
#include "hitfit/Lepjets_Event.h"

class hitfit_FCN : public FCNBase {
public:
  hitfit_FCN(Lepjets_Event event):_event(event) {}  
  ~HitFIT_FCN() {}
  //  virtual double up() const {return theErrorDef;}
  //virtual double operator()(const std::vector<double>&) const;
  Lepjets_Event  get_event_to_fit() {return _event;}
private:
  Lepjets_Event _event;
};
#endif //MN_hitfit_FCN_H_
