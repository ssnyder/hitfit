//
// $Id: Defaults_Text.hpp,v 1.1 2006-10-05 08:29:18 chriss Exp $
//
// File: hitfit/Defaults_Text.hpp
// Purpose: A lightweight implementation of the Defaults interface
//          that uses simple text files.
// Created: Jul, 2000, sss.
//
// Create instances of these objects passing in the name of a file.
// Each line of the file should contain a parameter setting like
//
//   NAME = VALUE
//
// Anything following a `;' or `#' is stripped off; leading and trailing
// spaces on VALUE are also removed.  Blank lines are ignored.
//
// You can also pass an argument list to the constructor.  After the
// defaults file is read, the argument list will be scanned, to possibly
// override some of the parameter settings.  An argument of the form
//
//   --NAME=VALUE
//
// is equivalent to the parameter setting
//
//     NAME=VALUE
//
// while
//
//   --NAME
//
// is equivalent to
//
//   NAME=1
//
// and
//
//   --noNAME
//
// is equivalent to
//
//   NAME=0
//

#ifndef HITFIT_DEFAULTS_TEXT_HPP
#define HITFIT_DEFAULTS_TEXT_HPP

#include <string>
#include <iosfwd>
#include "hitfit/Defaults.hpp"


namespace hitfit {


class Defaults_Textrep;


class Defaults_Text
  : public Defaults
//
// Purpose: A lightweight implementation of the Defaults interface
//          that uses simple text files.
//
{
public:
  // Constructor, destructor.
  Defaults_Text (std::string def_file);
  Defaults_Text (std::string def_file, int argc, char** argv);
  ~Defaults_Text ();

  // Test to see if parameter NAME exists.
  virtual bool exists (std::string name) const;

  // Get the value of NAME as an integer.
  virtual int get_int (std::string name) const;

  // Get the value of NAME as a boolean.
  virtual bool get_bool (std::string name) const;

  // Get the value of NAME as a float.
  virtual double get_float (std::string name) const;

  // Get the value of NAME as a string.
  virtual std::string get_string (std::string name) const;

  // Dump out all parameters.
  friend std::ostream& operator<< (std::ostream& s, const Defaults_Text& def);

private:
  // The internal representation.
  Defaults_Textrep* m_rep;
};


} // namespace hitfit


#endif // not HITFIT_DEFAULTS_TEXT_HPP
