//
// $Id: Objpair.ipp,v 1.1 2006-10-05 08:29:19 chriss Exp $
//
// File: hitfit/private/Objpair.ipp
// Purpose: Helper class for Pair_Table.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//


#include <cassert>


namespace hitfit {


inline
int Objpair::i () const
//
// Purpose: Return the first object index for this pair.
//
// Returns:
//   The first object index for this pair.
//
{
  return m_i;
}


inline
int Objpair::j () const
//
// Purpose: Return the second object index for this pair.
//
// Returns:
//   The second object index for this pair.
//
{
  return m_j;
}


inline
int Objpair::for_constraint (int k) const
//
// Purpose: Retrieve the value set for constraint K.
//
// Inputs:
//   k -           The constraint number (0-based).
//
// Returns:
//   The value for constraint K.
//
{
  assert (k >= 0 && (size_t)k < m_for_constraint.size());
  return m_for_constraint[k];
}


} // namespace hitfit
