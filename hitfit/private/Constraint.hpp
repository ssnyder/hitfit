//
// $Id: Constraint.hpp,v 1.1 2006-10-05 08:29:19 chriss Exp $
//
// File: hitfit/private/Constraint.hpp
// Purpose: Represent a mass constraint equation.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//
// This class represents a mass constraint equation.
// Mass constraints come in two varieties, either saying that the sum
// of a set of labels should equal a constant:
//
//     (1 + 2) = 80
//
// or that two such sums should equal each other:
//
//     (1 + 2) = (3 + 4)
//
// We represent such a constraint equation by two Constraint_Intermed
// instances, each of which represents one side of the equation.
//

#ifndef HITFIT_CONSTRAINT_HPP
#define HITFIT_CONSTRAINT_HPP


#include <memory>
#include <string>
#include "hitfit/private/Constraint_Intermed.hpp"


namespace hitfit {


class Fourvec_Event;


class Constraint
//
// Purpose: Represent a mass constraint equation.
//
{
public:
  // Constructor, destructor.  S is the string to parse describing
  // the constraint.
  Constraint (std::string s);
  Constraint (const Constraint& c);
  ~Constraint () {}

  // Assignment.
  Constraint& operator= (const Constraint&);

  // See if this guy references both labels ILABEL and JLABEL
  // on a single side of the constraint equation.
  int has_labels (int ilabel, int jlabel) const;

  // Evaluate the mass constraint, using the data in EV.
  // Return m(lhs)^2/2 - m(rhs)^2/2.
  double sum_mass_terms (const Fourvec_Event& ev) const;

  // Print this object.
  friend std::ostream& operator<< (std::ostream& s, const Constraint& c);


private:
  // The two sides of the constraint.
  std::auto_ptr<Constraint_Intermed> m_lhs;
  std::auto_ptr<Constraint_Intermed> m_rhs;
};


} // namespace hitfit


#endif // not HITFIT_CONSTRAINT_HPP

