//
// $Id: Refcount.ipp,v 1.1 2006-10-05 08:29:19 chriss Exp $
//
// File: private/Refcount.ipp
// Purpose: Inline implementations for Refcount.
// Created: Aug 2000, sss, from the version that used to be in d0om.
//
#include <assert.h>

namespace hitfit {


inline
Refcount::Refcount ()
//
// Purpose: Constructor.  Initializes the reference count to 0.
//
  : m_refcount (0)
{
}


inline
Refcount::~Refcount ()
//
// Purpose: Destructor.  It is an error to try to delete the object if the
//          refcount is not 0.
{
  assert (m_refcount == 0);
}


inline
void Refcount::incref () const
//
// Purpose: Increment the refcount.
// 
{
  ++m_refcount;
}


inline
void Refcount::decref () const
//
// Purpose: Decrement the refcount; delete the object when it reaches zero.
//
{
  // Actually, we do the test first, to avoid tripping the assertion
  // in the destructor.
  if (m_refcount == 1) {
    m_refcount = 0;
    delete this;
  }
  else {
    assert (m_refcount > 0);
    --m_refcount;
  }
}


inline
bool Refcount::decref_will_delete () const
//
// Purpose: Will the next decref() delete the object?
//
// Returns:
//   True if the next decref() will delete the object.
//
{
  return (m_refcount == 1);
}


inline
bool Refcount::unowned () const
//
// Purpose: True if incref() has never been called, or if the object
//          is being deleted.
//
// Returns:
//   True if incref() has never been called, or if the object
//   is being deleted.
//
{
  return (m_refcount == 0);
}


} 
