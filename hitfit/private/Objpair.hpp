//
// $Id: Objpair.hpp,v 1.1 2006-10-05 08:29:19 chriss Exp $
//
// File: hitfit/private/Objpair.hpp
// Purpose: Helper class for Pair_Table.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//
// An `Objpair' consists of two object indices in a Fourvec_Event,
// plus a value for each constraint.  This value is +1 if these two objects
// are used on the lhs of that constraint, -1 if they are used on the rhs
// of that constraint, and 0 if they are not used by that constraint.
//


#ifndef HITFIT_OBJPAIR_HPP
#define HITFIT_OBJPAIR_HPP


#include <vector>
#include <iosfwd>


namespace hitfit {


class Objpair
//
// Purpose: Helper class for Pair_Table.
//
{
public:
  // Constructor.  I and J are the two object indices, and NCONSTRAINTS
  // is the number of constraints in the problem.
  Objpair (int i, int j, int nconstraints);

  // Set the value for constraint K (0-based) to VAL.
  void has_constraint (int k, int val);

  // Get back the indices in this pair.
  int i () const;
  int j () const;

  // Retrieve the value set for constraint K.
  int for_constraint (int k) const;

  // Print this object.
  friend std::ostream& operator<< (std::ostream& s, const Objpair& o);


private:
  // The object indices for this pair.
  int m_i;
  int m_j;

  // The list of values for each constraint.
  std::vector<signed char> m_for_constraint;
};


} // namespace hitfit


#include "hitfit/private/Objpair.ipp"


#endif // not HITFIT_OBJPAIR_HPP
