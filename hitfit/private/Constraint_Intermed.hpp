//
// $Id: Constraint_Intermed.hpp,v 1.1 2006-10-05 08:29:19 chriss Exp $
//
// File: hitfit/private/Constraint_Intermed.hpp
// Purpose: Represent one side of a mass constraint equation.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//
// Mass constraints come in two varieties, either saying that the sum
// of a set of labels should equal a constant:
//
//     (1 + 2) = 80
//
// or that two such sums should equal each other:
//
//     (1 + 2) = (3 + 4)
//
// These classes represent one side of such an equation.
// There is an abstract base class Constraint_Intermed, and then concrete
// implementations for the two cases, Constraint_Intermed_Constant
// and Constraint_Intermed_Labels.  There is also a free function
// make_constraint_intermed() to parse a string representing one
// side of a constraint and return the appropriate Constraint_Intermed
// instance.
//

#ifndef HITFIT_CONSTRAINT_INTERMED_HPP
#define HITFIT_CONSTRAINT_INTERMED_HPP


#include <iosfwd>
#include <vector>
#include <string>
#include <memory>


namespace hitfit {


class Fourvec_Event;


//************************************************************************


class Constraint_Intermed
//
// Purpose: Abstract base class for describing one side of a mass constraint.
//
{
public:
  // Constructor, destructor.
  Constraint_Intermed () {}
  virtual ~Constraint_Intermed () = default;

  // Return true if this guy references both labels ILABEL and JLABEL.
  virtual bool has_labels (int ilabel, int jlabel) const = 0;

  // Evaluate this half of the mass constraint, using the data in EV.
  // Return m^2/2.
  virtual double sum_mass_terms (const Fourvec_Event& ev) const = 0;

  // Print out this object.
  virtual void print (std::ostream& s) const = 0;

  // Copy this object.
  virtual std::auto_ptr<Constraint_Intermed> clone () const = 0;
};


//************************************************************************


class Constraint_Intermed_Constant
  : public Constraint_Intermed
//
// Purpose: Concrete base class for a constant mass constraint half.
//
{
public:
  // Constructor, destructor.
  Constraint_Intermed_Constant (double constant);
  virtual ~Constraint_Intermed_Constant () {};
  
  // Copy constructor.
  Constraint_Intermed_Constant (const Constraint_Intermed_Constant& c);

  // Return true if this guy references both labels ILABEL and JLABEL.
  virtual bool has_labels (int ilabel, int jlabel) const;

  // Evaluate this half of the mass constraint, using the data in EV.
  // Return m^2/2.
  virtual double sum_mass_terms (const Fourvec_Event& ev) const;

  // Print out this object.
  virtual void print (std::ostream& s) const;

  // Copy this object.
  virtual std::auto_ptr<Constraint_Intermed> clone () const;

private:
  // Store c^2 / 2.
  double m_c2;
};


//************************************************************************


class Constraint_Intermed_Labels
  : public Constraint_Intermed
{
public:
  Constraint_Intermed_Labels (const std::vector<int>& labels);
  Constraint_Intermed_Labels (const Constraint_Intermed_Labels& c);
  virtual ~Constraint_Intermed_Labels () {};

  // Return true if this guy references both labels ILABEL and JLABEL.
  virtual bool has_labels (int ilabel, int jlabel) const;

  // Evaluate this half of the mass constraint, using the data in EV.
  // Return m^2/2.
  virtual double sum_mass_terms (const Fourvec_Event& ev) const;

  // Print out this object.
  virtual void print (std::ostream& s) const;

  // Copy this object.
  virtual std::auto_ptr<Constraint_Intermed> clone () const;

private:
  // Test to see if LABEL is used by this constraint half.
  bool has_label (int label) const;

  // List of the labels for this constraint half, kept in sorted order.
  std::vector<int> m_labels;

  // Disallow assignment
  Constraint_Intermed& operator= (const Constraint_Intermed&);
};



//************************************************************************


// Print out a Constraint_Intermed object.
std::ostream& operator<< (std::ostream& s, const Constraint_Intermed& ci);


// Parse the string S and construct the appropriate Constraint_Intermed
// instance.
std::auto_ptr<Constraint_Intermed> make_constraint_intermed (std::string s);


} // namespace hitfit


#endif // not HITFIT_CONSTRAINT_INTERMED_HPP
