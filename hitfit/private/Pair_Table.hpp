//
// $Id: Pair_Table.hpp,v 1.1 2006-10-05 08:29:19 chriss Exp $
//
// File: hitfit/private/Pair_Table.hpp
// Purpose: Helper for Fourvec_Constrainer.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//
// Build a lookup table to speed up constraint evaluation.
//
// We have a set of constraints, which reference labels, like
//
//    (1 2) = 80
//    (1 2) = (3 4)
//
// We also have a Fourvec_Event, which has a set of objects, each of which
// has a label.  A label may correspond to multiple objects.
//
// We'll be evaluating the mass constraints by considering each
// pair of objects o_1 o_2 and finding its contribution to each
// constraint.  (We get pairs because the constraints are quadratic
// in the objects.)
//
// We build a Pair_Table by calling the constructor, giving it the event
// and the set of constraints.  We can then get back from it a list
// of Objpair's, each representing a pair of objects that are
// used in some constraint.  The Objpair will be able to tell us
// in which constraints the pair is used (and on which side of the
// equation).
//


#ifndef HITFIT_PAIR_TABLE_HPP
#define HITFIT_PAIR_TABLE_HPP

#include <vector>
#include <iosfwd>
#include "hitfit/private/Constraint.hpp"
#include "hitfit/private/Objpair.hpp"


namespace hitfit {


class Fourvec_Event;


class Pair_Table
//
// Purpose: Helper for Fourvec_Constrainer.
//
{
public:
  // Constructor.  Give it the event and the list of constraints.
  Pair_Table (const std::vector<Constraint>& cv,
              const Fourvec_Event& ev);

  // The number of pairs in the table.
  int npairs () const;

  // Get one of the pairs from the table.
  const Objpair& get_pair (int pairno) const;


private:
  //The table of pairs.
  std::vector<Objpair> m_pairs;
};


// Dump out the table.
std::ostream& operator<< (std::ostream& s, const Pair_Table& p);


} // namespace hitfit


#endif // not PAIR_TABLE_HPP
