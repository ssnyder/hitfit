//
// $Id: Lepjets_Event_Lep.hpp,v 1.1 2006-10-05 08:29:18 chriss Exp $
//
// File: hitfit/Lepjets_Event_Lep.hpp
// Purpose: Represent a `lepton' in a Lepjets_Event.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//
// For each lepton, we store:
//
//   - 4-momentum
//   - type code
//   - Vector_Resolution
//


#ifndef HITFIT_LEPJETS_EVENT_LEP_HPP
#define HITFIT_LEPJETS_EVENT_LEP_HPP


#include "hitfit/fourvec.hpp"
#include "hitfit/Vector_Resolution.hpp"
#include <iosfwd>
#include "CLHEP/Random/RandomEngine.h"


namespace hitfit {


enum Lepton_Labels {
  lepton_label = 1,  // generic lepton
  electron_label = 2,
  muon_label = 3
};


class Lepjets_Event_Lep
//
// Purpose: Represent a `lepton' in a Lepjets_Event.
//
{
public:
  // Constructor.
  Lepjets_Event_Lep (const Fourvec& p,
                     int type,
                     const Vector_Resolution& res);

  // Access the 4-momentum.
  Fourvec& p ();
  const Fourvec& p () const;

  // Access the type code.
  int& type ();
  int type () const;

  // Access the resolution.
  const Vector_Resolution& res () const;
  Vector_Resolution& res ();

  // Return resolutions for this object.
  double p_sigma () const;
  double eta_sigma () const;
  double phi_sigma () const;

  // Smear this object.
  // If SMEAR_DIR is false, smear the momentum only.
  void smear (CLHEP::HepRandomEngine& engine, bool smear_dir = false);

  // Dump out this object.
  std::ostream& dump (std::ostream& s, bool full = false) const;

  // Sort on pt.
  bool operator< (const Lepjets_Event_Lep& x) const;


private:
  // The object state.
  Fourvec m_p;
  int m_type;
  Vector_Resolution m_res;
};


// Print the object.
std::ostream& operator<< (std::ostream& s, const Lepjets_Event_Lep& ev);


} // namespace hitfit


#endif // not HITFIT_LEPJETS_EVENT_LEP_HPP

