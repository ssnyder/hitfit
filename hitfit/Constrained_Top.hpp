//
// $Id: Constrained_Top.hpp,v 1.1 2006-10-05 08:29:18 chriss Exp $
//
// File: hitfit/Constrained_Top.hpp
// Purpose: Do kinematic fitting for a ttbar -> ljets event.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//


#ifndef HITFIT_CONSTRAINED_TOP_HPP
#define HITFIT_CONSTRAINED_TOP_HPP


#include "hitfit/Fourvec_Constrainer.hpp"
#include "hitfit/matutil.hpp"
#include <iosfwd>


namespace hitfit {


class Defaults;
class Lepjets_Event;


class Constrained_Top_Args
//
// Purpose: Hold on to parameters for the Constrained_Top class.
//
// Parameters controlling the operation of the fitter:
//   float bmass        - The mass to which b jets should be fixed.
//
{
public:
  // Constructor.  Initialize from a Defaults object.
  Constrained_Top_Args (const Defaults& defs);

  // Retrieve parameter values.
  double bmass () const;

  // Arguments for subobjects.
  const Fourvec_Constrainer_Args& fourvec_constrainer_args () const;

private:
  // Hold on to parameter values.
  double m_bmass;

  Fourvec_Constrainer_Args m_fourvec_constrainer_args;
};


//*************************************************************************


class Constrained_Top
//
// Purpose: Do kinematic fitting for a ttbar -> ljets event.
//
{
public:
  // Constructor.
  // LEPW_MASS, HADW_MASS, and TOP_MASS are the masses to which
  // those objects should be constrained.  To remove a constraint,
  // set the mass to 0.
  Constrained_Top (const Constrained_Top_Args& args,
                   double lepw_mass,
                   double hadw_mass,
                   double top_mass);

  // Do a constrained fit.
  double constrain (Lepjets_Event& ev,
                    double& mt,
                    double& sigmt,
                    Column_Vector& pullx,
                    Column_Vector& pully);

  // Dump out our state.
  friend std::ostream& operator<< (std::ostream& s, const Constrained_Top& ct);


private:
  // Parameter settings.
  const Constrained_Top_Args& m_args;

  // The guy that actually does the work.
  Fourvec_Constrainer m_constrainer;
};


} // namespace hitfit


#endif // not HITFIT_CONSTRAINED_TOP_HPP
