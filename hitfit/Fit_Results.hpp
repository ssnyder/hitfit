//
// $Id: Fit_Results.hpp,v 1.2 2007-07-03 16:17:24 erik Exp $
//
// File: hitfit/Fit_Results.hpp
// Purpose: Hold the results from kinematic fitting.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//
// This is a set of lists of fit results.
// Each list corresponds to some subset of jet permutations:
//   all permutations, btagged permutations, etc.
//


#ifndef HITFIT_FIT_RESULTS_HPP
#define HITFIT_FIT_RESULTS_HPP


#include "hitfit/Fit_Result_Vec.hpp"
#include "hitfit/matutil.hpp"
#include <vector>
#include <iosfwd>


namespace hitfit {


class Lepjets_Event;


class Fit_Results
//
// Purpose: Hold the results from kinematic fitting.
//
{
public:
  // Constructor.  Make N_LISTS lists, each of maximum length MAX_LEN.
  Fit_Results (int max_len, int n_lists);

  // Return the Ith list.
  const Fit_Result_Vec& operator[] (int i) const;

  // Add a new result.  LIST_FLAGS tells on which lists to enter it.
  void push (double chisq,
             const Lepjets_Event& ev,
             const Column_Vector& pullx,
             const Column_Vector& pully,
             double umwhad,
             double utmass,
             double mt,
             double sigmt,
             const Column_Vector& pars,
             const Column_Vector& errs,
             const Column_Vector& steps,
             const Column_Vector& chiterms,
             const std::vector<int>& list_flags);

  // Print this object.
  friend std::ostream& operator<< (std::ostream& s, const Fit_Results& res);


private:
  // The object state.
  std::vector<Fit_Result_Vec> m_v;
};


} // namespace hitfit


#endif // not HITFIT_FIT_RESULT_HPP
