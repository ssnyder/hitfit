//
// $Id: Resolution.hpp,v 1.1 2006-10-05 08:29:18 chriss Exp $
//
// File: hitfit/Resolution.hpp
// Purpose: Calculate resolutions for a quantity.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//
// This object will calculate resolutions for some quantity.
// We have three parameters:
//
//   C - constant term
//   R - resolution term
//   N - noise term
//
// Given a `momentum' p, we calculate the uncertainty in a quantity x as
//
//   sigma(x) = sqrt (C^2 p^2 + R^2 p + N^2)
//
// In addition, we have an `inverse' flag.  If that is set,
// we take the inverse of p before doing the above calculation
// (and `sigma(x)' is regarded as actually sigma(1/x)).
//
// We encode the resolution parameters into a string, from which these
// objects get initialized.  The format is
//
//    [-]C[,R[,N]]
//
// If a leading minus sign is present, that turns on the invert flag.
// Omitted parameters are set to 0.
//


#ifndef HITFIT_RESOLUTION_HPP
#define HITFIT_RESOLUTION_HPP


#include <string>
#include <iosfwd>
#include "CLHEP/Random/RandomEngine.h"



namespace hitfit {


class Resolution
//
// Purpose: Calculate resolutions for a quantity.
//
{
public:
  // Initialize from a string S.  The format is as described above.
  Resolution (std::string s = "");

  // Initialize to a constant resolution RES.  I.e., sigma() will
  // always return RES.  If INVERSE is true, set the inverse flag.
  Resolution (double res, bool inverse = false);

  // Return the setting of the inverse flag.
  bool inverse () const;

  // Return the uncertainty for a momentum P.
  double sigma (double p) const;

  // Given a value X, measured for an object with momentum P, 
  // pick a new value from a Gaussian distribution
  // described by this resolution --- with mean X and width sigma(P).
  double pick (double x, double p, CLHEP::HepRandomEngine& engine) const;

  // Dump, for debugging.
  friend std::ostream& operator<< (std::ostream& s, const Resolution& r);


private:
  // The resolution parameters.
  double m_constant_sigma;
  double m_resolution_sigma;
  double m_noise_sigma;
  bool m_inverse;
};


} // namespace hitfit


#endif // not HITFIT_RESOLUTION_HPP
