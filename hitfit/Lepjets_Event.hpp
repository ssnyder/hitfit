//
// $Id: Lepjets_Event.hpp,v 1.1 2006-10-05 08:29:18 chriss Exp $
//
// File: hitfit/Lepjets_Event.hpp
// Purpose: Represent a simple `event' consisting of leptons and jets.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//
// An instance of this class holds a list of `leptons' (as represented
// by Lepjets_Event_Lep) and `jets' (as represented by Lepjets_Event_Jet).
// We also record:
//
//   - Missing Et
//   - z-vertex
//   - run and event number
//


#ifndef HITFIT_LEPJETS_EVENT_HPP
#define HITFIT_LEPJETS_EVENT_HPP


#include "hitfit/Lepjets_Event_Jet.hpp"
#include "hitfit/Lepjets_Event_Lep.hpp"
#include <vector>
#include <iosfwd>

#include "CLHEP/Random/RandomEngine.h"

namespace hitfit {


class Lepjets_Event
//
// Purpose: Represent a simple `event' consisting of leptons and jets.
//
{
public:
  // Constructor.
  Lepjets_Event (int runnum, int evnum);

  // Get the run and event number.
  int runnum () const;
  int evnum () const;

  // Get the length of the lepton and jet lists.
  int nleps () const;
  int njets () const;

  // Access leptons and jets.
  Lepjets_Event_Lep& lep (int i);
  Lepjets_Event_Jet& jet (int i);
  const Lepjets_Event_Lep& lep (int i) const;
  const Lepjets_Event_Jet& jet (int i) const;

  // Access missing Et.
  Fourvec& met ();
  const Fourvec& met () const;

  // Access kt resolution.
  Resolution& kt_res ();
  const Resolution& kt_res () const;

  // Access the z-vertex.
  double zvertex () const;
  double& zvertex ();

  // Access the isMC flag.
  bool isMC () const;
  void setMC (bool isMC);

  // Access the discriminants.
  float& dlb ();
  float dlb () const;
  float& dnn ();
  float dnn () const;

  // Sum all objects (leptons or jets) with type TYPE.
  Fourvec sum (int type) const;

  // Calculate kt --- sum of all objects plus missing Et.
  Fourvec kt () const;

  // Add new objects to the event.
  void add_lep (const Lepjets_Event_Lep& lep);
  void add_jet (const Lepjets_Event_Jet& jet);

  // Smear the objects in the event according to their resolutions.
  void smear (CLHEP::HepRandomEngine& engine, bool smear_dir = false);

  // Sort according to pt.
  void sort ();

  // Remove objects failing pt and eta cuts.
  int cut_leps (double pt_cut, double eta_cut);
  int cut_jets (double pt_cut, double eta_cut);

  // Remove all but the first N jets.
  void trimjets (int n);

  // Dump this object.
  std::ostream& dump (std::ostream& s, bool full = false) const;


private:
  // The lepton and jet lists.
  std::vector<Lepjets_Event_Lep> m_leps;
  std::vector<Lepjets_Event_Jet> m_jets;

  // Other event state.
  Fourvec m_met;
  Resolution m_kt_res;
  double m_zvertex;
  bool m_isMC;
  int m_runnum;
  int m_evnum;
  float m_dlb;
  float m_dnn;
};


// Print the object.
std::ostream& operator<< (std::ostream& s, const Lepjets_Event& ev);



} // namespace hitfit


#endif // not HITFIT_LEPJETS_EVENT_HPP

