//
// $Id: isajet_codes.hpp,v 1.1 2006-10-05 08:29:19 chriss Exp $
//
// File: hitfit/isajet_codes.hpp
// Purpose: Define names for some isajet particle codes.
// Created: Jan, 2001, sss.
//

#ifndef HITFIT_ISAJET_CODES_HPP
#define HITFIT_ISAJET_CODES_HPP


namespace hitfit {

enum {
  //
  // ****  Quarks symbolic names (ISAJET ids used)
  //
  UP = 1,
  DOWN = 2,
  STRANGE = 3,
  CHARM = 4,
  BOTTOM = 5,
  TOP = 6,

  //
  // ****  Leptons
  //
  NUE = 11,
  ELECTRON = 12,
  NUMU = 13,
  MUON = 14,
  NUTAU = 15,
  TAU = 16,

  //
  // ****  Bosons
  //
  GLUON = 9,
  PHOTON = 10,
  W_BOSON = 80,
  Z_BOSON = 90,
  HIGGS = 81,

  //
  // ****  Mesons
  //
  KSHORT = 20,
  KLONG = -20,
  PIZERO = 110,
  PION = 120,
  KAON = 130,
  KZERO = 230
};


}


#endif // not HITFIT_ISAJET_CODES_HPP
