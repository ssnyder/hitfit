//
// $Id: Vector_Resolution.hpp,v 1.1 2006-10-05 08:29:19 chriss Exp $
//
// File: hitfit/Vector_Resolution.hpp
// Purpose: Calculate resolutions in p, phi, eta.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//
// These objects hold three Resolution objects, one each for p, phi, eta.
// In addition, we have a use_et flag; if set, then the p resolution
// is really in pt.
//
// We can initialize these objects from a string; the format is
//
//   <p-res>/<eta-res>/<phi-res>[/et]
//
// where the resolution formats are as given in Resolution.hpp.
//


#ifndef HITFIT_VECTOR_RESOLUTION_HPP
#define HITFIT_VECTOR_RESOLUTION_HPP


#include <string>
#include <iosfwd>
#include "hitfit/Resolution.hpp"
#include "hitfit/fourvec.hpp"


namespace hitfit {


class Vector_Resolution
//
// Purpose: Calculate resolutions in p, phi, eta.
//
{
public:
  // Constructor.  Parse a string as described above.
  Vector_Resolution (std::string s);

  // Constructor from individual resolution objects.
  Vector_Resolution (const Resolution& p_res,
                     const Resolution& eta_res,
                     const Resolution& phi_res,
                     bool use_et = false);

  // Get back the individual resolution objects.
  const Resolution& p_res () const;
  const Resolution& eta_res () const;
  const Resolution& phi_res () const;

  // Return the use_et flag.
  bool use_et () const;

  // Calculate resolutions from a 4-momentum.
  double p_sigma   (const Fourvec& v) const;
  double eta_sigma (const Fourvec& v) const;
  double phi_sigma (const Fourvec& v) const;

  // Smear a 4-vector V according to the resolutions.
  // If DO_SMEAR_DIR is false, only smear the total energy.
  void smear (Fourvec& v,
              CLHEP::HepRandomEngine& engine,
              bool do_smear_dir = false) const;

  // Dump this object, for debugging.
  friend std::ostream& operator<< (std::ostream& s,
                                   const Vector_Resolution& r);

private:
  // State for this object.
  Resolution m_p_res;
  Resolution m_eta_res;
  Resolution m_phi_res;
  bool m_use_et;

  // Helper.
  void smear_dir (Fourvec& v, CLHEP::HepRandomEngine& engine) const;
};


} // namespace hitfit


#endif // not HITFIT_VECTOR_RESOLUTION_HPP
