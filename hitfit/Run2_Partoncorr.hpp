//
// $Id: Run2_Partoncorr.hpp,v 1.2 2009-02-12 10:36:02 chriss Exp $
//
// File: hitfit/Run2_Partoncorr.hpp
// Purpose: Apply run 2 parton level corrections.
// Created: May, 2003, Martijn Mulders, based on Scot's Run1_jetcorr code.
//


#ifndef HITFIT_RUN2_PARTONCORR_HPP
#define HITFIT_RUN2_PARTONCORR_HPP


#include "hitfit/Vector_Resolution.hpp"
#include <vector>
#include <cstdlib>
#include <string>



namespace {

bool get_field (std::string s, std::string::size_type i, double& x)
//
// Purpose: Scan string S starting at position I.
//          Find the value of the first floating-point number we
//          find there.
//
// Inputs:
//   s -           The string to scan.
//   i -           Starting character position in the string.
//
// Outputs:
//   x -           The value of the number we found.
//
// Returns:
//   True if we found something that looks like a number, false otherwise.
//
{
  std::string::size_type j = i;
  while (j < s.size() && s[j] != ',' && !isdigit (s[j]) && s[j] != '-' && s[j] != '.')
    ++j;
  if (j < s.size() && (isdigit (s[j]) || s[j] == '.' || s[j] == '-')) {
    x = std::atof (s.c_str() + j);
    return true;
  }
  return false;
}

} // unnamed namespace



namespace hitfit {


class Lepjets_Event;
class Defaults;


class Run2_Partoncorr_Args
//
// Purpose: Hold on to parameters for the Run2_Partoncorr class.
//
//   float jet_mass_thresh-If a jet's mass exceeds this, rescale its
//                        3-momentum so that its mass is zero.
//                        This works around a cafix bug.
//   float lrespar0     - Frank's lepton resolution parameterization.
//   float lrespar1     -Frank's lepton resolution parameterization.
//   float lrespar2     -Frank's lepton resolution parameterization.
//   bool fh_scale_data_to_mc - Use scale_match.
//   bool fh_scale_data -Do scaling for data.
//   bool fh_scale_mc   -Do scaling for MC.
//   bool fh_scale_corr_met -Adjust missing Et when doing scaling.
//   float etadep_etaN  - Maximum eta for Nth eta-dependent jet resolution.
//   string etadep_resN - Nth eta-dependent jet resolution, as a string
//                       to Vector_Resolution.
//
{
public:
  // Constructor.  Initialize from a Defaults object.
  Run2_Partoncorr_Args (const Defaults& defs);

  // Retrieve the eta-dependent jet resolution for ETA.
  const Vector_Resolution& find_etadep_jetres (double eta) const;

  // Retrieve the eta-dependent electron resolution for ETA.
  const Vector_Resolution& find_etadep_electronres (double eta) const;

  // Retrieve the eta-dependent muon resolution for ETA.
  const Vector_Resolution& find_etadep_muonres (double eta) const;

  // Retrieve parameter values.
  double jet_mass_thresh () const;
  double lrespar0 () const;
  double lrespar1 () const;
  double lrespar2 () const;
  bool fh_scale_data_to_mc () const;
  bool fh_scale_data () const;
  bool fh_scale_mc () const;
  bool fh_scale_corr_met () const;
  double GetPartonCorr (int   typej, 
                        double ejet, 
                        double etaj) const;

private:
  // Vector of eta-dependent jet resolutions.
  struct Etadep_Jetres
  {
    double etamax;
    Vector_Resolution res;
    Etadep_Jetres (double eta, std::string s) : etamax (eta), res (s) {}
  }; 
  std::vector<Etadep_Jetres> m_etadep_jetres;
  std::vector<Etadep_Jetres> m_etadep_electronres;
  std::vector<Etadep_Jetres> m_etadep_muonres;

  // Hold on to parameter values.
  double m_jet_mass_thresh;
  double m_lrespar0;
  double m_lrespar1;
  double m_lrespar2;
  bool m_fh_scale_data_to_mc;
  bool m_fh_scale_data;
  bool m_fh_scale_mc;
  bool m_fh_scale_corr_met;

  struct Scale_Pars
  {
    double m_p0 ;
    double m_p1 ;
    double m_p2 ;
    Scale_Pars(std::string s) : m_p0 (0.), m_p1 (0.), m_p2 (0.) {
      double x;
      m_p0 = 0.;
      m_p1 = 0.;
      m_p2 = 0.;
      size_t i = 0;
      if (get_field (s, i, x)) m_p0 = x;
      i = s.find (',', i); 
      if (i != std::string::npos) {
	if (get_field (s, ++i, x)) m_p1 = x;
	i = s.find (',', i);
	if (i != std::string::npos) {
	  if (get_field (s, ++i, x)) m_p2 = x;      
	}
      }
    }
  };
  std::vector<float> m_flavour_scale_etabins;
  std::vector<Scale_Pars> m_lightquark_scale_pars;
  std::vector<Scale_Pars> m_heavyquark_scale_pars;

};


// Apply run 2 parton level corrections to EV.
void run2_partoncorr (const Run2_Partoncorr_Args& args,
                      bool do_Parton, bool do_deltaS, int deltaS_nsigma,
                      bool do_Resolutions,
                      Lepjets_Event& ev);


} // namespace hitfit


#endif // not HITFIT_RUN2_PARTONCORR

