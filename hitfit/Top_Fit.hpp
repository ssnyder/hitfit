//
// $Id: Top_Fit.hpp,v 1.4 2007-07-03 16:17:25 erik Exp $
//
// File: hitfit/Top_Fit.hpp
// Purpose: Handle jet permutations.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//

#ifndef HITFIT_TOP_FIT_HPP
#define HITFIT_TOP_FIT_HPP


#include "hitfit/Constrained_Top.hpp"
#include "hitfit/Run1_Jetcorr.hpp"
#include "hitfit/Run2_Partoncorr.hpp"
#include "hitfit/matutil.hpp"
#include "hitfit/Lepjets_Event.hpp"
#include <iosfwd>
#ifndef ROOT_TObjArray
#include "TObjArray.h"
#endif

namespace hitfit {


class Fit_Results;


//
// Indices for the various results lists we make in Fit_Results.
//
enum Lists
{
  all_list = 0,         // All events.
  noperm_list = 1,      // All jet assignments are correct.
  semicorrect_list = 2, // Jets assigned to the correct top.
  limited_isr_list = 3, // Top three jets are not ISR.
  topfour_list = 4,     // Top four jets are not ISR, any other jets are ISR.
  btag_list = 5,        // All tagged jets were assigned as b-jets.
  htag_list = 6,        // All tagged jets were assigned as b-jets or higgs.
  n_lists = 7
};


//*************************************************************************


class Top_Fit_Args
//
// Purpose: Hold on to parameters for the Top_Fit class.
//
//   bool print_event_flag - If true, print the event after the fit.
//   bool print_nu_flag - If true, print results for both nu solutions.
//   bool do_bothnu_flag- If true, try both nu solutions.
//   bool do_higgs_flag - If true, fit ttH events.
//   bool do_run1_jetcorr-If true, apply run 1 jet corrections.
//   bool do_run2_partoncorr-If true, apply run 2 parton level corrections.
//   float jet_mass_cut - Reject events with jet masses larger than this.
//   float mwhad_min_cut- Reject events with hadronic W mass before
//                        fitting smaller than this.
//   float mwhad_max_cut- Reject events with hadronic W mass before
//                        fitting larger than this.
//   float mtdiff_max_cut-Reject events where the difference between
//                        leptonic and hadronic top masses before fitting
//                        is larger than this.
//   int nkeep          - Number of results to keep in each list.
//
{
public:
  // Constructor.  Initialize from a Defaults object.
  Top_Fit_Args (const Defaults& defs);

  // Retrieve parameter values.
  bool print_event_flag () const;
  bool print_nu_flag () const;
  bool do_bothnu_flag () const;
  bool do_higgs_flag () const;
  bool do_run1_jetcorr () const;
  bool do_run2_partoncorr () const;
  bool do_run2_deltaScorr () const;
  bool do_run2_SetResolutions () const;
  double jet_mass_cut () const;
  double mwhad_min_cut () const;
  double mwhad_max_cut () const;
  double mtdiff_max_cut () const;
  int nkeep () const;
  int deltaScorr_nsigma_systematic () const;

  // Arguments for subobjects.
  const Constrained_Top_Args& constrainer_args () const;
  const Run1_Jetcorr_Args& jetcorr_args () const;
  const Run2_Partoncorr_Args& partoncorr_args () const;


private:
  // Hold on to parameter values.
  bool m_print_event_flag;
  bool m_print_nu_flag;
  bool m_do_bothnu_flag;
  bool m_do_higgs_flag;
  bool m_do_run1_jetcorr;
  bool m_do_run2_partoncorr;
  bool m_do_run2_deltaScorr;
  bool m_do_run2_SetResolutions;
  double m_jet_mass_cut;
  double m_mwhad_min_cut;
  double m_mwhad_max_cut;
  double m_mtdiff_max_cut;
  int m_nkeep;
  int m_deltaScorr_nsigma_systematic;

  Constrained_Top_Args m_args;
  Run1_Jetcorr_Args m_jetcorr_args;
  Run2_Partoncorr_Args m_partoncorr_args;
};


//*************************************************************************


  class Top_Fit : public TObject
//
// Purpose: Handle jet permutations.
//
{
public:
  // Constructor.
  // LEPW_MASS, HADW_MASS, and TOP_MASS are the masses to which
  // those objects should be constrained.  To remove a constraint,
  // set the mass to 0.
  Top_Fit (const Top_Fit_Args& args,
           double lepw_mass,
           double hadw_mass,
           double top_mass);

  // Fit a single jet permutation.  Return the results for that fit.
  double fit_one_perm (Lepjets_Event& ev,
                       double& umwhad,
                       double& utmass,
                       double& mt,
                       double& sigmt,
                       Column_Vector& pullx,
                       Column_Vector& pully);

  // Do the same but using Minuit
  double fit_one_perm_minuit (Lepjets_Event& ev,
                       double& umwhad,
                       double& utmass,
                       double& mt,
                       double& sigmt,
                       Column_Vector& pullx,
                       Column_Vector& pully);

  // return event to/after fit 
  Lepjets_Event get_event_to_fit();
  Lepjets_Event get_event_after_fit();


  // Fit all jet permutations in EV.
  Fit_Results fit (const Lepjets_Event& ev, bool use_minuit=false);

  // Print.
  friend std::ostream& operator<< (std::ostream& s, const Top_Fit& fitter);
  // This global function needs access to computeFCN()
  friend void TFCN(int & npar, double* gin, double & f, double* par, int flag);

private:
  // The object state.
  const Top_Fit_Args& m_args;
  Constrained_Top m_constrainer;
  double m_hadw_mass;
  Lepjets_Event m_event_to_fit;
  Lepjets_Event m_event_after_fit;
  void ComputeFCN(int& numpar, double* dum2, double& chi2, double* par, int iflag);

  Column_Vector m_pars;
  Column_Vector m_errs;
  Column_Vector m_steps;
  Column_Vector m_chiterms;
};


} // namespace hitfit


#endif // not HITFIT_TOP_FIT_HPP
