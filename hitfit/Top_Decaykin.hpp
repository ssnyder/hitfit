//
// $Id: Top_Decaykin.hpp,v 1.1 2006-10-05 08:29:19 chriss Exp $
//
// File: hitfit/Top_Decaykin.hpp
// Purpose: Calculate some kinematic quantities for ttbar events.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//


#ifndef TOP_DECAYKIN_HPP
#define TOP_DECAYKIN_HPP


#include "hitfit/fourvec.hpp"
#include <iosfwd>


namespace hitfit {


class Lepjets_Event;


class Top_Decaykin
//
// Purpose: Calculate some kinematic quantities for ttbar events.
//          This class has no state --- just static member functions.
// 
{
public:
  // Solve for the longitudinal z-momentum that makes the leptonic
  // top have mass TMASS.
  static bool solve_nu_tmass (const Lepjets_Event& ev, double tmass,
                              double& nuz1, double& nuz2);

  // Solve for the longitudinal z-momentum that makes the leptonic
  // W have mass WMASS.
  static bool solve_nu (const Lepjets_Event& ev, double wmass,
                        double& nuz1, double& nuz2);

  // Sum up the appropriate 4-vectors to find the hadronic W.
  static Fourvec hadw (const Lepjets_Event& ev);

  // Sum up the appropriate 4-vectors to find the leptonic W.
  static Fourvec lepw (const Lepjets_Event& ev);

  // Sum up the appropriate 4-vectors to find the hadronic t.
  static Fourvec hadt (const Lepjets_Event& ev);

  // Sum up the appropriate 4-vectors to find the leptonic t.
  static Fourvec lept (const Lepjets_Event& ev);

  // Print kinematic information for EV.
  static std::ostream& dump_ev (std::ostream& s, const Lepjets_Event& ev);
};


} // namespace hitfit


#endif // not TOP_DECAYKIN_HPP

