//
// $Id: Defaults.hpp,v 1.1 2006-10-05 08:29:18 chriss Exp $
//
// File: hitfit/Defaults.hpp
// Purpose: Define an interface for getting parameter settings.
// Created: Nov, 2000, sss.
//
// This defines a very simple abstract interface for retrieving settings
// for named parameters.  Using this ensures that the hitfit code doesn't
// have to depend on something like rcp.  There is a lightweight concrete
// implementation of this interface, Defaults_Text, which can be used
// for standalone applications.  If this code gets used with the D0 framework,
// a Defaults_RCP can be provided too.
//


#ifndef HITFIT_DEFAULTS_HPP
#define HITFIT_DEFAULTS_HPP


#include <string>


namespace hitfit {


class Defaults
//
// Purpose: Define an interface for getting parameter settings.
//
{
public:
  // Constructor, destructor.
  Defaults () {}
  virtual ~Defaults () {}

  // Test to see if parameter NAME exists.
  virtual bool exists (std::string name) const = 0;

  // Get the value of NAME as an integer.
  virtual int get_int (std::string name) const = 0;

  // Get the value of NAME as a boolean.
  virtual bool get_bool (std::string name) const = 0;

  // Get the value of NAME as a float.
  virtual double get_float (std::string name) const = 0;

  // Get the value of NAME as a string.
  virtual std::string get_string (std::string name) const = 0;
};


} // namespace hitfit


#endif // not HITFIT_DEFAULTS_HPP
