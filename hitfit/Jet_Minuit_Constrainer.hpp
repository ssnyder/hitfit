// This file's extension implies that it's C, but it's really -*- C++ -*-.
// $Id$
/**
 * @file Jet_Minuit_Constrainer.hpp
 * @author scott snyder <snyder@bnl.gov>
 * @date Feb, 2013
 * @brief Fit the sum of a set of jets to a resonance using minuit.
 */


#ifndef HITFIT_JET_MINUIT_CONSTRAINER_HPP
#define HITFIT_JET_MINUIT_CONSTRAINER_HPP


#include "hitfit/Fourvec_Event.hpp"
#include "hitfit/matutil.hpp"
#include <vector>


namespace hitfit {


class Jet_Minuit_Constrainer
{
public:
  Jet_Minuit_Constrainer (int masslabel, double mass, double width,
                          int isrlabel = -1,
                          bool domet = false);

  double constrain (Fourvec_Event& ev,
                    double* m,
                    Column_Vector& pull,
                    double tol = 1e-6);


private:
  double eval (const Fourvec_Event& ev, unsigned int njet, double* jet_E,
               double* m=0, Column_Vector* pull=0);
  std::vector<double> get_jets (const Fourvec_Event& ev);
  void set_jets  (Fourvec_Event& ev, const std::vector<double>& jet_E);
  int fit (const Fourvec_Event& ev, std::vector<double>& jet_E, double tol);

  static void fcn (int& npar, double* grad, double& f, double* par, int iflag);


  int m_masslabel;
  int m_isrlabel;
  double m_mass;
  double m_width2;

  const Fourvec_Event* m_ev;
  bool m_domet;
  static Jet_Minuit_Constrainer* m_self;
};


} // namespace hitfit


#endif // not HITFIT_JET_MINUIT_CONSTRAINER_HPP
