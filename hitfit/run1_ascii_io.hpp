//
// $Id: run1_ascii_io.hpp,v 1.1 2006-10-05 08:29:19 chriss Exp $
//
// File: hitfit/run1_ascii_io.hpp
// Purpose: Read and write the run 1 ntuple dump files.
// Created: Dec, 2000, sss, based on run 1 mass analysis code.
//

#ifndef HITFIT_RUN1_ASCII_IO_HPP
#define HITFIT_RUN1_ASCII_IO_HPP


#include "hitfit/Lepjets_Event.hpp"
#include <iosfwd>
class HepRandomEngine;


namespace hitfit {


class Defaults;
class Vector_Resolution;


class Run1_Ascii_IO_Args
//
// Purpose: Hold on to parameters for run1_ascii_io.
//
//   bool use_e         - If true, then when rescaling the 4-vectors
//   string jet_type_wanted-Character specifying which jet type algorithm
//                        is to be used.
//   string ele_res     - Electron resolutions, for Vector_Resolution.
//   string muo_res     - Muon resolutions, for Vector_Resolution.
//   string jet_res     - Jet resolutions, for Vector_Resolution.
//   string kt_res      - Kt resolution, for Resolution.
//
{
public:
  // Constructor.  Initialize from a Defaults object.
  Run1_Ascii_IO_Args (const Defaults& defs);

  // Retrieve parameter values.
  char jet_type_wanted () const;
  const Vector_Resolution& ele_res () const;
  const Vector_Resolution& muo_res () const;
  const Vector_Resolution& jet_res () const;
  const Resolution& kt_res () const;

private:
  // Hold on to parameter values.
  char m_jet_type_wanted;
  Vector_Resolution m_ele_res;
  Vector_Resolution m_muo_res;
  Vector_Resolution m_jet_res;
  Resolution m_kt_res;
};


//***************************************************************************


// Read an event from stream S.
Lepjets_Event read_run1_ascii (std::istream& s,
                               const Run1_Ascii_IO_Args& args);

// Write an event to stream S.
std::ostream& write_run1_ascii (std::ostream& s, const Lepjets_Event& ev);


} // namespace hitfit


#endif // not HITFIT_RUN1_ASCII_IO_HPP
