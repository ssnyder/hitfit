//
// $Id: Fourvec_Event.hpp,v 1.1 2006-10-05 08:29:18 chriss Exp $
//
// File: hitfit/Fourvec_Event.hpp
// Purpose: Represent an event for kinematic fitting as a collection
//           of 4-vectors.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//
// This class represents an `event' for kinematic fitting by
// Fourvec_Constrainer.  Each object in the event has the following
// attributes:
//
//   4-vector
//   mass
//     The kinematic fit assumes a fixed mass for each object.
//     That is specified by the `mass' attribute here.
//
//   p, phi, eta uncertainties
//   muon flag
//     If this is set, the `p' uncertainty is really in 1/p.
//
//   label
//     An integer that can be used to identify the object type.
//     I.e., lepton, b-jet from hadronic top, etc.
//
// There may be an object for a neutrino.
// If so, it is always at the end of the object list.
// It is not included in the count returned by nobjs() (but is included
// in nobjs_all()).
//
// We can also record one other `x' momentum, that will be added
// into the kt sum.  This can be used to store a missing Et that
// is not attributed to a neutrino (but is instead due to mismeasurement).
// Typically, this will be set to zero in events that have a neutrino,
// and to the measured missing Et in events that do not.
//


#ifndef HITFIT_FOURVEC_EVENT_HPP
#define HITFIT_FOURVEC_EVENT_HPP


#include "hitfit/fourvec.hpp"
#include <vector>
#include <iosfwd>


namespace hitfit {


struct FE_Obj
//
// Purpose: Represent a single object in a Fourvec_Event.
//          This is just a dumb data container.
//
{
  // The 4-momentum of the object.
  Fourvec p;

  // The mass of the object.
  // The kinematic fit will fix the mass to this.
  double mass;

  // A label to identify the object type.
  int label;

  // p, phi, and eta uncertainties.
  double p_error;
  double phi_error;
  double eta_error;

  // If this is true, then p_error is really an uncertainty in 1/p,
  // rather than p (and we should use 1/p as the fit variable).
  bool muon_p;

  // Constructor, for convenience.
  FE_Obj (Fourvec the_p,
          double the_mass,
          int the_label,
          double the_p_error,
          double the_phi_error,
          double the_eta_error,
          bool the_muon_p);
};


// Print it out.
std::ostream& operator<< (std::ostream& s, const FE_Obj& o);


//************************************************************************


// The special label used for a neutrino.
const int nu_label = -1;


class Fourvec_Event
//
// Purpose: Represent an event for kinematic fitting as a collection
//           of 4-vectors.
//
{
public:
  // Constructor.
  Fourvec_Event ();


  //****************************
  // Accessors.
  //

  // Return true if this event contains a neutrino.
  bool has_neutrino () const;

  // Return the number of objects in the event, not including any neutrino.
  int nobjs () const;

  // Return the number of objects in the event, including any neutrino.
  int nobjs_all () const;

  // Access object I.  (Indexing starts with 0.)
  const FE_Obj& obj (int i) const;

  // Access the neutrino 4-momentum.
  const Fourvec& nu () const;

  // Access the kt 4-momentum.
  const Fourvec& kt () const;

  // Access the X 4-momentum.
  const Fourvec& x () const;

  // Access the kt uncertainties.
  double kt_x_error () const;
  double kt_y_error () const;
  double kt_xy_covar () const;

  // Print out the contents.
  friend std::ostream& operator<< (std::ostream& s,
                                   const Fourvec_Event& ce);


  //****************************
  // Modifiers.
  //

  // Add an object to the event.
  // (This should not be a neutrino --- use set_nu_p for that.)
  void add (const FE_Obj& obj);

  // Set the neutrino 4-momentum to P.
  // This adds a neutrino if there wasn't already one.
  void set_nu_p (const Fourvec& p);

  // Set the 4-momentum of object I to P.
  void set_obj_p (int i, const Fourvec& p);

  // Set the 4-momentum of the X object.
  void set_x_p (const Fourvec& p);

  // Set the kt uncertainties.
  void set_kt_error (double kt_x_error, double kt_y_error, double kt_xy_covar);

  // Zero everything.
  void clear();


private:
  // The list of contained objects.
  std::vector<FE_Obj> m_objs;

  // Cached kt.  This should always be equal to the sum of all the
  // object momenta, including x.
  Fourvec m_kt;

  // Momemtum of the X object.
  Fourvec m_x;

  // The kt uncertainties.
  double m_kt_x_error;
  double m_kt_y_error;
  double m_kt_xy_covar;

  // Flag that a neutrino has been added.
  bool m_has_neutrino;
};


} // namespace hitfit


#endif // not HITFIT_FOURVEC_EVENT_HPP
