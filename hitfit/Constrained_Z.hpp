//
// $Id: Constrained_Z.hpp,v 1.1 2006-10-05 08:29:18 chriss Exp $
//
// File: Constrained_Z.hpp
// Purpose: Do kinematic fitting for a (Z->ll)+jets event.
// Created: Apr, 2004, sss
//

#ifndef CONSTRAINED_Z_HPP
#define CONSTRAINED_Z_HPP


#include "hitfit/Fourvec_Constrainer.hpp"
#include "hitfit/matutil.hpp"
#include <iosfwd>


namespace hitfit {


class Defaults;
class Lepjets_Event;


class Constrained_Z_Args
//
// Purpose: Hold on to parameters for the Constrained_Z class.
//
// Parameters controlling the operation of the fitter:
//   float zmass        - The mass to which the Z should be fixed.
//
{
public:
  // Constructor.  Initialize from a Defaults object.
  Constrained_Z_Args (const Defaults& defs);

  // Retrieve parameter values.
  double zmass () const;

  // Arguments for subobjects.
  const Fourvec_Constrainer_Args& fourvec_constrainer_args () const;

private:
  // Hold on to parameter values.
  double m_zmass;

  Fourvec_Constrainer_Args m_fourvec_constrainer_args;
};


//*************************************************************************


class Constrained_Z
//
// Purpose: Do kinematic fitting for a (Z->ll)+jets event.
//
{
public:
  // Constructor.
  Constrained_Z (const Constrained_Z_Args& args);

  // Do a constrained fit.
  double constrain (Lepjets_Event& ev, Column_Vector& pull);

  // Dump out our state.
  friend std::ostream& operator<< (std::ostream& s, const Constrained_Z& cz);


private:
  // Parameter settings.
  const Constrained_Z_Args& m_args;

  // The guy that actually does the work.
  Fourvec_Constrainer m_constrainer;
};


} // namespace hitfit


#endif // not CONSTRAINED_Z_HPP
