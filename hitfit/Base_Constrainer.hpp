//
// $Id: Base_Constrainer.hpp,v 1.1 2006-10-05 08:29:17 chriss Exp $
//
// File: hitfit/Base_Constrainer.hpp
// Purpose: Abstract base for the chisq fitter classes.
//          This allows for different algorithms to be used.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//

#ifndef HITFIT_BASE_CONSTRAINER_HPP
#define HITFIT_BASE_CONSTRAINER_HPP


#include "hitfit/matutil.hpp"
#include <iosfwd>


namespace hitfit {


class Defaults;


//*************************************************************************


class Base_Constrainer_Args
//
// Purpose: Hold on to parameters for the Base_Constrainer class.
//
// Parameters:
//   bool test_gradient - If true, check the constraint gradient calculations
//                        by also doing them numerically.
//   float test_step    - When test_gradient is true, the step size to use
//                        for numeric differentiation.
//   float test_eps     - When test_gradient is true, the maximum relative
//                        difference permitted between returned and
//                        numerically calculated gradients.
//
{
public:
  // Constructor.  Initialize from a Defaults object.
  Base_Constrainer_Args (const Defaults& defs);

  // Retrieve parameter values.
  bool test_gradient () const;
  double test_step () const;
  double test_eps () const;


private:
  // Hold on to parameter values.
  bool m_test_gradient;
  double m_test_step;
  double m_test_eps;
};


//*************************************************************************


class Constraint_Calculator
//
// Purpose: Abstract base class for evaluating constraints.
//          Derive from this and implement the eval() method.
//
{
public:
  // Constructor, destructor.  Pass in the number of constraints.
  Constraint_Calculator (int nconstraints);
  virtual ~Constraint_Calculator () {}

  // Get back the number of constraints.
  int nconstraints () const;

  // Evaluate constraints at the point described by X and Y (well-measured
  // and poorly-measured variables, respectively).  The results should
  // be stored in F.  BX and BY should be set to the gradients of F with
  // respect to X and Y, respectively.
  //
  // Return true if the point X, Y is accepted.
  // Return false if it is rejected (i.e., in an unphysical region).
  // The constraints need not be evaluated in that case.
  virtual bool eval (const Column_Vector& x,
                     const Column_Vector& y,
                     Row_Vector& F,
                     Matrix& Bx,
                     Matrix& By) = 0;


private:
  // The number of constraint functions.
  int m_nconstraints;
};


//*************************************************************************


class Base_Constrainer
//
// Purpose: Base class for chisq constrained fitter.
//
{
public:
  // Constructor, destructor.
  // ARGS holds the parameter settings for this instance.
  Base_Constrainer (const Base_Constrainer_Args& args);
  virtual ~Base_Constrainer () {}

  // Do the fit.
  // Call the number of well-measured variables Nw, the number of
  // poorly-measured variables Np, and the number of constraints Nc.
  // Inputs:
  //   CONSTRAINT_CALCULATOR is the object that will be used to evaluate
  //     the constraints.
  //   XM(Nw) and YM(Np) are the measured values of the well- and
  //     poorly-measured variables, respectively.
  //   X(Nw) and Y(Np) are the starting values for the fit.
  //   G_I(Nw,Nw) is the error matrix for the well-measured variables.
  //   Y(Np,Np) is the inverse error matrix for the poorly-measured variables.
  //
  // Outputs:
  //   X(Nw) and Y(Np) is the point at the minimum.
  //   PULLX(Nw) and PULLY(Np) are the pull quantities.
  //   Q(Nw,Nw), R(Np,Np), and S(Nw,Np) are the final error matrices
  //     between all the variables.
  //
  // The return value is the final chisq.  Returns a value < 0 if the
  // fit failed to converge.
  virtual double fit (Constraint_Calculator& constraint_calculator,
                      const Column_Vector& xm,
                      Column_Vector& x,
                      const Column_Vector& ym,
                      Column_Vector& y,
                      const Matrix& G_i,
                      const Diagonal_Matrix& Y,
                      Column_Vector& pullx,
                      Column_Vector& pully,
                      Matrix& Q,
                      Matrix& R,
                      Matrix& S) = 0;

  // Print out any internal state to S.
  virtual std::ostream& print (std::ostream& s) const;

  // Print out internal state to S.
  friend std::ostream& operator<< (std::ostream& s, const Base_Constrainer& f);


private:
#ifndef __REFLEX__
  // Parameter settings.
  const Base_Constrainer_Args& m_args;
#endif
  Base_Constrainer& operator= (const Base_Constrainer&);


protected:
  // Helper function to evaluate the constraints.
  // This takes care of checking what the user function returns against
  // numerical derivatives, if that was requested.
  bool call_constraint_fcn (Constraint_Calculator& constraint_calculator,
                            const Column_Vector& x,
                            const Column_Vector& y,
                            Row_Vector& F,
                            Matrix& Bx,
                            Matrix& By) const;
};


} // namespace hitfit


#endif // not HITFIT_BASE_CONSTRAINER_HPP

