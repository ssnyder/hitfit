//
// $Id: gentop.hpp,v 1.1 2006-10-05 08:29:19 chriss Exp $
//
// File: hitfit/gentop.hpp
// Purpose: Toy ttbar event generator for testing.
// Created: Jul, 2000, sss.
//
// This is a very simple event generator for ttbar events, to allow some
// basic tests of the mass fitting code.  We generate random ttbars,
// with kinematics pulled out of a hat, and then decay them into l+jets
// events.  No radiation or other such luxuries, and, as mentioned, any
// kinematic distribuions will certainly be wrong.  But the generated
// events should satisfy the l+jets mass constraints.
//


#include <string>
#include <iosfwd>


#include "CLHEP/Random/RandomEngine.h"

namespace hitfit {


class Defaults;
class Lepjets_Event;


class Gentop_Args
//
// Hold on to parameters for the toy event generator.
//   float mt           - Generated top mass.
//   float sigma_mt     - Width of top mass distribution.
//
//   float mh           - Generated Higgs mass.
//   float sigma_mh     - Width of Higgs mass distribution.
//
//   float mw           - Generated W mass.
//   float sigma_mw     - Width of W mass distribution.
//
//   float mb           - Generated b mass.
//   float sigma_mb     - Width of b mass distribution.
//
//   float t_pt_mean    - Mean pt of the generated top quarks.
//                        (It will be drawn from an exponential distribution.)
//   float recoil_pt_mean-Mean pt of ttbar system.
//                        (It will be drawn from an exponential distribution.)
//   float boost_sigma  - Width of z-boost of ttbar system.
//   float m_boost      - Mass of z-boost of ttbar system.
//   float sxv_tageff   - Assumed efficiency of SVX b-tag.
//   bool smear         - If true, smear the event.
//   bool smear_dir     - If false, smear only energies, not directions.
//   bool muon          - If false, decay leptonic ts into electrons.
//                        Otherwise, decay into muons.
//   string ele_res_str - Electron resolution, for Vector_Resolution.
//   string muo_res_str - Muon resolution, for Vector_Resolution.
//   string jet_res_str - Jet resolution, for Vector_Resolution.
//   string kt_res_str  - Kt resolution, for Resolution.
//
{
public:
  // Constructor.  Initialize from a Defaults object.
  Gentop_Args (const Defaults& defs);

  // Retrieve parameter values.
  double t_pt_mean () const;
  double mt () const;
  double sigma_mt () const;
  double mh () const;
  double sigma_mh () const;
  double recoil_pt_mean () const;
  double boost_sigma () const;
  double m_boost () const;
  double mb () const;
  double sigma_mb () const;
  double mw () const;
  double sigma_mw () const;
  double svx_tageff () const;
  bool smear () const;
  bool smear_dir () const;
  bool muon () const;
  std::string ele_res_str () const;
  std::string muo_res_str () const;
  std::string jet_res_str () const;
  std::string  kt_res_str () const;

private:
  // Hold on to parameter values.
  double _t_pt_mean;
  double _mt;
  double _sigma_mt;
  double _mh;
  double _sigma_mh;
  double _recoil_pt_mean;
  double _boost_sigma;
  double _m_boost;
  double _mb;
  double _sigma_mb;
  double _mw;
  double _sigma_mw;
  double _svx_tageff;
  bool   _smear;
  bool   _smear_dir;
  bool   _muon;
  std::string _ele_res_str;
  std::string _muo_res_str;
  std::string _jet_res_str;
  std::string _kt_res_str;
};


// Generate a ttbar -> ljets event.
Lepjets_Event gentop (const Gentop_Args& args,
                      CLHEP::HepRandomEngine& engine);

// Generate a ttH -> ljets+bb event.
Lepjets_Event gentth (const Gentop_Args& args,
                      CLHEP::HepRandomEngine& engine);


} // namespace hitfit
