//
// $Id: fourvec.hpp,v 1.1 2006-10-05 08:29:19 chriss Exp $
//
// File: hitfit/fourvec.hpp
// Purpose: Define 3- and 4-vector types for the hitfit package, and
//          supply a few additional operations.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//
// This file defines the types `Threevec' and `Fourvec' to be used
// in hitfit code.  These are based on the corresponding CLHEP classes.
// We also provide a handful of operations in addition to those that
// CLHEP has.
//

#ifndef HITFIT_FOURVEC_HPP
#define HITFIT_FOURVEC_HPP

#include "CLHEP/Vector/LorentzVector.h"


namespace hitfit {


// Define the types that we want to use.
typedef CLHEP::HepLorentzVector Fourvec;
typedef CLHEP::Hep3Vector Threevec;

// Adjust the 3-vector part of V (leaving the energy unchanged) so that
// it has mass MASS.
void adjust_p_for_mass (Fourvec& v, double mass);

// Adjust the energy component of V (leaving the 3-vector part unchanged)
// so that it has mass MASS.
void adjust_e_for_mass (Fourvec& v, double mass);

// Rotate V through polar angle THETA.
void rottheta (Fourvec& v, double theta);

// Rotate V through a polar angle such that its pseudorapidity changes by ETA.
void roteta (Fourvec& v,   double eta);

// Conversions between pseudorapidity and polar angle.
double eta_to_theta (double eta);
double theta_to_eta (double theta);

// Get the detector eta (D0-specific).  Needs a Z-vertex.
double deteta (const Fourvec& v, double zvert);  // XXX

// Get the detector eta (D0-specific), for a sub-detector with radius r_detector 
// and z-halflength z_detector.  Needs a Z-vertex.
double deteta (const Fourvec& v, double zvert, double r_detector, double z_detector);  // XXX

// Handle wraparound for a difference in azimuthal angles.
double phidiff (double phi);

// Find the distance in R between two four-vectors.
double delta_r (const Fourvec& a, const Fourvec& b);


} // namespace hitfit


#endif // not HITFIT_FOURVEC_HPP

