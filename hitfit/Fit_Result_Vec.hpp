//
// $Id: Fit_Result_Vec.hpp,v 1.1 2006-10-05 08:29:18 chriss Exp $
//
// File: hitfit/Fit_Result_Vec.hpp
// Purpose: Hold a set of Fit_Result structures.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//
// This class holds pointers to a set of Fit_Result's, resulting from
// different jet permutation with some consistent selection.
// The results are ordered by increasing chisq values.  A maximum
// length for the vector may be specified; when new results are added,
// those with the largest chisq fall off the end.
//
// The Fit_Result objects are reference counted, in order to allow them
// to be entered in multiple vectors.
//

#ifndef HITFIT_FIT_RESULT_VEC_HPP
#define HITFIT_FIT_RESULT_VEC_HPP


#include <vector>
#include <iosfwd>


namespace hitfit {


class Fit_Result;


class Fit_Result_Vec
//
// Purpose: Hold a set of Fit_Result structures.
//
{
public:
  // Constructor, destructor.  The maximum length of the vector
  // is specified here.
  Fit_Result_Vec (int max_len);
  ~Fit_Result_Vec ();

  // Copy constructor.
  Fit_Result_Vec (const Fit_Result_Vec& vec);

  // Assignment.
  Fit_Result_Vec& operator= (const Fit_Result_Vec& vec);

  // Get back the number of results in the vector.
  int size () const;

  // Get back the Ith result in the vector.
  const Fit_Result& operator[] (int i) const;

  // Add a new result to the vector.
  void push (Fit_Result* res);

  // Dump out the vector.
  friend std::ostream& operator<< (std::ostream& s,
                                   const Fit_Result_Vec& resvec);

private:
  // The object state.
  std::vector<Fit_Result*> m_v;
  int m_max_len;
};


} // namespace hitfit


#endif // not HITFIT_FIT_RESULT_HPP
