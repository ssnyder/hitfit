//
// $Id: Fit_Result.hpp,v 1.2 2007-07-03 16:17:24 erik Exp $
//
// File: hitfit/Fit_Result.hpp
// Purpose: Hold the result from a single kinematic fit.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//
// These objects are reference-counted.
//

#ifndef HITFIT_FIT_RESULT_HPP
#define HITFIT_FIT_RESULT_HPP


#include "hitfit/Refcount.hpp"
#include "hitfit/Lepjets_Event.hpp"
#include "hitfit/matutil.hpp"
#include <iosfwd>


namespace hitfit {


class Fit_Result
  : public Refcount
//
// Purpose: Hold the result from a single kinematic fit.
//
{
public:
  // Constructor.  Provide the results of the fit.
  Fit_Result (double chisq,
              const Lepjets_Event& ev,
              const Column_Vector& pullx,
              const Column_Vector& pully,
              double umwhad,
              double utmass,
              double mt,
              double sigmt,
              const Column_Vector&  pars, 
              const Column_Vector&  errs, 
              const Column_Vector&  steps, 
              const Column_Vector&  chiterms);

  // Get back the fit result.
  double chisq () const;
  double umwhad () const;
  double utmass () const;
  double mt () const;
  double sigmt () const;
  const Column_Vector& pullx () const;
  const Column_Vector& pully () const;
  const Lepjets_Event& ev () const;
  double pars (int i) const;
  double errs (int i) const;
  double steps (int i) const;
  double chiterms (int i) const;
  const Column_Vector& pars () const;
  const Column_Vector& errs () const;
  const Column_Vector& steps () const;
  const Column_Vector& chiterms () const;
  
  // For sorting by chisq.
  friend bool operator< (const Fit_Result& a, const Fit_Result& b);

  // Print this object.
  friend std::ostream& operator<< (std::ostream& s, const Fit_Result& res);


private:
  // Store the results of the kinematic fit.
  double m_chisq;
  double m_umwhad;
  double m_utmass;
  double m_mt;
  double m_sigmt;
  Column_Vector m_pullx;
  Column_Vector m_pully;
  Lepjets_Event m_ev;
  Column_Vector m_pars;
  Column_Vector m_errs;
  Column_Vector m_steps;
  Column_Vector m_chiterms;
};


} // namespace hitfit


#endif // not HITFIT_FIT_RESULT_HPP
