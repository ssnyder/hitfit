//
// $Id: Refcount.hpp,v 1.1 2006-10-05 08:29:18 chriss Exp $
//
// File: Refcount.hpp
// Purpose: A base class for a simple reference-counted object.
// Created: Aug 2000, sss, from the version that used to be in d0om.
//
// To make a reference counted type, derive from class d0_util::Refcount.
// When the object is created, its refcount is initially set to 0;
// the first action taken by the creator should be to call incref()
// to bump the refcount to 1.  Thereafter, the refcount may be incremented
// by incref() and decremented by decref().  If the reference count reaches
// 0, the object calls delete on itself.
//
// If the object is deleted explicitly, the reference count must be 0 or 1.
// Otherwise, an assertion violation will be reported.
//

#ifndef HITFIT_REFCOUNT_HPP
#define HITFIT_REFCOUNT_HPP

namespace hitfit {


class Refcount
//
// Purpose: Simple reference-counted object.
//
{
public:
  // Constructor, destructor.
  Refcount ();
  virtual ~Refcount ();

  // Increment and decrement reference count.
  void incref () const;
  void decref () const;

  // True if calling decref() will delete the object.
  bool decref_will_delete () const;

  // True if incref() has never been called, or if the object is being
  // deleted.
  bool unowned () const;


protected:
  // Reset the refcount to zero.
  // This should only be used in the context of a dtor of a derived
  // class that wants to throw an exception.
  // It may also be used to implement a `release' function.
  void nuke_refcount ();


private:
  // The reference count itself.
  mutable unsigned m_refcount;
};


} 


#include "hitfit/private/Refcount.ipp"


#endif 
