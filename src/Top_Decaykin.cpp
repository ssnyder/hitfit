//
// $Id: Top_Decaykin.cpp,v 1.1 2006-10-05 08:29:22 chriss Exp $
//
// File: src/Top_Decaykin.cpp
// Purpose: Calculate some kinematic quantities for ttbar events.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//


#include "hitfit/Top_Decaykin.hpp"
#include "hitfit/Lepjets_Event.hpp"
#include "hitfit/fourvec.hpp"
#include <cmath>
#include <algorithm>
#include <ostream>


using std::sqrt;
using std::abs;
using std::swap;
using std::ostream;


namespace hitfit {


namespace {


Fourvec leptons (const Lepjets_Event& ev)
//
// Purpose: Sum all leptons in EV.
//
// Inputs:
//   ev -          The event.
//
// Returns:
//   The sum of all leptons in EV.
//
{
  return (ev.sum (lepton_label) +
          ev.sum (electron_label) +
          ev.sum (muon_label));
}


} // unnamed namespace


bool Top_Decaykin::solve_nu_tmass (const Lepjets_Event& ev,
                                   double tmass,
                                   double& nuz1, double& nuz2)
//
// Purpose: Solve for the neutrino longitudinal z-momentum that makes
//          the leptonic top have mass TMASS.
//
// Inputs:
//   ev -          The event to solve.
//   tmass -       The desired top mass.
//
// Outputs:
//   nuz1 -        First solution (smaller absolute value).
//   nuz2 -        Second solution.
//  
// Returns:
//   True if there was a real solution.  False if there were only
//   imaginary solutions.  (In that case, we just set the imaginary
//   part to zero.)
//  
{
  bool discrim_flag = true;

  const Fourvec& vnu = ev.met ();
  Fourvec cprime = leptons (ev) + ev.sum (lepb_label);
  double alpha1 = tmass*tmass - cprime.m2();
  double a = 2 * 4 * (cprime.z()*cprime.z() - cprime.e()*cprime.e());
  double alpha = alpha1 + 2*(cprime.x()*vnu.x() + cprime.y()*vnu.y());
  double b = 4 * alpha * cprime.z();
  double c = alpha*alpha - 4 * cprime.e()*cprime.e() * vnu.vect().perp2();
  double d = b*b - 2*a*c;
  if (d < 0) {
    discrim_flag = false;
    d = 0;
  }

  double dd = sqrt (d);
  nuz1 = (-b + dd)/a;
  nuz2 = (-b - dd)/a;
  if (abs (nuz1) > abs (nuz2))
    swap (nuz1, nuz2);

  return discrim_flag;
}


bool Top_Decaykin::solve_nu (const Lepjets_Event& ev,
                             double wmass,
                             double& nuz1,
                             double& nuz2)
//
// Purpose: Solve for the neutrino longitudinal z-momentum that makes
//          the leptonic W have mass WMASS.
//
// Inputs:
//   ev -          The event to solve.
//   wmass -       The desired W mass.
//
// Outputs:
//   nuz1 -        First solution (smaller absolute value).
//   nuz2 -        Second solution.
//
// Returns:
//   True if there was a real solution.  False if there were only
//   imaginary solutions.  (In that case, we just set the imaginary
//   part to zero.)
//  
{
  bool discrim_flag = true;

  Fourvec vnu  = ev.met();
  Fourvec vlep = leptons (ev);

  double x = vlep.x()*vnu.x() + vlep.y()*vnu.y() + wmass*wmass/2;
  double a = vlep.z()*vlep.z() - vlep.e()*vlep.e();
  double b = 2*x*vlep.z();
  double c = x*x - vnu.perp2() * vlep.e()*vlep.e();

  double d = b*b - 4*a*c;
  if (d < 0) {
    d = 0;
    discrim_flag = false;
  }

  nuz1 = (-b + sqrt (d))/2/a;
  nuz2 = (-b - sqrt (d))/2/a;
  if (abs (nuz1) > abs (nuz2))
    swap (nuz1, nuz2);

  return discrim_flag;
}


Fourvec Top_Decaykin::hadw (const Lepjets_Event& ev)
//
// Purpose: Sum up the appropriate 4-vectors to find the hadronic W.
//
// Inputs:
//   ev -          The event.
//
// Returns:
//   The hadronic W.
// 
{
  return (ev.sum (hadw1_label) + ev.sum (hadw2_label));
}


Fourvec Top_Decaykin::lepw (const Lepjets_Event& ev)
//
// Purpose: Sum up the appropriate 4-vectors to find the leptonic W.
//
// Inputs:
//   ev -          The event.
//
// Returns:
//   The leptonic W.
// 
{
  return (leptons (ev) + ev.met ());
}


Fourvec Top_Decaykin::hadt (const Lepjets_Event& ev)
//
// Purpose: Sum up the appropriate 4-vectors to find the hadronic t.
//
// Inputs:
//   ev -          The event.
//
// Returns:
//   The hadronic t.
// 
{
  return (ev.sum (hadb_label) + hadw (ev));
}


Fourvec Top_Decaykin::lept (const Lepjets_Event& ev)
//
// Purpose: Sum up the appropriate 4-vectors to find the leptonic t.
//
// Inputs:
//   ev -          The event.
//
// Returns:
//   The leptonic t.
// 
{
  return (ev.sum (lepb_label) + lepw (ev));
}


ostream& Top_Decaykin::dump_ev (ostream& s, const Lepjets_Event& ev)
//
// Purpose: Print kinematic information for EV.
//
// Inputs:
//   s -           The stream to which to write.
//   ev -          The event to dump.
//
// Returns:
//   The stream S.
//
{
  s << ev;
  Fourvec p;

  p = lepw (ev);
  s << "lepw " << p << " " << p.m() << "\n";
  p = lept (ev);
  s << "lept " << p << " " << p.m() << "\n";
  p = hadw (ev);
  s << "hadw " << p << " " << p.m() << "\n";
  p = hadt (ev);
  s << "hadt " << p << " " << p.m() << "\n";

  return s;
}


} // namespace hitfit


