//
// $Id: Fit_Result.cpp,v 1.2 2007-07-03 16:19:52 erik Exp $
//
// File: src/Fit_Result.cpp
// Purpose: Hold the result from a single kinematic fit.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//

#include "hitfit/Fit_Result.hpp"
#include <ostream>
#include <cmath>


using std::ostream;
using std::abs;


namespace hitfit {


Fit_Result::Fit_Result (double chisq,
                        const Lepjets_Event& ev,
                        const Column_Vector& pullx,
                        const Column_Vector& pully,
                        double umwhad,
                        double utmass,
                        double mt,
                        double sigmt,
                        const Column_Vector&  pars,
                        const Column_Vector&  errs,
                        const Column_Vector&  steps,
                        const Column_Vector&  chiterms )
//
// Purpose: Constructor.
//
// Inputs:
//   chisq -       The fit chisq.
//   ev -          The event kinematics.
//   pullx -       The pull quantities for the well-measured variables.
//   pully -       The pull quantities for the poorly-measured variables.
//   umwhad -      The hadronic W mass before the fit.
//   utmass -      The top quark mass before the fit.
//   mt -          The top quark mass after the fit.
//   sigmt -       The top quark mass uncertainty after the fit.
//   pars  -       The fitted parameters, such as jet-scales, but also the
//                 lambda's which turn on/off the constraints.
//   errs  -       The errors (after the fit) on fitted parameters
//   steps -       Stepsizes for the to be fitted parameters:
//                 0 means fixed, non-0 belongs to a fitted parameter 
//   chiterms -    The different individual terms in the chi^2
//
  : m_chisq (chisq),
    m_umwhad (umwhad),
    m_utmass (utmass),
    m_mt (mt),
    m_sigmt (sigmt),
    m_pullx (pullx),
    m_pully (pully),
    m_ev (ev),
    m_pars (pars),
    m_errs (errs),
    m_steps (steps),
    m_chiterms (chiterms)
{
}


double Fit_Result::chisq () const
//
// Purpose: Return the fit chisq.
//
// Returns:
//   Return the fit chisq.
//
{
  return m_chisq;
}


double Fit_Result::umwhad () const
//
// Purpose: Return the hadronic W mass before the fit.
//
// Returns:
//   The hadronic W mass before the fit.
//
{
  return m_umwhad;
}


double Fit_Result::utmass () const
//
// Purpose: Return the top mass before the fit.
//
// Returns:
//   The top mass before the fit.
//
{
  return m_utmass;
}


double Fit_Result::mt () const
//
// Purpose: Return the top mass after the fit.
//
// Returns:
//   The top mass after the fit.
//
{
  return m_mt;
}


double Fit_Result::sigmt () const
//
// Purpose: Return the top mass uncertainty after the fit.
//
// Returns:
//   The top mass uncertainty after the fit.
//
{
  return m_sigmt;
}


const Column_Vector& Fit_Result::pullx () const
//
// Purpose: Return the pull quantities for the well-measured variables.
//
// Returns:
//   The pull quantities for the well-measured variables.
//
{
  return m_pullx;
}


const Column_Vector& Fit_Result::pully () const
//
// Purpose: Return the pull quantities for the poorly-measured variables.
//
// Returns:
//   The pull quantities for the poorly-measured variables.
//
{
  return m_pully;
}


const Lepjets_Event& Fit_Result::ev () const
//
// Purpose: Return the event kinematic quantities after the fit.
//
// Returns:
//   The event kinematic quantities after the fit.
//
{
  return m_ev;
}


///////////////////////////////--
// MinuitTopFit output
///////////////////////////////--
const Column_Vector& Fit_Result::pars () const
//
//
{
  return m_pars;
}

const Column_Vector& Fit_Result::errs () const
//
//          
{
  return m_errs;
}

const Column_Vector& Fit_Result::steps () const
//
//          
{
  return m_steps;
}

const Column_Vector& Fit_Result::chiterms () const
//
//          
{
  return m_chiterms;
}

double Fit_Result::pars (int i) const
//
//
{
  if (i>m_pars.num_row()) return 0.0;
  return m_pars(i);
}

double Fit_Result::errs (int i) const
//
//
{
  if (i>m_errs.num_row()) return 0.0;
  return m_errs(i);
}

double Fit_Result::steps (int i) const
//
//
{
  if (i>m_steps.num_row()) return 0.0;
  return m_steps(i);
}

double Fit_Result::chiterms (int i) const
//
//
{
  if (i>m_chiterms.num_row()) return 0.0;
  return m_chiterms(i);
}




bool operator< (const Fit_Result& a, const Fit_Result& b)
//
// Purpose: Compare two objects by chisq.
//          The use of abs() is to make sure that the -1000 flags
//          go to the end.
//
// Inputs:
//   a -           The first object to compare.
//   b -           The second object to compare.
//
// Returns:
//   The result of the comparison.
//
{
  return abs (a.m_chisq) < abs (b.m_chisq);
}


ostream& operator<< (ostream& s, const Fit_Result& res)
//
// Purpose: Print the object to S.
//
// Inputs:
//   s -           The stream to which to write.
//   res -         The object to write.
//
// Returns:
//   The stream S.
//
{
  s << "chisq: "  << res.m_chisq  << "\n";
  s << "umwhad: " << res.m_umwhad << "\n";
  s << "utmass: " << res.m_utmass << "\n";
  s << "mt: "     << res.m_mt     << "\n";
  s << "sigmt: "  << res.m_sigmt << "\n";
  s << res.m_ev;
  s << "pullx: " << res.m_pullx.T();
  s << "pully: " << res.m_pully.T();
  return s;
}


} // namespace hitfit
