//
// $Id: Objpair.cpp,v 1.1 2006-10-05 08:29:21 chriss Exp $
//
// File: src/Objpair.cpp
// Purpose: Helper class for Pair_Table.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//

#include "hitfit/private/Objpair.hpp"
#include <ostream>
#include <cassert>


using std::ostream;


namespace hitfit {


Objpair::Objpair (int i, int j, int nconstraints)
//
// Purpose: Constructor.
//
// Inputs:
//   i -           The first object index.
//   j -           The second object index.
//   nconstraints- The number of constraints in the problem.
//
  : m_i (i),
    m_j (j),
    m_for_constraint (nconstraints)
{
}


void Objpair::has_constraint (int k, int val)
//
// Purpose: Set the value for constraint K (0-based) to VAL.
//
// Inputs:
//   k -           The constraint number (0-based).
//   val -         The value to set for this constraint.
//
{
  assert (k >= 0 && (size_t)k < m_for_constraint.size());
  m_for_constraint[k] = static_cast<signed char> (val);
}


ostream& operator<< (ostream& s, const Objpair& o)
//
// Purpose: Print the object to S.
//
// Inputs:
//   s -           The stream to which to write.
//   o -           The object to write.
//
// Returns:
//   The stream S.
//
{
  s << o.m_i << " " << o.m_j;
  for (unsigned k = 0; k < o.m_for_constraint.size(); ++k)
    s << " " << static_cast<int> (o.m_for_constraint[k]);
  return s;
}


} // namespace hitfit
