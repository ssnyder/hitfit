//
// $Id: Refcount.cpp,v 1.1 2006-10-05 08:29:21 chriss Exp $
//
// File: Refcount.cpp
// Purpose: Reference count implementation.
// Created: Aug 2000, sss, from the version that used to be in d0om.
//

#include "hitfit/Refcount.hpp"


namespace hitfit {


void Refcount::nuke_refcount ()
//
// Purpose: Reset the refcount to zero.
//          This should only be used in the context of a dtor of a derived
//          class which wants to throw an exception.
//
{
  m_refcount = 0;
}


}
