//
// $Id: hitfit_topfit.cpp,v 1.1 2007-07-04 15:37:14 erik Exp $
//
// File: bin/hitfit_topfit.cpp
// Purpose: ttbar mass fitter.
// Created: Aug, 2000, sss.
//

#include "hitfit/Top_Fit.hpp"
#include "hitfit/Top_Decaykin.hpp"
#include "hitfit/run1_ascii_io.hpp"
#include "hitfit/Lepjets_Event.hpp"
#include "hitfit/Fit_Results.hpp"
#include "hitfit/Fit_Result.hpp"
#include "hitfit/Defaults_Text.hpp"

#include <TTree.h>
#include <TFile.h>

#include "CLHEP/Random/JamesRandom.h"
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <cassert>
#include <unistd.h> // for unlink().


using std::cerr;
using std::cout;
using std::ifstream;
using std::string;
using std::min;
using std::memset;
using std::sqrt;
using hitfit::Defaults_Text;
using hitfit::Defaults;
using hitfit::Top_Fit_Args;
using hitfit::Top_Fit;
using hitfit::Top_Decaykin;
using hitfit::Fit_Results;
using hitfit::Fit_Result_Vec;
using hitfit::Fit_Result;
using hitfit::Lepjets_Event;
using hitfit::Lepjets_Event_Jet;
using hitfit::Fourvec;
using hitfit::Run1_Ascii_IO_Args;
using hitfit::adjust_e_for_mass;
using hitfit::read_run1_ascii;
using CLHEP::HepJamesRandom;


HepJamesRandom engine;

const int n_book_measured = 23;
const int n_book_unmeasured = 1;

template <int N = 1>
struct fourvec_ntuple
{
  float pt[N];
  float eta[N];
  float phi[N];
  float msq[N];
};


const int ntup_maxjets = 10;
struct lepjets_event_ntuple
{
  fourvec_ntuple<> l;
  fourvec_ntuple<> nu;
  int njetsn;
  fourvec_ntuple<ntup_maxjets> jets;
  int j_typ[ntup_maxjets];
  float j_tagpt[ntup_maxjets];
  int j_svx[ntup_maxjets];
  float kt_x;
  float kt_y;
  float mtot;
  float dlb;
  float dnn;
};


template <int N = 1>
struct fitres_ntuple
{
  int nperms;
  float chisq[N];
  float mt[N];
  float sigmt[N];
  float umwhad[N];
  float utmass[N];
  int perm[N];
  float m_isr[N];
  float m_h[N];
  float m_tt[N];
  float pt_t[N];
  float kt[N];
  float eta_t1[N];
  float eta_t2[N];
  float phi_t1[N];
  float phi_t2[N];
  float pt_t1[N];
  float pt_t2[N];
};


const int ntup_maxperm = 23;
struct ntuple
{
  int runno;
  int evonum;
  lepjets_event_ntuple before;
  lepjets_event_ntuple after;
  fitres_ntuple<ntup_maxperm> all;
  fitres_ntuple<ntup_maxperm> noperm;
  fitres_ntuple<ntup_maxperm> semicorrect;
  fitres_ntuple<ntup_maxperm> limited_isr;
  fitres_ntuple<ntup_maxperm> topfour;
  fitres_ntuple<ntup_maxperm> btag;
  fitres_ntuple<ntup_maxperm> htag;
  float mpull[n_book_measured];
  float upull[n_book_unmeasured];
};

/*
template <class T>
void setup_branch (TTree* hepTree,
                   string name,
                   T* var,
                   int n = 1,
                   string index = "",
		   string branchObjectType
		   )
{
  string ifArray_extension = "";
 // bool isDimensionSet = false;
 // if (hepTree->index > 1) isDimensionSet = true;
  
  if (n > 1) {
    // break;
//     if (!isDimensionSet) {
 //       hepTree->Branch(index.c_str(), &n , (index+"/I").c_str() ) ;
  //   } 
 //    ifArray_extension = "["+index+"]";
  } 
  
  string leafName = name + ifArray_extension + branchObjectType;
  hepTree->Branch(name.c_str(), var, leafName.c_str() );
  
}
*/

template <int N>
void setup_fourvec_ntuple (string prefix,
                           TTree* hepTree,
                           fourvec_ntuple<N>& ntuple,
                           string index = "")
{
  if (N>1) {
     hepTree->Branch( (prefix + "_pt").c_str(),  ntuple.pt,  (prefix+"_pt["+index+"]/F").c_str() );
     hepTree->Branch( (prefix + "_eta").c_str(), ntuple.eta, (prefix+"_eta["+index+"]/F").c_str() );  
     hepTree->Branch( (prefix + "_phi").c_str(), ntuple.phi, (prefix+"_phi["+index+"]/F").c_str() );
     hepTree->Branch( (prefix + "_msq").c_str(), ntuple.msq, (prefix+"_msq["+index+"]/F").c_str() );
  } else {
     hepTree->Branch( (prefix + "_pt").c_str(),  ntuple.pt,  (prefix+"_pt[1]/F").c_str() );
     hepTree->Branch( (prefix + "_eta").c_str(), ntuple.eta, (prefix+"_eta[1]/F").c_str() );  
     hepTree->Branch( (prefix + "_phi").c_str(), ntuple.phi, (prefix+"_phi[1]/F").c_str() );
     hepTree->Branch( (prefix + "_msq").c_str(), ntuple.msq, (prefix+"_msq[1]/F").c_str() );
  }

}


void setup_lepjets_event_ntuple (string prefix_base,
                                 string prefix_end,
                                 TTree* hepTree,
                                 lepjets_event_ntuple& ntuple)
{

  string prefix = prefix_base + prefix_end;

  setup_fourvec_ntuple (prefix + "_lepton",  hepTree, ntuple.l);
  setup_fourvec_ntuple (prefix + "_nu", hepTree, ntuple.nu);

  string iname = prefix + "_njetsn";

  hepTree->Branch( iname.c_str(), &ntuple.njetsn, (iname+"/I").c_str() );

/*    
  int Nlong = ntup_maxjets;  
  std::cout << "Nlong = " << Nlong << std::endl;
  std::cout << "de string 'iname' = " << iname << std::endl;
  if ( hepTree->GetBranchStatus(iname_end.c_str()) == 0) {
     std::cout << "Setting branch " << iname_end << " at Nlong = " << ntup_maxjets << std::endl;   
     hepTree->Branch(iname_end.c_str(), &Nlong, (iname_end+"/I").c_str());
  }  */

  setup_fourvec_ntuple (prefix+"_j", hepTree, ntuple.jets, iname);

  if (ntuple.njetsn > 1){
     hepTree->Branch( (prefix+"_j_typ").c_str(),  ntuple.j_typ ,  (prefix+"_j_typ["+iname+"]/I").c_str() );
     hepTree->Branch( (prefix+"_j_tagpt").c_str(),ntuple.j_tagpt ,(prefix+"_j_tagpt["+iname+"]/F").c_str() );
     hepTree->Branch( (prefix+"_j_svx").c_str(),  ntuple.j_svx ,  (prefix+"_j_svx["+iname+"]/I").c_str() );
  } else {
     hepTree->Branch( (prefix+"_j_typ").c_str(),  ntuple.j_typ ,  (prefix+"_j_typ[1]/I").c_str() );
     hepTree->Branch( (prefix+"_j_tagpt").c_str(),ntuple.j_tagpt ,(prefix+"_j_tagpt[1]/F").c_str() );
     hepTree->Branch( (prefix+"_j_svx").c_str(),  ntuple.j_svx ,  (prefix+"_j_svx[1]/I").c_str() );
  }
  
  hepTree->Branch( (prefix+"_kt_x").c_str(), &ntuple.kt_x, ((prefix+"_kt_x/F").c_str() ));
  hepTree->Branch( (prefix+"_kt_y").c_str(), &ntuple.kt_y, ((prefix+"_kt_y/F").c_str() ));
  hepTree->Branch( (prefix+"_mtot").c_str(), &ntuple.mtot, ((prefix+"_mtot/F").c_str() ));    
  hepTree->Branch( (prefix+"_dlb" ).c_str(), &ntuple.dlb,  ((prefix+"_dlb/F" ).c_str() ));
  hepTree->Branch( (prefix+"_dnn" ).c_str(), &ntuple.dnn,  ((prefix+"_dnn/F" ).c_str() ));
  
}



template <int N>
void setup_fitres_ntuple (string prefix_base,
                          string prefix_end,
			  TTree* hepTree,
                          fitres_ntuple<N>& ntuple)
{

  string prefix = prefix_base + prefix_end;

  string iname = prefix + "_nperms";
//  string iname_end = prefix_end + "_nperms";

  hepTree->Branch( iname.c_str() , &ntuple.nperms, (iname+"/I").c_str() ) ;

  if (ntuple.nperms > 1) {
     hepTree->Branch( (prefix+"_chisq" ).c_str()   , ntuple.chisq,  (prefix+"_chisq["+iname+"]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_mt"    ).c_str()   , ntuple.mt   ,  (prefix+"_mt["+iname+"]/F").c_str() ) ;  
     hepTree->Branch( (prefix+"_sigmt" ).c_str()   , ntuple.sigmt , (prefix+"_sigmt["+iname+"]/F").c_str() ) ; 
     hepTree->Branch( (prefix+"_umwhad").c_str()   , ntuple.umwhad, (prefix+"_umwhad["+iname+"]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_utmass").c_str()   , ntuple.utmass, (prefix+"_utmass["+iname+"]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_perm"  ).c_str()   , ntuple.perm,   (prefix+"_perm["+iname+"]/I").c_str() ) ;     
     hepTree->Branch( (prefix+"_m_isr" ).c_str()   , ntuple.m_isr,  (prefix+"_m_isr["+iname+"]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_m_h"   ).c_str()   , ntuple.m_h,    (prefix+"_m_h["+iname+"]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_m_tt"  ).c_str()   , ntuple.m_tt,   (prefix+"_m_tt["+iname+"]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_pt_t"  ).c_str()   , ntuple.pt_t,   (prefix+"_pt_t["+iname+"]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_kt"    ).c_str()   , ntuple.kt,     (prefix+"_kt["+iname+"]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_eta_t1").c_str()   , ntuple.eta_t1, (prefix+"_eta_t1["+iname+"]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_eta_t2").c_str()   , ntuple.eta_t2, (prefix+"_eta_t2["+iname+"]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_phi_t1").c_str()   , ntuple.phi_t1, (prefix+"_phi_t1["+iname+"]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_phi_t2").c_str()   , ntuple.phi_t2, (prefix+"_phi_t2["+iname+"]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_pt_t1" ).c_str()   , ntuple.pt_t1,  (prefix+"_pt_t1["+iname+"]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_pt_t2" ).c_str()   , ntuple.pt_t2,  (prefix+"_pt_t2["+iname+"]/F").c_str() ) ;
  } else {   
     hepTree->Branch( (prefix+"_chisq" ).c_str()   , ntuple.chisq,  (prefix+"_chisq[1]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_mt"    ).c_str()   , ntuple.mt   ,  (prefix+"_mt[1]/F").c_str() ) ;  
     hepTree->Branch( (prefix+"_sigmt" ).c_str()   , ntuple.sigmt , (prefix+"_sigmt[1]/F").c_str() ) ; 
     hepTree->Branch( (prefix+"_umwhad").c_str()   , ntuple.umwhad, (prefix+"_umwhad[1]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_utmass").c_str()   , ntuple.utmass, (prefix+"_utmass[1]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_perm"  ).c_str()   , ntuple.perm,   (prefix+"_perm[1]/I").c_str() ) ;     
     hepTree->Branch( (prefix+"_m_isr" ).c_str()   , ntuple.m_isr,  (prefix+"_m_isr[1]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_m_h"   ).c_str()   , ntuple.m_h,    (prefix+"_m_h[1]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_m_tt"  ).c_str()   , ntuple.m_tt,   (prefix+"_m_tt[1]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_pt_t"  ).c_str()   , ntuple.pt_t,   (prefix+"_pt_t[1]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_kt"    ).c_str()   , ntuple.kt,     (prefix+"_kt[1]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_eta_t1").c_str()   , ntuple.eta_t1, (prefix+"_eta_t1[1]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_eta_t2").c_str()   , ntuple.eta_t2, (prefix+"_eta_t2[1]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_phi_t1").c_str()   , ntuple.phi_t1, (prefix+"_phi_t1[1]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_phi_t2").c_str()   , ntuple.phi_t2, (prefix+"_phi_t2[1]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_pt_t1" ).c_str()   , ntuple.pt_t1,  (prefix+"_pt_t1[1]/F").c_str() ) ;
     hepTree->Branch( (prefix+"_pt_t2" ).c_str()   , ntuple.pt_t2,  (prefix+"_pt_t2[1]/F").c_str() ) ;
  } 
}



void setup_ntuple (TTree* hepTree, ntuple& ntuple)
{

  hepTree->Branch( "TopFit_runno" , &ntuple.runno,  "TopFit_runno/I"  );
  hepTree->Branch( "TopFit_evonum", &ntuple.evonum, "TopFit_evonum/I" );

  setup_lepjets_event_ntuple ("TopFit","_before", hepTree, ntuple.before);
  setup_lepjets_event_ntuple ("TopFit","_after", hepTree, ntuple.after);

  setup_fitres_ntuple ("TopFit", "_All",  hepTree, ntuple.all);
  setup_fitres_ntuple ("TopFit", "_Noperm", hepTree, ntuple.noperm);
  setup_fitres_ntuple ("TopFit", "_Semicorrect", hepTree, ntuple.semicorrect);
  setup_fitres_ntuple ("TopFit", "_Limited_isr", hepTree, ntuple.limited_isr);
  setup_fitres_ntuple ("TopFit", "_Topfour", hepTree, ntuple.topfour);
  setup_fitres_ntuple ("TopFit", "_Btag", hepTree, ntuple.btag);
  setup_fitres_ntuple ("TopFit", "_Htag", hepTree, ntuple.htag);

  hepTree->Branch( "TopFit_mpull", ntuple.mpull, "TopFit_mpull[23]/F" );   // 23=n_book_measured
  hepTree->Branch( "TopFit_upull", ntuple.upull, "TopFit_upull[1]/F" );    // 1 =n_book_unmeasured 
}


template <int N>
void fill_fourvec (const Fourvec& v,
                   fourvec_ntuple<N>& ntuple,
                   int i = 0)
{
  ntuple.pt[i]  = v.perp();
  ntuple.phi[i] = v.phi();
  ntuple.eta[i] = v.pseudoRapidity();
  ntuple.msq[i] = v.m2();
}



void fill_lepjets_event (const Lepjets_Event& ev,
                         lepjets_event_ntuple& ntuple)
{
  if (ev.nleps() > 0)
    fill_fourvec (ev.lep(0).p(), ntuple.l);
  fill_fourvec (ev.met(), ntuple.nu);

  ntuple.njetsn = min (ntup_maxjets, ev.njets());
  for (int i=0; i < ntuple.njetsn; i++) {
    const Lepjets_Event_Jet& j = ev.jet(i);
    fill_fourvec (j.p(), ntuple.jets, i);
    ntuple.j_typ[i] = j.type();
    ntuple.j_svx[i] = j.svx_tag() ? 1 : 0;
    ntuple.j_tagpt[i] = j.slt_tag() ? j.tag_lep().perp() : 0;
  }
  Fourvec kt = ev.kt();

  ntuple.kt_x = kt.x();
  ntuple.kt_y = kt.y();
  ntuple.mtot = kt.m();
  ntuple.dlb = ev.dlb();
  ntuple.dnn = ev.dnn();
}


int pack_perm (const Lepjets_Event& ev)
{
  int packed = 0;
  for (int i=0; i < ev.njets(); i++)
    packed = ((packed << 4) | (ev.jet(i).type() & 0xf));
  return packed;
}


template <int N>
void fill_fitres (const Fit_Result& res,
                  fitres_ntuple<N>& ntuple,
                  int i)
{
  ntuple.chisq[i] = res.chisq ();
  ntuple.mt[i] = res.mt();
  ntuple.sigmt[i] = res.sigmt();
  ntuple.umwhad[i] = res.umwhad();
  ntuple.utmass[i] = res.utmass();

  const Lepjets_Event& ev = res.ev();
  ntuple.perm[i] = pack_perm (ev);

  Fourvec isr = ev.sum (hitfit::isr_label);
  Fourvec higgs = ev.sum (hitfit::higgs_label);
  Fourvec hadt = Top_Decaykin::hadt (ev);
  Fourvec lept = Top_Decaykin::lept (ev);
  ntuple.m_isr[i] = isr . m();
  ntuple.m_h[i] = higgs . m();
  ntuple.m_tt[i] = (hadt + lept) . m ();
  ntuple.pt_t[i] = (hadt.perp() + lept.perp()) / 2;
  ntuple.eta_t1[i] = lept.pseudoRapidity();
  ntuple.eta_t2[i] = hadt.pseudoRapidity();
  ntuple.phi_t1[i] = lept.phi();
  ntuple.phi_t2[i] = hadt.phi();
  ntuple.pt_t1[i]  = lept.perp();
  ntuple.pt_t2[i]  = hadt.perp();
  ntuple.kt[i] =     ev.kt().perp();
}


template <int N>
void fill_fitres_vec (const Fit_Result_Vec& resvec,
                      fitres_ntuple<N>& ntuple)
{
  ntuple.nperms = min (N, resvec.size ());
  for (int i=0; i < ntuple.nperms; i++)
    fill_fitres (resvec[i], ntuple, i);
}


void do_scale_jets (Lepjets_Event& ev,
                    const Defaults& defs)
{
  static bool gotparms = false;
  static float scale[3];

  if (!gotparms) {
    scale[0] = defs.get_float ("scale0");
    scale[1] = defs.get_float ("scale1");
    scale[2] = defs.get_float ("scale2");
    gotparms = true;
  }

  for (int j=0; j<ev.njets(); j++) {
    double et = ev.jet(j).p().perp();
    double newet = scale[0] + scale[1]*et + scale[2]*et*et;
    if (newet < 5) // ??? XXX
      newet = 5;
    ev.jet(j).p() *= (newet / et);
  }
}


int main (int argc, char** argv)
{
  const char* fname = 0;
  for (int i=1; i < argc; i++) {
    if (argv[i][0] != '-') {
      fname = argv[i];
      break;
    }
  }

  if (!fname) {
    cerr << "no filename\n";
    exit (1);
  }

  Defaults_Text defs ("defaults", argc, argv);

  engine.setSeed (defs.get_int ("seed"));

  double lepw_mass  = defs.get_float ("lepw_mass");
  double hadw_mass  = defs.get_float ("hadw_mass");
  double top_mass   = defs.get_float ("top_mass");
  int leading_jets  = defs.get_int ("leading_jets");
  double leppt_cut  = defs.get_float ("leppt_cut");
  double jetpt_cut  = defs.get_float ("jetpt_cut");
  double lepeta_cut = defs.get_float ("lepeta_cut");
  double jeteta_cut = defs.get_float ("jeteta_cut");
  double nupt_cut   = defs.get_float ("nupt_cut");
  bool smear        = defs.get_bool ("smear");
  bool print_input_event = defs.get_bool ("print_input_event");
  bool print_ideogram_info = defs.get_bool ("print_ideogram_info");
  string ntup_fname = defs.get_string ("ntup_fname");
  int nev = defs.get_int ("nev");
  bool scale_jets   = defs.get_bool ("scale_jets");

  Top_Fit_Args args (defs);
  Top_Fit fitter (args, lepw_mass, hadw_mass, top_mass);

  ifstream in (fname);
  if (! in.is_open()) {
    cerr << "Can't open input file " << fname << "\n";
    exit (1);
  }

  //  if (defs.get_bool ("FSRfit") && defs.get_bool ("do_higgs_flag")) {
  //  cerr << "Don't know how to handle FSRfit AND d0_higgs_flag Combined !! Abort!  \n" << fname << "\n";
  //  exit (1);
  //}


  //
  // The following creates a root-file where the results will be saved
  //
  TFile *fitFile = new TFile(ntup_fname.c_str(), "RECREATE");
  TTree *hepTree = new TTree("Topfit", "Results from the hitfit_topfit");
  struct ntuple ntuple;

  setup_ntuple (hepTree, ntuple);

  Run1_Ascii_IO_Args io_args (defs);
  while (1) {
    cout << "\n";
    Lepjets_Event ev = read_run1_ascii (in, io_args);
    if (ev.evnum() < 0)
      break;

    if (!print_ideogram_info) {
      cout << ev.evnum() << "\n";
      cerr << ev.evnum() << "\n";
    }

    if (smear)
      ev.smear (engine, true);

    {
      double nuz1, nuz2;
      Top_Decaykin::solve_nu (ev, lepw_mass, nuz1, nuz2);
      ev.met().setZ (nuz1);
      adjust_e_for_mass (ev.met(), 0);
      for (int i=0; i < ev.nleps(); i++)
        adjust_e_for_mass (ev.lep(i).p(), 0);
    }

    ev.sort ();

    if (print_input_event)
      cout << ev;

    memset (&ntuple, 0, sizeof (ntuple));

    ntuple.runno  = ev.runnum ();
    ntuple.evonum = ev.evnum ();

    fill_lepjets_event (ev, ntuple.before);

    if (ev.cut_leps (leppt_cut, lepeta_cut) < 1 ||
        ev.cut_jets (jetpt_cut, jeteta_cut) < 4 ||
        ev.met().perp() < nupt_cut ||
        ev.nleps() > 1)
    {
      cout << "failed kin cuts\n";
      cout << 0 << "   is the number of solutions for this event" << "\n" ;
    } else {
      if (scale_jets) {
        do_scale_jets (ev, defs);
        ev.sort ();
        fill_lepjets_event (ev, ntuple.before);
      }

      if (leading_jets > 0) {
        assert (leading_jets >= 4);
        ev.trimjets (leading_jets);
      }
      if (print_ideogram_info) { 
	int no_of_combinations = 12;
	if (ev.cut_jets (jetpt_cut, jeteta_cut) > 4) 
	  no_of_combinations = defs.get_bool ("FSRfit") ? 140 : 60;
	if (defs.get_bool ("do_write_bothnu_flag")) no_of_combinations*=2;
	// print event header needed for Run II Ideogram analysis
	cout << no_of_combinations << " is the number of solutions for this event" << "\n" ;
      }

      Fit_Results fitres = fitter.fit (ev);
      if (fitres[hitfit::all_list].size() > 0) {
	if (!print_ideogram_info) { 
	  // print traditional output for backwards compatibility
	  cout << fitres[hitfit::all_list][0].chisq()
	       << " " << fitres[hitfit::all_list][0].mt()
	       << " " << fitres[hitfit::all_list][0].sigmt()
	       << "\n";
	}
        // XXX pay attention to b-tags, etc?
        fill_lepjets_event (fitres[hitfit::all_list][0].ev(), ntuple.after);

        fill_fitres_vec (fitres[hitfit::all_list],         ntuple.all);
        fill_fitres_vec (fitres[hitfit::noperm_list],      ntuple.noperm);
        fill_fitres_vec (fitres[hitfit::semicorrect_list], ntuple.semicorrect);
        fill_fitres_vec (fitres[hitfit::limited_isr_list], ntuple.limited_isr);
        fill_fitres_vec (fitres[hitfit::topfour_list],     ntuple.topfour);
        fill_fitres_vec (fitres[hitfit::btag_list],        ntuple.btag);
        fill_fitres_vec (fitres[hitfit::htag_list],        ntuple.htag);
/*
        if (fitres[hitfit::noperm_list].size() > 0)
          hist_pull_functions (manager,
                               fitres[hitfit::noperm_list][0],
                               ntuple);
*/

      }
    }

    hepTree->Fill();

    if (--nev == 0) break;
  }
 
  hepTree->Print();  
  fitFile->Write();
  delete fitFile;

}


