//
// $Id: Constrained_Z.cpp,v 1.1 2006-10-05 08:29:19 chriss Exp $
//
// File: Constrained_Z.cpp
// Purpose: Do kinematic fitting for a (Z->ll)+jets event.
// Created: Apr, 2004, sss
//

#include "hitfit/Constrained_Z.hpp"
#include "hitfit/Fourvec_Event.hpp"
#include "hitfit/Lepjets_Event.hpp"
#include "hitfit/Defaults.hpp"
#include <ostream>
#include <stdio.h>
#include <assert.h>

namespace hitfit {


//*************************************************************************
// Argument handling.
//


Constrained_Z_Args::Constrained_Z_Args (const Defaults& defs)
//
// Purpose: Constructor.
//
// Inputs:
//   defs -        The Defaults instance from which to initialize.
//
  : m_zmass (defs.get_float ("zmass")),
    m_fourvec_constrainer_args (defs)
{
}


double Constrained_Z_Args::zmass () const
//
// Purpose: Return the zmass parameter.
//          See the header for documentation.
//
{
  return m_zmass;
}


const Fourvec_Constrainer_Args&
Constrained_Z_Args::fourvec_constrainer_args () const
//
// Purpose: Return the contained subobject parameters.
//
{
  return m_fourvec_constrainer_args;
}


//*************************************************************************


Constrained_Z::Constrained_Z (const Constrained_Z_Args& args)
//
// Purpose: Constructor.
//
// Inputs:
//   args -        The parameter settings for this instance.
//   
  : m_args (args),
    m_constrainer (args.fourvec_constrainer_args())
{
  char buf[256];
  sprintf (buf, "(%d) = %f", lepton_label, m_args.zmass());
  m_constrainer.add_constraint (buf);
}


namespace {


FE_Obj make_fe_obj (const Lepjets_Event_Lep& obj, double mass, int type)
//
// Purpose: Helper to create an object to put into the Fourvec_Event.
//
// Inputs:
//   obj -         The input object.
//   mass -        The mass to which it should be constrained.
//   type -        The type to assign it.
//
// Returns:
//   The constructed FE_Obj.
//
{
  return FE_Obj (obj.p(), mass, type,
                 obj.p_sigma(), obj.eta_sigma(), obj.phi_sigma(),
                 obj.res().p_res().inverse());
}


void do_import (const Lepjets_Event& ev, Fourvec_Event& fe)
//
// Purpose: Convert from a Lepjets_Event to a Fourvec_Event.
//
// Inputs:
//   ev -          The input event.
//
// Outputs:
//   fe -          The initialized Fourvec_Event.
//
{
  assert (ev.nleps() >= 2);
  fe.add (make_fe_obj (ev.lep(0), 0, lepton_label));
  fe.add (make_fe_obj (ev.lep(1), 0, lepton_label));

  for (int j=0; j < ev.njets(); j++)
    fe.add (make_fe_obj (ev.jet(j), 0, isr_label));

  Fourvec kt = ev.kt ();
  fe.set_kt_error (ev.kt_res().sigma (kt.x()),
                   ev.kt_res().sigma (kt.y()),
                   0);
  fe.set_x_p (ev.met());
}


void do_export (const Fourvec_Event& fe, Lepjets_Event& ev)
//
// Purpose: Convert from a Fourvec_Event to a Lepjets_Event.
//
// Inputs:
//   fe -          The input event.
//   ev -          The original Lepjets_Event.
//
// Outputs:
//   ev -          The updated Lepjets_Event.
//
{
  ev.lep(0).p() = fe.obj(0).p;
  ev.lep(1).p() = fe.obj(1).p;

  for (int j=0, k=1; j < ev.njets(); j++)
    ev.jet(j).p() = fe.obj(k++).p;

  Fourvec nu;
  ev.met() = nu;
}


} // unnamed namespace


double Constrained_Z::constrain (Lepjets_Event& ev, Column_Vector& pull)
//
// Purpose: Do a constrained fit for EV.
//          Returns the pull quantities in PULL.
//          Returns the chisq; this will be < 0 if the fit failed
//          to converge.
//
// Inputs:
//   ev -          The event we're fitting.
//
// Outputs:
//   ev -          The fitted event.
//   pull -        Pull quantities for well-measured variables.
//
// Returns:
//   The fit chisq, or < 0 if the fit didn't converge.
//
{
  Fourvec_Event fe;
  do_import (ev, fe);
  Column_Vector pully;
  double m, sigm;
  double chisq = m_constrainer.constrain (fe, m, sigm, pull, pully);
  do_export (fe, ev);

  return chisq;
}


std::ostream& operator<< (std::ostream& s, const Constrained_Z& cz)
//
// Purpose: Print the object to S.
//
// Inputs:
//   s -           The stream to which to write.
//   cz -          The object to write.
//
// Returns:
//   The stream S.
//
{
  return s << cz.m_constrainer;
}


} // namespace hitfit
