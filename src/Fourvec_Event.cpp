//
// $Id: Fourvec_Event.cpp,v 1.1 2006-10-05 08:29:21 chriss Exp $
//
// File: src/Fourvec_Event.cpp
// Purpose: Represent an event for kinematic fitting as a collection
//          of 4-vectors.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//


#include "hitfit/Fourvec_Event.hpp"
#include <cassert>
#include <ostream>


using std::ostream;


namespace hitfit {


FE_Obj::FE_Obj (Fourvec the_p,
                double the_mass,
                int the_label,
                double the_p_error,
                double the_phi_error,
                double the_eta_error,
                bool the_muon_p)
//
// Purpose: Contructor.
//
// Inputs:
//   the_p -       4-momentum.
//   the_mass -    The mass of the object.
//                 The constrained fit will fix the mass to this value.
//   the_p_error - Uncertainty in p (or, if the_muon_p is set, in 1/p).
//   the_phi_error-Uncertainty in phi.
//   the_eta_error-Uncertainty in eta.
//   the_muon_p -  If true, the `p' uncertainty is in 1/p, and 1/p
//                 should be used as the fit variable instead of p.
//
  : p (the_p),
    mass (the_mass),
    label (the_label),
    p_error (the_p_error),
    phi_error (the_phi_error),
    eta_error (the_eta_error),
    muon_p (the_muon_p)
{
}


ostream& operator<< (std::ostream& s, const FE_Obj& o)
//
// Purpose: Print the object to S.
//
// Inputs:
//   s -           The stream to which to write.
//   o -           The object to write.
//
// Returns:
//   The stream S.
//
{
  s << o.p << " - " << o.mass << " - " << o.label << "\n";
  s << "    errors: " << o.p_error << " " << o.phi_error << " " << o.eta_error;
  if (o.muon_p)
    s << " (mu)";
  s << "\n";
  return s;
}


//************************************************************************


Fourvec_Event::Fourvec_Event ()
//
// Purpose: Constructor.
//
  : m_kt_x_error (0),
    m_kt_y_error (0),
    m_kt_xy_covar (0),
    m_has_neutrino (false)
{
}


bool Fourvec_Event::has_neutrino () const
//
// Purpose: Return true if this event has a neutrino.
//
// Returns:
//   True if this event has a neutrino.
//
{
  return m_has_neutrino;
}


int Fourvec_Event::nobjs () const
//
// Purpose: Return the number of objects in the event, not including
//          any neutrino.
//
// Returns:
//   The number of objects in the event, not including any neutrino.
//
{
  return m_objs.size() - (m_has_neutrino ? 1 : 0);
}


int Fourvec_Event::nobjs_all () const
//
// Purpose: Return the number of objects in the event, including any neutrino.
//
// Returns:
//   The number of objects in the event, including any neutrino.
//
{
  return m_objs.size();
}


const FE_Obj& Fourvec_Event::obj (int i) const
//
// Purpose: Access object I.
//
// Inputs:
//   i -           The index of the desired object (0-based indexing).
//
{
  assert (i >= 0 && (size_t)i < m_objs.size ());
  return m_objs[i];
}


const Fourvec& Fourvec_Event::nu () const
//
// Purpose: Access the neutrino 4-vector.
//
{
  assert (m_has_neutrino);
  return m_objs.back().p;
}


const Fourvec& Fourvec_Event::kt () const
//
// Purpose: Access the kt 4-vector.
//
{
  return m_kt;
}


const Fourvec& Fourvec_Event::x () const
//
// Purpose: Access the X 4-vector.
//
{
  return m_x;
}


double Fourvec_Event::kt_x_error () const
//
// Purpose: Return the X uncertainty in kt.
//
// Returns:
//   The X uncertainty in kt.
//
{
  return m_kt_x_error;
}


double Fourvec_Event::kt_y_error () const
//
// Purpose: Return the Y uncertainty in kt.
//
// Returns:
//   The Y uncertainty in kt.
//
{
  return m_kt_y_error;
}


double Fourvec_Event::kt_xy_covar () const
//
// Purpose: Return the kt XY covariance.
//
// Returns:
//   The kt XY covariance.
//
{
  return m_kt_xy_covar;
}


ostream& operator<< (ostream& s, const Fourvec_Event& fe)
//
// Purpose: Print out the contents of the class.
//
// Inputs:
//   s -           The stream to which to write.
//   fe -          The object to write.
//
// Returns:
//   The stream S.
//
{
  s << "kt: (" << fe.m_kt.x() << ", " << fe.m_kt.y() << "); "
    << " error: " << fe.m_kt_x_error << " " << fe.m_kt_y_error << " "
    << fe.m_kt_xy_covar << "\n";
  s << "x: " << fe.m_x << "\n";
  for (unsigned i = 0; i < fe.m_objs.size(); i++)
    s << i+1 << ": " << fe.m_objs[i];
  return s;
}


void Fourvec_Event::add (const FE_Obj& obj)
//
// Purpose: Add an object to the event.
//
// Inputs:
//   obj -         The object to add.
//                 It should not be a neutrino.
//
{
  assert (obj.label != nu_label);

  // Add to the end of the list, but before any neutrino.
  if (m_has_neutrino) {
    assert (m_objs.size() > 0);
    m_objs.insert (m_objs.begin() + m_objs.size() - 1, obj);
  }
  else
    m_objs.push_back (obj);

  // Maintain kt.
  m_kt += obj.p;
}


void Fourvec_Event::set_nu_p (const Fourvec& p)
//
// Purpose: Set the neutrino 4-momentum to P.
//          This adds a neutrino if there wasn't one there already.
//
// Inputs:
//   p -           The new 4-momentum of the neutrino.
//
{
  if (m_has_neutrino) {
    m_kt -= m_objs.back().p;
    m_objs.back().p = p;
  }
  else {
    m_has_neutrino = true;
    m_objs.push_back (FE_Obj (p, 0, nu_label, 0, 0, 0, false));
  }

  m_kt += p;
}


void Fourvec_Event::set_obj_p (int i, const Fourvec& p)
//
// Purpose: Set object I's 4-momentum to P.
//
// Inputs:
//   i -           The index of the object to change (0-based indexing).
//   p -           The new 4-momentum.
//
{
  assert (i >= 0 && (size_t)i < m_objs.size ());
  m_kt -= m_objs[i].p;
  m_objs[i].p = p;
  m_kt += p;
}


void Fourvec_Event::set_x_p (const Fourvec& p)
//
// Purpose: Set the momentum of the X object to P.
//
// Inputs:
//   p -           The new 4-momentum.
//
{
  m_kt -= m_x;
  m_x = p;
  m_kt += p;
}


void Fourvec_Event::set_kt_error (double kt_x_error,
                                  double kt_y_error,
                                  double kt_xy_covar)
//
// Purpose: Set the kt uncertainties.
//
// Inputs:
//   kt_x_error -  The uncertainty in the X component of kt.
//   kt_y_error -  The uncertainty in the Y component of kt.
//   kt_xy_covar - The covariance between the X and Y components.
//
{
  m_kt_x_error = kt_x_error;
  m_kt_y_error = kt_y_error;
  m_kt_xy_covar = kt_xy_covar;
}


void Fourvec_Event::clear()
{
  m_objs.clear();
  m_kt = Fourvec();
  m_x = Fourvec();
  m_kt_x_error = 0;
  m_kt_y_error = 0;
  m_kt_xy_covar = 0;
  m_has_neutrino = false;
}


} // namespace hitfit

