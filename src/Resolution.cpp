//
// $Id: Resolution.cpp,v 1.1 2006-10-05 08:29:21 chriss Exp $
//
// File: src/Resolution.cpp
// Purpose: Calculate resolutions for a quantity.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//


#include "hitfit/Resolution.hpp"
#include "CLHEP/Random/RandGauss.h"
#include <cmath>
#include <iostream>
#include <cctype>
#include <cstdlib>


using std::sqrt;
using std::ostream;
using std::string;
using std::isspace;
using std::isdigit;
#ifndef __GNUC__
using std::atof;
#endif
using CLHEP::RandGauss;


namespace {


bool get_field (string s, string::size_type i, double& x)
//
// Purpose: Scan string S starting at position I.
//          Find the value of the first floating-point number we
//          find there.
//
// Inputs:
//   s -           The string to scan.
//   i -           Starting character position in the string.
//
// Outputs:
//   x -           The value of the number we found.
//
// Returns:
//   True if we found something that looks like a number, false otherwise.
//
{
  string::size_type j = i;
  while (j < s.size() && s[j] != ',' && !isdigit (s[j]) && s[j] != '.')
    ++j;
  if (j < s.size() && (isdigit (s[j]) || s[j] == '.')) {
    x = atof (s.c_str() + j);
    return true;
  }
  return false;
}


} // unnamed namespace


namespace hitfit {


Resolution::Resolution (std::string s /*= ""*/)
//
// Purpose: Constructor.
//
// Inputs:
//   s -           String encoding the resolution parameters, as described
//                 in the comments in the header.
//
{
  m_inverse = false;
  m_constant_sigma = 0;
  m_resolution_sigma = 0;
  m_noise_sigma = 0;

  // Skip spaces.
  double x;
  string::size_type i = 0;
  while (i < s.size() && isspace (s[i]))
    ++i;

  // Check for the inverse flag.
  if (s[i] == '-') {
    m_inverse = true;
    ++i;
  }
  else if (s[i] == '+') {
    ++i;
  }

  // Get the constant term.
  if (get_field (s, i, x)) m_constant_sigma = x;
  i = s.find (',', i);

  // Look for a resolution term.
  if (i != string::npos) {
    ++i;
    if (get_field (s, i, x)) m_resolution_sigma = x;

    // Look for a noise term.
    i = s.find (',', i);
    if (i != string::npos) {
      if (get_field (s, i+1, x)) m_noise_sigma = x;
    }
  }
}


Resolution::Resolution (double res,
                        bool inverse /*= false*/)
//
// Purpose: Constructor, to give a constant resolution.
//          I.e., sigma() will always return RES.
//
// Inputs:
//   res -         The resolution value.
//   inverse -     The inverse flag.
// 
  : m_constant_sigma (0),
    m_resolution_sigma (0),
    m_noise_sigma (res),
    m_inverse (inverse)
{
}


bool Resolution::inverse () const
//
// Purpose: Return the inverse flag.
//
// Returns:
//   The inverse flag.
//
{
  return m_inverse;
}


double Resolution::sigma (double p) const
//
// Purpose: Return the uncertainty for a momentum P.
//
// Inputs:
//    p -          The momentum
//
// Returns:
//   The uncertainty for a momentum P.
//
{
  if (m_inverse)
    p = 1 / p;

  return sqrt ((m_constant_sigma*m_constant_sigma*p +
		m_resolution_sigma*m_resolution_sigma)*p +
	       m_noise_sigma*m_noise_sigma);
}


double Resolution::pick (double x, double p, CLHEP::HepRandomEngine& engine) const
//
// Purpose: Given a value X, measured for an object with momentum P, 
//          pick a new value from a Gaussian distribution
//          described by this resolution --- with mean X and width sigma(P).
//
// Inputs:
//    x -          The quantity value (distribution mean).
//    p -          The momentum, for calculating the width.
//    engine -     The underlying RNG.
//
// Returns:
//   A smeared value of X.
//
{
  RandGauss gen (engine);
  if (m_inverse)
    return 1 / gen.fire (1 / x, sigma (p));
  else
    return gen.fire (x, sigma (p));
}


ostream& operator<< (ostream& s, const Resolution& r)
//
// Purpose: Dump this object to S.
//
// Inputs:
//    s -          The stream to which to write.
//    r -          The object to dump.
//
// Returns:
//   The stream S.
//   
{
  if (r.m_inverse) s << "-";
  s << r.m_constant_sigma << ","
    << r.m_resolution_sigma << ","
    << r.m_noise_sigma;
  return s;
}


} // namespace hitfit
