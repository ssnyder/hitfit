//
// $Id: Pair_Table.cpp,v 1.1 2006-10-05 08:29:21 chriss Exp $
//
// File: src/Pair_Table.cpp
// Purpose: Helper for Fourvec_Constrainer.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//

#include "hitfit/private/Pair_Table.hpp"
#include "hitfit/Fourvec_Event.hpp"
#include <ostream>


using std::vector;
using std::ostream;


namespace hitfit {


Pair_Table::Pair_Table (const vector<Constraint>& cv,
                        const Fourvec_Event& ev)
//
// Purpose: Constructor.
//
// Inputs:
//   cv -          The list of constraints for the problem.
//   ev -          The event.
//
{
  // The number of objects in the event, including any neutrino.
  int nobjs = ev.nobjs_all();

  // Number of constraints.
  int nc = cv.size();

  // Loop over pairs of objects.
  for (int i=0; i < nobjs-1; i++)
    for (int j=i+1; j < nobjs; j++) {
      // Make an Objpair instance out of it.
      Objpair p (i, j, nc);

      // Loop over constraints.
      bool wanted = false;
      for (int k=0; k < nc; k++) {
        int val = cv[k].has_labels (ev.obj (i).label, ev.obj (j).label);
        if (val) {
          // This pair is used by this constraint.  Record it.
          p.has_constraint (k, val);
          wanted = true;
        }
      }

      // Was this pair used by any constraint?
      if (wanted)
        m_pairs.push_back (p);
    }
}


int Pair_Table::npairs () const
//
// Purpose: Return the number of pairs in the table.
//
// Returns:
//   The number of pairs in the table.
//
{
  return m_pairs.size();
}


const Objpair& Pair_Table::get_pair (int pairno) const
//
// Purpose: Return one pair from the table.
//
// Inputs:
//   pairno -      The number of the pair (0-based).
//
// Returns:
//   Pair PAIRNO.
//
{
  assert (pairno >= 0 && (size_t)pairno < m_pairs.size());
  return m_pairs[pairno];
}


ostream& operator<< (ostream& s, const Pair_Table& p)
//
// Purpose: Print the object to S.
//
// Inputs:
//   s -           The stream to which to write.
//   p -           The object to write.
//
// Returns:
//   The stream S.
//
{
  for (int i=0; i < p.npairs(); i++)
    s << " " << p.get_pair (i) << "\n";
  return s;
}


} // namespace hitfit
