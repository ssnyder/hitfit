//
// $Id: Lepjets_Event_Lep.cpp,v 1.1 2006-10-05 08:29:21 chriss Exp $
//
// File: src/Lepjets_Event_Lep.cpp
// Purpose: Represent a `lepton' in a Lepjets_Event.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//

#include "hitfit/Lepjets_Event_Lep.hpp"


using CLHEP::HepRandomEngine;


namespace hitfit {


Lepjets_Event_Lep::Lepjets_Event_Lep (const Fourvec& p,
                                      int type,
                                      const Vector_Resolution& res)
//
// Purpose: Constructor.
//
// Inputs:
//   p -           The 4-momentum.
//   type -        The type code.
//   res -         The resolution.
//
  : m_p (p),
    m_type (type),
    m_res (res)
{
}


Fourvec& Lepjets_Event_Lep::p ()
//
// Purpose: Access the 4-momentum.
//
// Returns:
//   The 4-momentum.
//
{
  return m_p;
}


const Fourvec& Lepjets_Event_Lep::p () const
//
// Purpose: Access the 4-momentum.
//
// Returns:
//   The 4-momentum.
//
{
  return m_p;
}


int& Lepjets_Event_Lep::type ()
//
// Purpose: Access the type code.
//
// Returns:
//   The type code.
//
{
  return m_type;
}


int Lepjets_Event_Lep::type () const
//
// Purpose: Access the type code.
//
// Returns:
//   The type code.
//
{
  return m_type;
}


const Vector_Resolution& Lepjets_Event_Lep::res () const
//
// Purpose: Access the resolutions.
//
// Returns:
//   The resolutions.
//
{
  return m_res;
}


Vector_Resolution& Lepjets_Event_Lep::res ()
//
// Purpose: Access the resolutions.
//
// Returns:
//   The resolutions.
//
{
  return m_res;
}


double Lepjets_Event_Lep::p_sigma () const
//
// Purpose: Return the momentum (or 1/p) resolution for this object.
//
// Returns:
//   The momentum (or 1/p) resolution for this object.
//
{
  return m_res.p_sigma (m_p);
}


double Lepjets_Event_Lep::eta_sigma () const
//
// Purpose: Return the eta resolution for this object.
//
// Returns:
//   The eta resolution for this object.
//
{
  return m_res.eta_sigma (m_p);
}


double Lepjets_Event_Lep::phi_sigma () const
//
// Purpose: Return the phi resolution for this object.
//
// Returns:
//   The phi resolution for this object.
//
{
  return m_res.phi_sigma (m_p);
}


void Lepjets_Event_Lep::smear (HepRandomEngine& engine,
                               bool smear_dir /*= false*/)
//
// Purpose: Smear this object according to its resolutions.
//
// Inputs:
//   engine -      The underlying RNG.
//   smear_dir -   If false, smear the momentum only.
//
{
  m_res.smear (m_p, engine, smear_dir);
}


std::ostream& Lepjets_Event_Lep::dump (std::ostream& s,
                                       bool full /*= false*/) const
//
// Purpose: Dump out this object.
//
// Inputs:
//   s -           The stream to which to write.
//   full -        If true, dump the resolutions too.
//
// Returns:
//   The stream S.
//
{
  s << "[" << m_type << "] " << m_p;
  if (full) {
    s << "\n    " << m_res;
  }
  return s;
}


std::ostream& operator<< (std::ostream& s, const Lepjets_Event_Lep& l)
//
// Purpose: Dump out this object.
//
// Inputs:
//   s -           The stream to which to write.
//   l -           The object to dump.
//
// Returns:
//   The stream S.
//
{
  return l.dump (s);
}


bool Lepjets_Event_Lep::operator< (const Lepjets_Event_Lep& x) const
//
// Purpose: Sort objects by pt.
//
// Retruns:
//   True if this object's pt is less than that of x.
{
  return p().perp2() < x.p().perp2();
}


} // namespace hitfit
