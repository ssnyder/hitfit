//
// $Id: Constraint.cpp,v 1.1 2006-10-05 08:29:20 chriss Exp $
//
// File: src/Constraint.cpp
// Purpose: Represent a mass constraint equation.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//

#include "hitfit/private/Constraint.hpp"
#include "hitfit/private/Constraint_Intermed.hpp"
#include <iostream>
#include <assert.h>

using std::auto_ptr;
using std::ostream;
using std::string;

namespace hitfit {


Constraint::Constraint (const Constraint& c)
//
// Purpose: Copy constructor.
//
// Inputs:
//   c -           The instance to copy.
//
  : m_lhs (c.m_lhs->clone ()),
    m_rhs (c.m_rhs->clone ())
{
}


Constraint& Constraint::operator= (const Constraint& c)
//
// Purpose: Assignment.
//
// Inputs:
//   c -           The instance to copy.
//
// Returns:
//   This instance.
//
{
  {
    auto_ptr<Constraint_Intermed> ci = c.m_lhs->clone ();
    m_lhs = ci;
  }
  {
    auto_ptr<Constraint_Intermed> ci = c.m_rhs->clone ();
    m_rhs = ci;
  }

  return *this;
}


Constraint::Constraint (string s)
//
// Purpose: Constructor.
//          Build a constraint from the string describing it.
//
// Inputs:
//   s -           The string describing the constraint.
//
{
  // Split it at the equals sign.
  string::size_type i = s.find ('=');
  assert (i != string::npos);

  // And then build the two halves.
  {
    auto_ptr<Constraint_Intermed> ci =
      make_constraint_intermed (s.substr (0, i));
    m_lhs = ci;
  }
  {
    auto_ptr<Constraint_Intermed> ci =
      make_constraint_intermed (s.substr (i+1));
    m_rhs = ci;
  }
}


int Constraint::has_labels (int ilabel, int jlabel) const
//
// Purpose: See if this guy references both labels ILABEL and JLABEL
//          on a single side of the constraint equation.
//
// Inputs:
//   ilabel -      The first label to test.
//   jlabel -      The second label to test.
//
// Returns:
//   +1 if the LHS references both.
//   -1 if the RHS references both.
//    0 if neither reference both.
//
{
  if (m_lhs->has_labels (ilabel, jlabel))
    return 1;
  else if (m_rhs->has_labels (ilabel, jlabel))
    return -1;
  else
    return 0;
}


double Constraint::sum_mass_terms (const Fourvec_Event& ev) const
//
// Purpose: Evaluate the mass constraint, using the data in EV.
//          Return m(lhs)^2/2 - m(rhs)^2/2.
//
// Inputs:
//   ev -          The event for which the constraint should be evaluated.
//
// Returns:
//   m(lhs)^2/2 - m(rhs)^2/2.
//
{
  return m_lhs->sum_mass_terms (ev) - m_rhs->sum_mass_terms (ev);
}


ostream& operator<< (ostream& s, const Constraint& c)
//
// Purpose: Print the object to S.
//
// Inputs:
//   s -           The stream to which to write.
//   c -           The object to write.
//
// Returns:
//   The stream S.
//
{
  s << *c.m_lhs.get() << " = " << *c.m_rhs.get();
  return s;
}


} // namespace hitfit
