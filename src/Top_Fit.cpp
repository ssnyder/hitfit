//
// $Id: Top_Fit.cpp,v 1.8 2009-02-12 10:36:03 chriss Exp $
//
// File: src/Top_Fit.cpp
// Purpose: Handle jet permutations.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//
// XXX handle merging jets.
// XXX btagging for ttH.
//



#include "hitfit/Top_Fit.hpp"
#include "hitfit/Lepjets_Event.hpp"
#include "hitfit/Top_Decaykin.hpp"
#include "hitfit/Defaults.hpp"
#include "hitfit/Fit_Results.hpp"
#include "hitfit/Run1_Jetcorr.hpp"
#include "hitfit/Run2_Partoncorr.hpp"
#include "hitfit/fourvec.hpp"

#ifndef ROOT_TVirtualFitter
#include "TVirtualFitter.h"
#endif

#include <iostream>
#include <algorithm>
#include <cmath>
#include <cassert>

using std::cout;
using std::endl;
using std::abs;
using std::next_permutation;
using std::sort;
using std::vector;
using std::ostream;


namespace hitfit {

TVirtualFitter *FCNFitter=0;
void TFCN(Int_t& npar, Double_t* gin, Double_t& f, Double_t* par, Int_t flag);


//*************************************************************************
// Argument handling.
//


Top_Fit_Args::Top_Fit_Args (const Defaults& defs)
//
// Purpose: Constructor.
//
// Inputs:
//   defs -        The Defaults instance from which to initialize.
//
  : m_print_event_flag (defs.get_bool ("print_event_flag")),
    m_print_nu_flag (defs.get_bool ("print_nu_flag")),
    m_do_bothnu_flag (defs.get_bool ("do_bothnu_flag")),
    m_do_higgs_flag (defs.get_bool ("do_higgs_flag")),
    m_do_run1_jetcorr (defs.get_bool ("do_run1_jetcorr")),
    m_do_run2_partoncorr (defs.get_bool ("do_run2_partoncorr")),
    m_do_run2_deltaScorr (defs.get_bool ("do_run2_deltaScorr")),
    m_do_run2_SetResolutions (defs.get_bool ("do_run2_SetResolutions")),
    m_jet_mass_cut (defs.get_float ("jet_mass_cut")),
    m_mwhad_min_cut (defs.get_float ("mwhad_min_cut")),
    m_mwhad_max_cut (defs.get_float ("mwhad_max_cut")),
    m_mtdiff_max_cut (defs.get_float ("mtdiff_max_cut")),
    m_nkeep (defs.get_int ("nkeep")),
    m_deltaScorr_nsigma_systematic (defs.get_int ("deltaScorr_nsigma_systematic")),
    m_args (defs),
    m_jetcorr_args (defs),
    m_partoncorr_args (defs)
{
}


bool Top_Fit_Args::print_event_flag () const
//
// Purpose: Return the print_event_flag parameter.
//          See the header for documentation.
//
{
  return m_print_event_flag;
}


bool Top_Fit_Args::print_nu_flag () const
//
// Purpose: Return the print_nu_flag parameter.
//          See the header for documentation.
//
{
  return m_print_nu_flag;
}


bool Top_Fit_Args::do_bothnu_flag () const
//
// Purpose: Return the do_bothnu_flag parameter.
//          See the header for documentation.
//
{
  return m_do_bothnu_flag;
}

 
bool Top_Fit_Args::do_higgs_flag () const
//
// Purpose: Return the do_higgs_flag parameter.
//          See the header for documentation.
//
{
  return m_do_higgs_flag;
}

 
bool Top_Fit_Args::do_run1_jetcorr () const
//
// Purpose: Return the do_run1_jetcorr parameter.
//          See the header for documentation.
//
{
  return m_do_run1_jetcorr;
}

 
bool Top_Fit_Args::do_run2_partoncorr () const
//
// Purpose: Return the do_run2_partoncorr parameter.
//          See the header for documentation.
//
{
  return m_do_run2_partoncorr;
}

bool Top_Fit_Args::do_run2_deltaScorr () const
//
// Purpose: Return the do_run2_deltaScorr parameter.
//          See the header for documentation.
//
{
  return m_do_run2_deltaScorr;
}

 bool Top_Fit_Args::do_run2_SetResolutions () const
//
// Purpose: Return the do_run2_SetResolutions parameter.
//          See the header for documentation.
//
{
  return m_do_run2_SetResolutions;
}


double Top_Fit_Args::jet_mass_cut () const
//
// Purpose: Return the jet_mass_cut parameter.
//          See the header for documentation.
//
{
  return m_jet_mass_cut;
}


double Top_Fit_Args::mwhad_min_cut () const
//
// Purpose: Return the mwhad_min_cut parameter.
//          See the header for documentation.
//
{
  return m_mwhad_min_cut;
}


double Top_Fit_Args::mwhad_max_cut () const
//
// Purpose: Return the mwhad_max_cut parameter.
//          See the header for documentation.
//
{
  return m_mwhad_max_cut;
}


double Top_Fit_Args::mtdiff_max_cut () const
//
// Purpose: Return the mtdiff_max_cut parameter.
//          See the header for documentation.
//
{
  return m_mtdiff_max_cut;
}


int Top_Fit_Args::nkeep () const
//
// Purpose: Return the nkeep parameter.
//          See the header for documentation.
//
{
  return m_nkeep;
}

int Top_Fit_Args::deltaScorr_nsigma_systematic () const
//
// Purpose: Return the deltaScorr_systematic_nsigma parameter.
//          See the header for documentation.
//
{
  return m_deltaScorr_nsigma_systematic;
}


const Constrained_Top_Args& Top_Fit_Args::constrainer_args () const
//
// Purpose: Return the contained subobject parameters.
//
{
  return m_args;
}


const Run1_Jetcorr_Args& Top_Fit_Args::jetcorr_args () const
//
// Purpose: Return the contained subobject parameters.
//
{
  return m_jetcorr_args;
}

const Run2_Partoncorr_Args& Top_Fit_Args::partoncorr_args () const
//
// Purpose: Return the contained subobject parameters.
//
{
  return m_partoncorr_args;
}


//*************************************************************************
// Helper functions.
//


namespace {


bool test_for_bad_masses (const Lepjets_Event& ev,
                          const Top_Fit_Args& args,
                          double mwhad,
                          double umthad,
                          double umtlep)
//
// Purpose: Apply mass cuts to see if this event should be rejected
//          without fitting.
//
// Inputs:
//   ev -          The event to test.
//   args -        Parameter setting.
//   mwhad -       The hadronic W mass.
//   umthad -      The hadronic top mass.
//   umtlep -      The leptonic top mass.
//
// Returns:
//   True if the event should be rejected.
//
{
  //  return false;
  // Reject the event if any jet's mass is too large.
  if (ev.sum (lepb_label).m()  > args.jet_mass_cut() ||
      ev.sum (hadb_label).m()  > args.jet_mass_cut() ||
      ev.sum (hadw1_label).m() > args.jet_mass_cut() ||
      ev.sum (hadw2_label).m() > args.jet_mass_cut())
    {
      std::cout<<"jet mass too large!!"<<std::endl;
      return true;
    }
  // Reject if if the hadronic W mass is outside the window.
  if (mwhad < args.mwhad_min_cut() || mwhad > args.mwhad_max_cut())
    {
      std::cout<<"Hadronic W mass outside window"<<std::endl;
      return true;
    }
  // And if the two top masses are too far apart.
  if (abs (umthad - umtlep) > args.mtdiff_max_cut())
    {
      std::cout<<"The two top masses are too far apart"<<std::endl;
      return true;
    }
  // It's ok.
  return false;
}


vector<int> classify_jetperm (const vector<int>& jet_types,
                              const Lepjets_Event& ev)
//
// Purpose: Classify a jet permutation, to decide on what result
//          lists it should be put.
//
// Inputs:
//   jet_types -   Vector of jet types.
//   ev -          The original event being fit.
//
// Returns:
//   A list_flags vector, appropriate to pass to Fit_Results::push.
//
{
  // Start by assuming it's on all the lists.
  // We'll clear the flags if we see that it actually doesn't
  // belong.
  vector<int> out (n_lists);
  out[all_list] = 1;
  out[noperm_list] = 1;
  out[semicorrect_list] = 1;
  out[limited_isr_list] = 1;
  out[topfour_list] = 1;
  out[btag_list] = 1;
  out[htag_list] = 1;

  // Loop over jets.
  assert ((int)jet_types.size() == ev.njets());
  for (size_t i=0; i < jet_types.size(); i++)
  {
    {
      int t1 =   jet_types[i];    // Current type of this jet.
      int t2 = ev.jet(i).type();  // `Correct' type of this jet.

      // Consider hadw1_label and hadw2_label the same.
      if (t1 == hadw2_label) t1 = hadw1_label;
      if (t2 == hadw2_label) t2 = hadw1_label;

      // If they're not the same, the permutation isn't correct.
      if (t1 != t2) out[noperm_list] = 0;

      // Test for a semicorrect permutation.
      // Here, all hadronic-side jets are considered equivalent.
      if (t1 == hadw1_label) t1 = hadb_label;
      if (t2 == hadw1_label) t2 = hadb_label;
      if (t1 != t2) out[semicorrect_list] = 0;
    }

    if (jet_types[i] == isr_label && i <= 2)
      out[limited_isr_list] = 0;

    if ((jet_types[i] == isr_label && i <= 3) ||
        (jet_types[i] != isr_label && i >= 4))
      out[topfour_list] = 0;

    if ((ev.jet(i).svx_tag() || ev.jet(i).slt_tag()) &&
        ! (jet_types[i] == hadb_label || jet_types[i] == lepb_label))
      out[btag_list] = 0;

    if ((ev.jet(i).svx_tag() || ev.jet(i).slt_tag()) &&
        ! (jet_types[i] == hadb_label  || jet_types[i] == lepb_label ||
           jet_types[i] == higgs_label))
      out[htag_list] = 0;
  }
  return out;
}


void set_jet_types (const vector<int>& jet_types,
                    Lepjets_Event& ev)
//
// Purpose: Update EV with a new set of jet types.
//
// Inputs:
//   jet_types -   Vector of new jet types.
//   ev -          The event to update.
//
// Outputs:
//   ev -          The updated event.
//
{
  assert (ev.njets() == (int)jet_types.size());
  bool saw_hadw1 = false;
  for (int i=0; i < ev.njets(); i++) {
    int t = jet_types[i];
    if (t == hadw1_label) {
      if (saw_hadw1)
        t = hadw2_label;
      saw_hadw1 = true;
    }
    ev.jet (i).type() = t;
  }
}


} // unnamed namespace


//*************************************************************************


Top_Fit::Top_Fit (const Top_Fit_Args& args,
                  double lepw_mass,
                  double hadw_mass,
                  double top_mass)
//
// Purpose: Constructor.
//
// Inputs:
//   args -        The parameter settings for this instance.
//   lepw_mass -   The mass to which the leptonic W should be constrained,
//                 or 0 to skip this constraint.
//   hadw_mass -   The mass to which the hadronic W should be constrained,
//                 or 0 to skip this constraint.
//   top_mass -    The mass to which the top quarks should be constrained,
//                 or 0 to skip this constraint.
//   
  : m_args (args),
    m_constrainer (args.constrainer_args(),
                  lepw_mass, hadw_mass, top_mass),
	    m_hadw_mass (hadw_mass), m_event_to_fit(0,0), m_event_after_fit(0,0)
{

}


double Top_Fit::fit_one_perm (Lepjets_Event& ev,
                              double& umwhad,
                              double& utmass,
                              double& mt,
                              double& sigmt,
                              Column_Vector& pullx,
                              Column_Vector& pully)
//
// Purpose: Fit a single jet permutation.
//
// Inputs:
//   ev -          The event to fit.
//                 The object labels must have already been assigned.
//
// Outputs:
//   ev-           The event after the fit.
//   umwhad -      Hadronic W mass before fitting.
//   mt -          Top mass after fitting.
//   sigmt -       Top mass uncertainty after fitting.
//   pullx -       Vector of pull quantities for well-measured variables.
//   pully -       Vector of pull quantities for poorly-measured variables.
//
// Returns:
//   The fit chisq, or < 0 if the fit didn't converge.
//
{
  mt = 0;
  sigmt = 0;

  // Apply run 1 jet corrections, if requested.
  if (m_args.do_run1_jetcorr ())
    run1_jetcorr (m_args.jetcorr_args(), ev);
  
  // Apply run 2 parton level corrections, if requested.
  // Overide object resolutions with run 2 parameters as defined in 
  // defaults file, if requested.
  if (m_args.do_run2_partoncorr () || m_args.do_run2_SetResolutions ())
    run2_partoncorr (m_args.partoncorr_args(), 
                     m_args.do_run2_partoncorr(),
		     m_args.do_run2_deltaScorr(),
		     m_args.deltaScorr_nsigma_systematic(),
                     m_args.do_run2_SetResolutions(),
                     ev);
  
  // Find the neutrino solutions by requiring that the leptonic top
  // have the same mass as the hadronic top.
  umwhad = Top_Decaykin::hadw (ev) . m();
  double umthad = Top_Decaykin::hadt (ev) . m();
  double nuz1, nuz2;
//  Top_Decaykin::solve_nu_tmass (ev, umthad, nuz1, nuz2);
  Top_Decaykin::solve_nu (ev, 80.4, nuz1, nuz2);
  
  // Set up to try the first neutrino solution.
  ev.met().setZ (nuz1);
  adjust_e_for_mass (ev.met(), 0);

  // Find the unfit top mass as the average of the two sides.
  double umtlep = Top_Decaykin::lept (ev) . m();
  utmass = (umthad + umtlep) / 2;

  // Trace, if requested.
  if (m_args.print_event_flag()) {
    cout << "Before fit:\n";
    Top_Decaykin::dump_ev (cout, ev);
  }

  // Maybe reject this event.
  if (m_hadw_mass > 0 && test_for_bad_masses (ev, m_args, umwhad,
                                             umthad, umtlep))
  {
    cout << " bad mass comb";
    return -999;
  }

  // Save the event, to try the other neutrino solution.
  Lepjets_Event ev2 = ev;

  // Do the fit.
  double chisq = m_constrainer.constrain (ev, mt, sigmt, pullx, pully);

  // Trace, if requested.
  if (m_args.print_nu_flag())
    cout << "nu: " << chisq << " " << mt << " ";

  // If there's a distinct second neutrino solution, try it too,
  // if requested.
  if (m_args.do_bothnu_flag() &&
      nuz1 != nuz2 &&
      abs (nuz2) < m_args.constrainer_args().fourvec_constrainer_args().e_com())
  {
    ev2.met().setZ (nuz2);
    adjust_e_for_mass (ev2.met(), 0);
    double mt2, sigmt2;
    Column_Vector pullx2;
    Column_Vector pully2;
    double chisq2 = m_constrainer.constrain (ev2, mt2, sigmt2, pullx2, pully2);
    if (chisq2 >= 0 && chisq2 < chisq) {
      // This one's better.
      chisq = chisq2;
      ev = ev2;
      mt = mt2;
      sigmt = sigmt2;
      pullx = pullx2;
      pully = pully2;
    }

    // Tracing.
    if (m_args.print_nu_flag())
      cout << chisq << " " << mt << "\n";
  }
  else {
    // Tracing.
    if (m_args.print_nu_flag())
      cout << "-1 -1\n";
  }

  // Done!
  return chisq;
}

double Top_Fit::fit_one_perm_minuit (Lepjets_Event& ev,
                              double& umwhad,
                              double& utmass,
                              double& mt,
                              double& sigmt,
                              Column_Vector& pullx,
                              Column_Vector& pully)
//
// Purpose: Fit a single jet permutation.
//
// Inputs:
//   ev -          The event to fit.
//                 The object labels must have already been assigned.
//
// Outputs:
//   ev-           The event after the fit.
//   umwhad -      Hadronic W mass before fitting.
//   mt -          Top mass after fitting.
//   sigmt -       Top mass uncertainty after fitting.
//   pullx -       Vector of pull quantities for well-measured variables.
//   pully -       Vector of pull quantities for poorly-measured variables.
//
// Returns:
//   The fit chisq, or < 0 if the fit didn't converge.
//
{
  mt = 0;
  sigmt = 0;
  
  // Apply run 1 jet corrections, if requested.
  if (m_args.do_run1_jetcorr ())
    run1_jetcorr (m_args.jetcorr_args(), ev);
  
  // Apply run 2 parton level corrections, if requested.
  // Overide object resolutions with run 2 parameters as defined in 
  // defaults file, if requested.
  if (m_args.do_run2_partoncorr () || m_args.do_run2_SetResolutions ())
    run2_partoncorr (m_args.partoncorr_args(), 
                     m_args.do_run2_partoncorr(),
		     m_args.do_run2_deltaScorr(),
		     m_args.deltaScorr_nsigma_systematic(),
                     m_args.do_run2_SetResolutions(),
                     ev);
  
  //--
  // Find the neutrino solutions by requiring that the leptonic top
  // have the same mass as the hadronic top. (FOR THE OLD INTERFACE?)
  //--
  mt = 0;
  sigmt = 0;
  umwhad = Top_Decaykin::hadw (ev) . m();
  double umthad = Top_Decaykin::hadt (ev) . m();
  double nuz1, nuz2;
  Top_Decaykin::solve_nu (ev, 80.4, nuz1, nuz2);  
//  Top_Decaykin::solve_nu_tmass (ev, umthad, nuz1, nuz2);
  // Set up to try the first neutrino solution.
  ev.met().setZ (nuz1);
  adjust_e_for_mass (ev.met(), 0);
  // Find the unfit top mass as the average of the two sides.
  double umtlep = Top_Decaykin::lept (ev) . m();
  utmass = (umthad + umtlep) / 2;
  //
  //--
  //
  // Prepare the fit:
  // get the hadronic jets
  //
  m_event_to_fit=ev;
  double chisq=0;
 //---
 //
 // MINUIT Fitting
 //
 //---  
 const int npar=13;
// --
//  to store results 
//--
    m_pars=Column_Vector(npar);
    m_errs=Column_Vector(npar);
    m_steps=Column_Vector(npar);
    m_chiterms=Column_Vector(100);
// CHECK that these vector are deleted and dont cause memory leaks
// (not done yet!!!)
//--
//
    //double par[npar];
 const char* par_name[npar]  = {"jes_l1", "jes_l2",  "jes_Bl", "jes_Bh", "jes_met", "nes", "les", "mtop", "lam_mteq", "lam_mthad", "lam_mwlep", "lam_mwhad", "lam_jes"};
 double par_start[npar]      = {  1,         1.,       1.,        1.,       1,        1,     1,     175,     1,            1,          1,            1,         0 };
 double par_start_step[npar] = {  0.02,      0.02,     0.02,      0.02,     0.0 ,     0.,    0.,    0.,      0,            0,          0,            0,         0 }; 
 double par_max[npar]        = {  5,         5,        5,         5,        2.0,      2,     2,     250,     1000,         1000,      100,          100,       100};
 double par_min[npar]        = {0.0,       0.0,      0.0,         0.0,      0.0,      0,     0,      50,    -1000,        -1000,       0,            0,      -1000};
 int ierflg; ierflg = 0;
 //double arglist[5]= {0,0,0,0,0};

 FCNFitter = TVirtualFitter::Fitter(this, npar);
 FCNFitter->Clear();
 FCNFitter->SetObjectFit(this);
 FCNFitter->SetFCN(TFCN);
 
 for (int i=0;i<npar;i++) {
   FCNFitter->SetParameter(i, par_name[i], par_start[i], par_start_step[i], par_min[i], par_max[i]);
 }

 // fit
 Int_t status = FCNFitter->ExecuteCommand("MINIMIZE",0,0);
 cout << " status after fit should be 0  " << status << endl;
 //--
 //
 // Calculate arguments of this function. (mostly for the old interface).
 //
 //--
 double *dum2 = 0;
 int numpar=npar;

 for (int i=0;i<npar;i++) {
   par_start[i]=FCNFitter->GetParameter(i); 
   m_pars(i+1)=FCNFitter->GetParameter(i); 
   m_errs(i+1)=FCNFitter->GetParError(i); 
   m_steps(i+1)= par_start_step[i]; 
 }

 // Print out the covariance matrix
/* 
 for (int i = 0; i< 7; i++) {
    for (int j = 0; j< 7; j++) {
 
       Double_t  matrixElem = FCNFitter->GetCovarianceMatrixElement(i,j);
       std::cout << "  " << std::setw(3) << std::setprecision(3) << matrixElem << "  ";
    }
    std::cout << std::endl;
 }
*/
 ComputeFCN(numpar, dum2,  chisq, par_start, ierflg);
   cout << " CHISQ after fit: " << chisq << endl;
 // NOTE: from now on, 'ev' contains event after fit as it was done in the old way.
 ev=get_event_after_fit(); 
 // for (int i=0;i<npar;i++) {
 //  //   pullx -       The pull quantities for the well-measured variables.
 //  //   pully -       The pull quantities for the poorly-measured variables.
 //  // I just put the parameters and errors in these pull vectors, sicne i dont yet know better.......CHECK
 //  pullx.push_back(FCNFitter->GetParameter(i)); 
 //  pully.push_back(FCNFitter->GetParError(i)); 
 // }

 cout << " # rows #col in pluulx " << pullx.num_row() << " " <<  pullx.num_col() << endl;
 cout << " # rows #col in pluuly " << pully.num_row() << " " <<  pully.num_col() << endl;
  for (int i=0;i<pullx.num_row();i++) {
   pullx(i)=FCNFitter->GetParameter(i); 
  }
  for (int i=0;i<pully.num_row();i++) {
   pully(i)=FCNFitter->GetParameter(i); 
  }
 
 
 int itop=7;
 if (std::string(par_name[itop])!="mtop") cout << " FATAL: fit_one_perm: the mass of the top is lost "<< endl;
 if (FCNFitter->IsFixed(itop) || par_start_step[itop]==0) {
   // calc mtop from the new event

   mt = Top_Decaykin::hadt (ev) . m();
   sigmt=1.4; // i dont know yet what the error is. Have to think about this. CHECK
   cout << " The top mass from calculation after converg.: " << mt << endl;
   //    double umtlep = Top_Decaykin::lept (ev) . m();
 } else {   //        !!!!! this doesn't work!!!!!
            // in the above if-statement par_start_step[] is always == 0
	    //
   // mtop was a fit parameter
   mt=FCNFitter->GetParameter(itop); 
   sigmt=FCNFitter->GetParError(itop); 
   cout << " The top mass from fit after converg.: " << mt << endl;
 }
 
  
 // Tracing.
 if (m_args.print_nu_flag()) {
   cout << chisq << " " << mt << "\n";
 }
 else {
   // Tracing.
   if (m_args.print_nu_flag())
     cout << "-1 -1\n";
 }
 
 // Done!
 return chisq;
}






  Fit_Results Top_Fit::fit (const Lepjets_Event& ev, bool use_minuit)
//
// Purpose: Fit all jet permutations for EV.
//
// Inputs:
//   ev -          The event to fit.
//
// Returns:
//   The results of the fit.
//
{
  // Make a new Fit_Results object.
  Fit_Results res (m_args.nkeep(), n_lists);

  // Set up the vector of jet types.
  vector<int> jet_types (ev.njets(), isr_label);
  assert (ev.njets() >= 4);
  jet_types[0] = lepb_label;
  jet_types[1] = hadb_label;
  jet_types[2] = hadw1_label;
  jet_types[3] = hadw1_label;

  if (m_args.do_higgs_flag() && ev.njets() >= 6) {
    jet_types[4] = higgs_label;
    jet_types[5] = higgs_label;
  }

  // Must be in sorted order.
  sort (jet_types.begin(), jet_types.end());

  do {
    // Copy the event.
    Lepjets_Event fev = ev;

    // Install the new jet types.
    set_jet_types (jet_types, fev);

    // Figure out on what lists this permutation should go.
    vector<int> list_flags = classify_jetperm (jet_types, ev);

    // Tracing.
//    cout << "(";
//    for (int i=0; i < jet_types.size(); i++) {
//      if (i) cout << " ";
//      cout << jet_types[i];
//    }
//    cout << ") ";

    // Do the fit.
    double umwhad, utmass, mt, sigmt;
    Column_Vector pullx;
    Column_Vector pully;
    double chisq;
    
//    std::cout << "the BOOL use_minuit is set to " << use_minuit << std::endl;
    use_minuit = true;
    
    if(use_minuit){
      chisq= fit_one_perm_minuit (fev, umwhad, utmass, mt, sigmt, pullx, pully);
    }else{
      chisq= fit_one_perm (fev, umwhad, utmass, mt, sigmt, pullx, pully);
    }
    // Print the result, if requested.
    if (m_args.print_event_flag()) {
      cout << "After fit:\n";
      Top_Decaykin::dump_ev (cout, ev);
    }

    {
      //char buf[256];
//      sprintf (buf, "%8.3f  %6.2f pm %5.2f %c\n",
//               chisq, mt, sigmt, (list_flags[noperm_list] ? '*' : ' '));
//      cout << buf;
    }

    // Add it to the results.
    res.push (chisq, fev, pullx, pully, umwhad, utmass, mt, sigmt, m_pars, m_errs, m_steps, m_chiterms, list_flags);

    // Step to the next permutation.
  } while (next_permutation (jet_types.begin(), jet_types.end()));

  return res;
}


ostream& operator<< (ostream& s, const Top_Fit& fitter)
//
// Purpose: Print the object to S.
//
// Inputs:
//   s -           The stream to which to write.
//   fitter -      The object to write.
//
// Returns:
//   The stream S.
//
{
  return s << fitter.m_constrainer;
}

Lepjets_Event  Top_Fit::get_event_to_fit()    { return m_event_to_fit;    }
Lepjets_Event  Top_Fit::get_event_after_fit() { return m_event_after_fit; }

void Top_Fit::ComputeFCN(int& /*numpar*/, double* /*dum2*/, double& chi2, double* par, int /*iflag*/)
{
  // first get the event
  Lepjets_Event ev=get_event_to_fit();
  // copy the event
  Lepjets_Event evnew = ev;
  //
  // some constants
  //double wmass = 80.4;
  
/*  // only phi resolution (E, eta and neutrino are replaced by truth values
                            to calculate these)
  double wLepmass_sigma = 3.4;
  double wHadmass_sigma = 2.8;
  double topLepm_sigma  = 1.4;
  double topHadm_sigma  = 1.2;
  double topHadm_sigma  = 3.3;  // The latest experimental error from PDG 2006
*/
/*  // phi resolution; (neutrino not replaced; to use when etmiss has
                        no free scale parameter jes_met)
  double wLepmass_sigma = 9.3;
  double wHadmass_sigma = 2.8;
  double topHadm_sigma  = 1.2;
  double topLepm_sigma  = 1.6;
  */
  //  eta and phi resolutions; ( only jet energies are replaced by truth value)
  double wLepmass_sigma = 9.3;
  double wHadmass_sigma = 4.2;
  double topHadm_sigma  = 3.2;
  double topLepm_sigma  = 4.2; 
  
  //
  // reset the main chi2
  chi2 = 0.;

  //
  // the fit parameters
  double jes_l1  = par[0]; // light jet energy scale for Wdg1
  double jes_l2  = par[1]; // light jet energy scale for Wdg2
  double jes_Bl  = par[2]; // b jet (leptonic side) energy scale
  double jes_Bh  = par[3]; // b jet (hadronic side) energy scale
  double jes_met = par[4]; // scale met (and thus px,py neutrino)
  //double nes     = par[5]; // scale px, py neutrino, but not met. ----> NOT IMPLEMENTED; have to think/discuss.
  double les     = par[6]; // charged leptonic energy scale 
  double mtop    = par[7]; // fit the top mass to be mtop, if lam_mthad is set to >0
  double lam_mteq  = par[8];  // to switch constraint mtlep=mthad on (and/or change weight, if required)
  double lam_mthad = par[9];  // to switch constraint mt_hadronic on (and/or change weight, if required)
  double lam_mwlep = par[10]; // to switch constraint mw_leptonic on (and/or change weight, if required)
  double lam_mwhad = par[11]; // to switch constraint mw_hadronic on (and/or change weight, if required)
  double lam_jes   = par[12]; // to equalize jes_l and jes_b and thus fit a single jes.
                              // NOTE: jes_l1 should be fitparameter and fix jes_l2 in Minuit!!!
                              //       jes_Bl should be fitparameter and fix jes_bh in Minuit!!!
  //
  if (lam_jes>0.5) {    // to fit a single light JES and a single heavy JES
     jes_l2 = jes_l1;
     jes_Bh = jes_Bl;
  }
  //
  // reset chis terms
  const int nmax=100;
  double chis[nmax];
  for (int i=0 ; i < nmax ; i++ ) {
    chis[i]=0.0;
  }
  //--
  // calculate chis terms
  //--
  //
  // measured quantities: jets, lepton and met
  //--
  // Hadronic jets: 
  // scale jets and put in evnew
  // chis[0] for leptonic B jet
  // chis[1] for hadronic B jet
  // chis[2] for hadronic W jet 1
  // chis[3] for hadronic W jet 2
  //--
  int iterm=-1;

  assert(ev.njets()==4);
  assert(ev.nleps()==1);


  for (int i=0 ; i<4 ; i++ ) {
    double scale = 0;

    if (ev.jet(i).type()==lepb_label) { // put lepb_had inc chis[0] etc.
      adjust_e_for_mass(ev.jet(i).p(), 5. ); 
      iterm = 0;
      scale = jes_Bl;
    } 
    if (ev.jet(i).type()==hadb_label) { 
      adjust_e_for_mass(ev.jet(i).p(), 5. ); 
      iterm = 1; 
      scale = jes_Bh;
    }
    if (ev.jet(i).type()==hadw1_label) { 
      adjust_e_for_mass(ev.jet(i).p(), 0. ); 
      iterm = 2;  
      scale = jes_l1;
    }
    if (ev.jet(i).type()==hadw2_label) { 
      adjust_e_for_mass(ev.jet(i).p(), 0. ); 
      iterm = 3; 
      scale = jes_l2;
    }
    assert (iterm >=0);  
    Threevec vjet=(ev.jet(i).p()).v();
    double error=(ev.jet(i).p()).perp() * ev.jet(i).p_sigma() / vjet.mag(); // error on et - the same for light and heavy jets
    chis[iterm]=(scale*(ev.jet(i).p()).perp() - (ev.jet(i).p()).perp() ) / error; 
    chis[iterm]*=chis[iterm];

    evnew.jet(i).p().setVect(scale*vjet);
    if (iterm>=2) {
      adjust_e_for_mass(evnew.jet(i).p(), 0. );
    } else {
      adjust_e_for_mass(evnew.jet(i).p(), 5. );
    }
    // CHECK does evnew contains jetnew now??????

    if (1==0 && fabs(scale-1)>0.1) {
      cout << " CHECK SAME JET?  in px" << evnew.jet(i).p().px()   << " " << scale*ev.jet(i).p().px() << endl; 
      cout << " CHECK SAME JET?  in et" << evnew.jet(i).p().perp() << " " << scale*ev.jet(i).p().perp() << endl; 
    }
  }


  //--
  // Leptonic side:
  // chis[4] scale lepton et
  // chis[5] for Kt_x, --> scale met=neutrono + scaled jets
  // chis[6] for Kt_y, --> scale met=neutrono + scaled jets
  //--
  // Scale lepton and put in evnew
  //
  Threevec vlep=ev.lep(0).p().v();
  double error=ev.lep(0).p().perp() * ev.lep(0).p_sigma() / vlep.mag(); // error on et
  // CHECK: is p_sigma the error or 1/p_sigma
  chis[4]=(les*ev.lep(0).p().perp() - ev.lep(0).p().perp() ) / error;
  chis[4]*=chis[4];

  evnew.lep(0).p().setVect(les*vlep);
  adjust_e_for_mass(evnew.lep(0).p(), 0. );
  //  cout << " CHECK LEP et " << evnew.lep(0).p().perp() << " " << les* ev.lep(0).p().perp() << endl;
  //--
  //
  // For Kt:
  // uses evnew, thus all the scaled object (jets, leptons and met) to calculate new Kt and
  // penalty this accordingly.
  //
  // put scaled scaled met in evnew.
  //--
  //
// Even when jet_met = 1 the etmiss is adjusted as to correct for the changes in jets.
  
  float dpxjet=evnew.kt().px() - ev.kt().px();
  float dpyjet=evnew.kt().py() - ev.kt().py();
//   Threevec newmet(jes_met*ev.met().px(), jes_met*ev.met().py(), jes_met*ev.met().pz()); 
  Threevec newmet((jes_met*ev.met().px())-dpxjet, (jes_met*ev.met().py())-dpyjet, jes_met*ev.met().pz());
  evnew.met().setVect(newmet);
  adjust_e_for_mass (evnew.met(), 0);

  // The missing energy can also be scaled:
  //   
/*  
  std::cout << "Adding two terms to the chi2!!" << std::endl;  

  double kt_error_x=evnew.kt_res().sigma(0.)/sqrt(2.0); // NOT SURE this gives the correct sigma. evnew contains scaled objects
  double kt_error_y=evnew.kt_res().sigma(0.)/sqrt(2.0); //

  chis[5]= ( evnew.kt().px() - ev.kt().px() )/kt_error_x;
  chis[5]*=chis[5] ;
  chis[6]= ( evnew.kt().py() - ev.kt().py() )/kt_error_y;
  chis[6]*=chis[6] ;  
  
*/
  //--
  // Mass Constraints
  // 
  // chis[11] for constraint mtlep=mthad 
  // chis[12] for constraint mt_hadronic 
  // chis[13] for constraint mw_leptonic 
  // chis[14] for constraint mw_hadronic 
  //--
  // 
  // first get pz neutrino
  double nuz1,nuz2,nuz[2];
  double mthad = Top_Decaykin::hadt (evnew).m();
  // cout << " The top mass in FCN: " << mthad << endl;

  // get the p_z for the neutrino
//  bool FoundPosDiscrForNeuPz = Top_Decaykin::solve_nu_tmass (evnew, mthad, nuz1, nuz2);
  /*bool FoundPosDiscrForNeuPz =*/ Top_Decaykin::solve_nu (evnew, 80.4, nuz1, nuz2);

   //   nuz1=0; nuz2=0; // CHECK
  nuz[0]=nuz1; nuz[1]=nuz2;
  // check contraints for both solutions
  double chis_c[4];
  double chis_ctotold=0;

  for (int ineut=0; ineut<2; ineut++) {
    evnew.met().setZ(nuz[ineut]);
    adjust_e_for_mass(evnew.met(), 0);    
    // At this point, evnew contains the adapted event for this call to fcn
    // (by Minuit). Check the constraints and penalty
    // mthad we have already
    double mtlep = Top_Decaykin::lept(evnew).m();    
    double mwhad = Top_Decaykin::hadw(evnew).m();    
    double mwlep = Top_Decaykin::lepw(evnew).m();    
    if (lam_mthad > 0.) {
        chis_c[0]= lam_mteq  * ( mtlep - mthad ) / topLepm_sigma ;
    } else {
        chis_c[0]= lam_mteq  * ( mtlep - mthad ) / sqrt(topHadm_sigma*topHadm_sigma + topLepm_sigma*topLepm_sigma) ; 
    }
    chis_c[1]= lam_mthad * ( mthad - mtop ) / topHadm_sigma;  // Note: error is more than top width
    chis_c[2]= lam_mwlep * ( mwlep - 80.4 ) / wLepmass_sigma; // Note: error is more than W width 
    chis_c[3]= lam_mwhad * ( mwhad - 80.4 ) / wHadmass_sigma;

    // An easy way to pass on the boolean of whether a positive
    // discriminant was found in the calculation of the neutrino pz.
    // chis[9] is not used anyway for now, and an extra 0.00001 to 
    // the chi2 doesn't matter.
    // 
	 // if (FoundPosDiscrForNeuPz){
	 //     chis[9] = 0.00001;
	 // }
    
    double chis_ctot=0 ;
    for (int i=0;i<4;i++) { 
      chis_c[i]*=chis_c[i];
      if (ineut==0) chis[11+i]=chis_c[i];  // store the chis_c in chis for 1st 
      chis_ctot+=chis_c[i];
    }
    
    
    if (ineut==1 && chis_ctot<chis_ctotold) { // if 2nd neut is better, take it
      for (int i=0;i<4;i++) chis[11+i]=chis_c[i];
    } else { // first was better, chis is fine, but put back 1ste neutrino
      evnew.met().setZ(nuz[0]);
      adjust_e_for_mass(evnew.met(), 0);    
    }
    chis_ctotold=chis_ctot;
  } // end loop over two neutrinmo solutions
  // get the total chi2
  for (int j=0; j<nmax; j++)  {
      m_chiterms[j] = chis[j];
      chi2 += chis[j];
  }
  m_event_after_fit=evnew;
} // end fcn





void TFCN(Int_t& npar, Double_t* gin, Double_t& f, Double_t* par, Int_t flag) {
   // Function called by the minimisation package.

   Top_Fit* fitter = dynamic_cast<Top_Fit*>(FCNFitter->GetObjectFit());
   if (!fitter) {
      FCNFitter->Error("TFCN","Invalid fit object encountered!");
      return;
   }
   fitter->ComputeFCN(npar, gin, f, par, flag);
}



} // namespace hitfit
