//
// $Id: Lepjets_Event.cpp,v 1.1 2006-10-05 08:29:21 chriss Exp $
//
// File: src/Lepjets_Event.hpp
// Purpose: Represent a simple `event' consisting of leptons and jets.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//


#include "hitfit/Lepjets_Event.hpp"
#include <algorithm>
#include <functional>
#include <cmath>
#include <cassert>

using std::sort;
using std::less;
using std::not2;
using std::remove_if;
using std::abs;
using CLHEP::HepRandomEngine;


namespace hitfit {


Lepjets_Event::Lepjets_Event (int runnum, int evnum)
//
// Purpose: Constructor.
//
// Inputs:
//   runnum -      The run number.
//   evnum -       The event number.
//
  : m_kt_res(12.),
    m_zvertex (0),
    m_isMC(false),
    m_runnum (runnum),
    m_evnum (evnum),
    m_dlb (-1),
    m_dnn (-1)
    //    m_kt_res("0.,0.,12");
{
}


int Lepjets_Event::runnum () const
//
// Purpose: Return the run number.
//
// Returns:
//   The run number.
//
{
  return m_runnum;
}


int Lepjets_Event::evnum () const
//
// Purpose: Return the event number.
//
// Returns:
//   The event number.
//
{
  return m_evnum;
}


int Lepjets_Event::nleps () const
//
// Purpose: Return the length of the lepton list.
//
// Returns:
//   The length of the lepton list.
//
{
  return m_leps.size ();
}


int Lepjets_Event::njets () const
//
// Purpose: Return the length of the jet list.
//
// Returns:
//   The length of the jet list.
//
{
  return m_jets.size ();
}


Lepjets_Event_Lep& Lepjets_Event::lep (int i)
//
// Purpose: Return the Ith lepton.
//
// Inputs:
//   i -           The lepton index (counting from 0).
//
// Returns:
//   The Ith lepton.
//
{
  assert (i >= 0 && (size_t)i < m_leps.size());
  return m_leps[i];
}


Lepjets_Event_Jet& Lepjets_Event::jet (int i)
//
// Purpose: Return the Ith jet.
//
// Inputs:
//   i -           The jet index (counting from 0).
//
// Returns:
//   The Ith jet.
//
{
  assert (i >= 0 && (size_t)i < m_jets.size());
  return m_jets[i];
}


const Lepjets_Event_Lep& Lepjets_Event::lep (int i) const
//
// Purpose: Return the Ith lepton.
//
// Inputs:
//   i -           The lepton index (counting from 0).
//
// Returns:
//   The Ith lepton.
//
{
  assert (i >= 0 && (size_t)i < m_leps.size());
  return m_leps[i];
}


const Lepjets_Event_Jet& Lepjets_Event::jet (int i) const
//
// Purpose: Return the Ith jet.
//
// Inputs:
//   i -           The jet index (counting from 0).
//
// Returns:
//   The Ith jet.
//
{
  assert (i >= 0 && (size_t)i < m_jets.size());
  return m_jets[i];
}


Fourvec& Lepjets_Event::met ()
//
// Purpose: Return the missing Et.
//
// Returns:
//   The missing Et.
//
{
  return m_met;
}


const Fourvec& Lepjets_Event::met () const
//
// Purpose: Return the missing Et.
//
// Returns:
//   The missing Et.
//
{
  return m_met;
}


Resolution& Lepjets_Event::kt_res ()
//
// Purpose: Return the kt resolution.
//
// Returns:
//   The kt resolution.
//
{
  return m_kt_res;
}


const Resolution& Lepjets_Event::kt_res () const
//
// Purpose: Return the kt resolution.
//
// Returns:
//   The kt resolution.
//
{
  return m_kt_res;
}


double Lepjets_Event::zvertex () const
//
// Purpose: Return the z-vertex.
//
// Returns:
//   The z-vertex.
//
{
  return m_zvertex;
}


double& Lepjets_Event::zvertex ()
//
// Purpose: Return the z-vertex.
//
// Returns:
//   The z-vertex.
//
{
  return m_zvertex;
}


bool Lepjets_Event::isMC () const
//
// Purpose: Return the isMC flag.
//
// Returns:
//   The isMC flag.
//
{
  return m_isMC;
}


void Lepjets_Event::setMC (bool isMC)
//
// Purpose: set isMC flag.
//
// Returns:
//   nothing
//
{
  m_isMC = isMC;
}


float Lepjets_Event::dlb () const
//
// Purpose: Return the LB discriminant.
//
// Returns:
//   The LB discriminant.
//
{
  return m_dlb;
}


float& Lepjets_Event::dlb ()
//
// Purpose: Return the LB discriminant.
//
// Returns:
//   The LB discriminant.
//
{
  return m_dlb;
}


float Lepjets_Event::dnn () const
//
// Purpose: Return the NN discriminant.
//
// Returns:
//   The NN discriminant.
//
{
  return m_dnn;
}


float& Lepjets_Event::dnn ()
//
// Purpose: Return the NN discriminant.
//
// Returns:
//   The NN discriminant.
//
{
  return m_dnn;
}


Fourvec Lepjets_Event::sum (int type) const
//
// Purpose: Sum all objects with type code TYPE.
//
// Inputs:
//   type -        The type code to match.
//
// Returns:
//   The sum of all objects with type code TYPE.
//
{
  Fourvec out;
  for (size_t i=0; i < m_leps.size(); i++)
    if (m_leps[i].type() == type)
      out += m_leps[i].p();
  for (size_t i=0; i < m_jets.size(); i++)
    if (m_jets[i].type() == type)
      out += m_jets[i].p();
  return out;
}


Fourvec Lepjets_Event::kt () const
//
// Purpose: Calculate kt --- sum of all objects plus missing Et.
//
// Returns:
//   The event kt.
{
  Fourvec v = m_met;
  for (size_t i=0; i < m_leps.size(); i++)
    v += m_leps[i].p();
  for (size_t i=0; i < m_jets.size(); i++)
    v += m_jets[i].p();
  return v;
}


void Lepjets_Event::add_lep (const Lepjets_Event_Lep& lep)
//
// Purpose: Add a lepton to the event.
//
// Inputs:
//   lep -         The lepton to add.
//
{
  m_leps.push_back (lep);
}


void Lepjets_Event::add_jet (const Lepjets_Event_Jet& jet)
//
// Purpose: Add a jet to the event.
//
// Inputs:
//   jet -         The jet to add.
//
{
  m_jets.push_back (jet);
}


void Lepjets_Event::smear (HepRandomEngine& engine, bool smear_dir /*= false*/)
//
// Purpose: Smear the objects in the event according to their resolutions.
//
// Inputs:
//   engine -      The underlying RNG.
//   smear_dir -   If false, smear the momentum only.
//
{
  Fourvec before, after;
  for (size_t i=0; i < m_leps.size(); i++) {
    before += m_leps[i].p();
    m_leps[i].smear (engine, smear_dir);
    after += m_leps[i].p();
  }
  for (size_t i=0; i < m_jets.size(); i++) {
    before += m_jets[i].p();
    m_jets[i].smear (engine, smear_dir);
    after += m_jets[i].p();
  }

  Fourvec kt = m_met + before;
  kt(Fourvec::X) = m_kt_res.pick (kt(Fourvec::X), kt(Fourvec::X), engine);
  kt(Fourvec::Y) = m_kt_res.pick (kt(Fourvec::Y), kt(Fourvec::Y), engine);
  m_met = kt - after;
}


void Lepjets_Event::sort ()
//
// Purpose: Sort the objects in the event in order of descending pt.
//
{
  std::sort (m_leps.begin(), m_leps.end(), not2 (less<Lepjets_Event_Lep> ()));
  std::sort (m_jets.begin(), m_jets.end(), not2 (less<Lepjets_Event_Lep> ()));
}


namespace {


struct Lepjets_Event_Cutter
//
// Purpose: Helper for cutting on objects.
//
{
  Lepjets_Event_Cutter (double pt_cut, double eta_cut)
    : m_pt_cut (pt_cut), m_eta_cut (eta_cut)
  {}
  bool operator() (const Lepjets_Event_Lep& l) const;
  double m_pt_cut;
  double m_eta_cut;
};


bool Lepjets_Event_Cutter::operator () (const Lepjets_Event_Lep& l) const
//
// Purpose: Object cut evaluator.
//
{
  return ! (l.p().perp() > m_pt_cut && abs (l.p().pseudoRapidity()) < m_eta_cut);
}


} // unnamed namespace


int Lepjets_Event::cut_leps (double pt_cut, double eta_cut)
//
// Purpose: Remove all leptons failing the pt and eta cuts.
//
// Inputs:
//   pt_cut -      Pt cut.  Remove objects with pt less than this.
//   eta_cut -     Eta cut.  Remove objects with abs(eta) larger than this.
//
// Returns:
//   The number of leptons remaining after the cuts.
//
{
  m_leps.erase (remove_if (m_leps.begin(), m_leps.end(),
                          Lepjets_Event_Cutter (pt_cut, eta_cut)),
               m_leps.end ());
  return m_leps.size ();
}


int Lepjets_Event::cut_jets (double pt_cut, double eta_cut)
//
// Purpose: Remove all jets failing the pt and eta cuts.
//
// Inputs:
//   pt_cut -      Pt cut.  Remove objects with pt less than this.
//   eta_cut -     Eta cut.  Remove objects with abs(eta) larger than this.
//
// Returns:
//   The number of jets remaining after the cuts.
//
{
  m_jets.erase (remove_if (m_jets.begin(), m_jets.end(),
                          Lepjets_Event_Cutter (pt_cut, eta_cut)),
               m_jets.end ());
  return m_jets.size ();
}


void Lepjets_Event::trimjets (int n)
//
// Purpose: Remove all but the first N jets.
//
// Inputs:
//   n -           The number of jets to keep.
//
{
  if ((size_t)n >= m_jets.size())
    return;
  m_jets.erase (m_jets.begin() + n, m_jets.end());
}


std::ostream& Lepjets_Event::dump (std::ostream& s, bool full /*=false*/) const
//
// Purpose: Dump out this object.
//
// Inputs:
//   s -           The stream to which to write.
//   full -        If true, dump all information for this object.
//
// Returns:
//   The stream S.
//
{
  s << "Run: " << m_runnum << "  Event: " << m_evnum << "\n";
  s << "Leptons:\n";
  for (size_t i=0; i < m_leps.size(); i++) {
    s << "  ";
    m_leps[i].dump (s, full);
    s << "\n";
  }
  s << "Jets:\n";
  for (size_t i=0; i < m_jets.size(); i++) {
    s << "  ";
    m_jets[i].dump (s, full);
    s << "\n";
  }
  s << "Missing Et: " << m_met << "\n";
  if (m_zvertex != 0)
    s << "z-vertex: " << m_zvertex << "\n";
  return s;
}


std::ostream& operator<< (std::ostream& s, const Lepjets_Event& ev)
//
// Purpose: Dump out this object.
//
// Inputs:
//   s -           The stream to which to write.
//   ev -          The object to dump.
//
// Returns:
//   The stream S.
//
{
  return ev.dump (s);
}


} // namespace hitfit





