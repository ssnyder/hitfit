//
// $Id: Run2_Partoncorr.cpp,v 1.2 2007-07-03 16:19:52 erik Exp $
//
// File: src/Run2_Partoncorr.cpp
// Purpose: Apply run 2 parton level corrections.
// Created: May, 2003, mulders, based on Scott's Run1 jet correction code.
//


#include "hitfit/Run2_Partoncorr.hpp"
#include "hitfit/Defaults.hpp"
#include "hitfit/Lepjets_Event.hpp"
#include "hitfit/Constrained_Top.hpp"
#include "hitfit/Vector_Resolution.hpp"
#include <vector>
#include <cassert>
#include <cmath>
#include <iostream>


using std::abs;
using std::sqrt;
using std::vector;
using std::cout;


//************************************************************************
// Declare fortran routines that we call.
//
/*
extern "C" {
  float fcor_r5j_ (const int& ityp,   const float& engj,
                   const float& etaj, const float& zver,
                   const float& emut, const float& edep,
			 float& eres,       float& elep,
                   const int&   corrtyp);

  float fcor_r5j_met_ (float met[2],      const float& thetaj,
                       const float& phij, const float& elep,
                       const float& edep, int& ier);

  float scale_match_ (const int& is_data,
                      const float& etaj,
                      const float& zver);
  float scale_cor_ (const int& is_data, const float& etaj,
                    const float& zver);
}

*/
namespace hitfit {


//************************************************************************
// Argument handling.
//


Run2_Partoncorr_Args::Run2_Partoncorr_Args (const Defaults& defs)
//
// Purpose: Constructor.
//
// Inputs:
//   defs -        The Defaults instance from which to initialize.
//
  : m_jet_mass_thresh (defs.get_float ("run2_jet_mass_thresh")),
    m_lrespar0 (defs.get_float ("run2_lrespar0")),
    m_lrespar1 (defs.get_float ("run2_lrespar1")),
    m_lrespar2 (defs.get_float ("run2_lrespar2")),
    m_fh_scale_data_to_mc (defs.get_bool ("run2_fh_scale_data_to_mc")),
    m_fh_scale_data (defs.get_bool ("run2_fh_scale_data")),
    m_fh_scale_mc (defs.get_bool ("run2_fh_scale_mc")),
    m_fh_scale_corr_met (defs.get_bool ("run2_fh_scale_corr_met"))
{
  // Get the eta-dependent electron resolutions.
  int i = 1;
  for (;;) {
    char buf[64];
    sprintf (buf, "run2_etadep_electron_eta%d", i);
    if (!defs.exists (buf))
      break;
    double etamax = defs.get_float (buf);
    sprintf (buf, "run2_etadep_electron_res%d", i);
    m_etadep_electronres.push_back (Etadep_Jetres (etamax, defs.get_string (buf)));
    ++i;
  }
  // Get the eta-dependent muon resolutions.
  i = 1;
  for (;;) {
    char buf[64];
    sprintf (buf, "run2_etadep_muon_eta%d", i);
    if (!defs.exists (buf))
      break;
    double etamax = defs.get_float (buf);
    sprintf (buf, "run2_etadep_muon_res%d", i);
    m_etadep_muonres.push_back (Etadep_Jetres (etamax, defs.get_string (buf)));
    ++i;
  }
  // Get the eta-dependent jet resolutions.
  i = 1;
  for (;;) {
    char buf[64];
    sprintf (buf, "run2_etadep_jet_eta%d", i);
    if (!defs.exists (buf))
      break;
    double etamax = defs.get_float (buf);
    sprintf (buf, "run2_etadep_jet_res%d", i);
    m_etadep_jetres.push_back (Etadep_Jetres (etamax, defs.get_string (buf)));
    ++i;
  }
  // Get the eta-dependent parton level corrections for light and heavy jets.
  i = 1;
  for (;;) {
    char buf[64];
    sprintf (buf, "run2_etadep_scale_eta%d", i);
    if (!defs.exists (buf))
      break;
    m_flavour_scale_etabins.push_back (defs.get_float (buf));
    sprintf (buf, "run2_light_etadep_scale%d", i);
    m_lightquark_scale_pars.push_back ( Scale_Pars (defs.get_string (buf)));
    sprintf (buf, "run2_heavy_etadep_scale%d", i);
    m_heavyquark_scale_pars.push_back ( Scale_Pars (defs.get_string (buf)));
    ++i;
  }
}


const Vector_Resolution&
Run2_Partoncorr_Args::find_etadep_muonres (double eta) const
//
// Purpose: Return the eta-dependent muon resolution for ETA.
//
// Inputs:
//   eta -         The muon pseudorapidity.
//
// Returns:
//   The eta-dependent muon resolution for ETA.
//
{
  assert (m_etadep_muonres.size() > 0);
  size_t i;
  eta = abs (eta);
  for (i=0; i < m_etadep_muonres.size()-1; i++)
    if (eta < m_etadep_muonres[i].etamax)
      break;
  return m_etadep_muonres[i].res;
}

const Vector_Resolution&
Run2_Partoncorr_Args::find_etadep_electronres (double eta) const
//
// Purpose: Return the eta-dependent electron resolution for ETA.
//
// Inputs:
//   eta -         The electron pseudorapidity.
//
// Returns:
//   The eta-dependent electron resolution for ETA.
//
{
  assert (m_etadep_electronres.size() > 0);
  size_t i;
  eta = abs (eta);
  for (i=0; i < m_etadep_electronres.size()-1; i++)
    if (eta < m_etadep_electronres[i].etamax)
      break;
  return m_etadep_electronres[i].res;
}

const Vector_Resolution&
Run2_Partoncorr_Args::find_etadep_jetres (double eta) const
//
// Purpose: Return the eta-dependent jet resolution for ETA.
//
// Inputs:
//   eta -         The jet pseudorapidity.
//
// Returns:
//   The eta-dependent jet resolution for ETA.
//
{
  assert (m_etadep_jetres.size() > 0);
  size_t i;
  eta = abs (eta);
  for (i=0; i < m_etadep_jetres.size()-1; i++)
    if (eta < m_etadep_jetres[i].etamax)
      break;
  return m_etadep_jetres[i].res;
}


double Run2_Partoncorr_Args::jet_mass_thresh () const
//
// Purpose: Return the jet_mass_thresh parameter.
//          See the header for documentation.
//
{
  return m_jet_mass_thresh;
}


double Run2_Partoncorr_Args::lrespar0 () const
//
// Purpose: Return the lrespar0 parameter.
//          See the header for documentation.
//
{
  return m_lrespar0;
}


double Run2_Partoncorr_Args::lrespar1 () const
//
// Purpose: Return the lrespar1 parameter.
//          See the header for documentation.
//
{
  return m_lrespar1;
}


double Run2_Partoncorr_Args::lrespar2 () const
//
// Purpose: Return the lrespar2 parameter.
//          See the header for documentation.
//
{
  return m_lrespar2;
}


bool Run2_Partoncorr_Args::fh_scale_data_to_mc () const
//
// Purpose: Return the fh_scale_data_to_mc parameter.
//          See the header for documentation.
//
{
  return m_fh_scale_data_to_mc;
}


bool Run2_Partoncorr_Args::fh_scale_data () const
//
// Purpose: Return the fh_scale_data parameter.
//          See the header for documentation.
//
{
  return m_fh_scale_data;
}


bool Run2_Partoncorr_Args::fh_scale_mc () const
//
// Purpose: Return the fh_scale_mc parameter.
//          See the header for documentation.
//
{
  return m_fh_scale_mc;
}


bool Run2_Partoncorr_Args::fh_scale_corr_met () const
//
// Purpose: Return the fh_scale_corr_met parameter.
//          See the header for documentation.
//
{
  return m_fh_scale_corr_met;
}

double Run2_Partoncorr_Args::GetPartonCorr (
                     int   typej, 
                     double ejet, 
                     double etaj) const
//
// Purpose: Apply run-2 parton level corrections.
//
// Inputs:
//   typej -       The type of jet
//   ejet -        The energy of the jet
//   etaj -      The (physics?) eta of the jet
//
// Outputs:
//   - - - -       Return value is scaling factor E_corrected / E_original
//
{
  // First determine bin of eta:
  assert (m_flavour_scale_etabins.size() > 0);
  size_t i;
  double p0,p1,p2;
  //  std::cout << "ejet is " << ejet << "\n" ;
  //std::cout << "etaj is " << etaj ;
  etaj = abs (etaj);
  for (i=0; i < m_flavour_scale_etabins.size()-1; i++)
    if (etaj < m_flavour_scale_etabins[i])
      break;
  //    if(i==0)
  //  {std::cout << "Central Detector! ";}
  //else if(i==1)
  //  {std::cout << "ICR!! ";}
  //else if(i==2)
  //  {std::cout << "ENDCAP !! ";}
  //else {std::cout << "Ohoh i is equal to " << i ;}

  if(typej == 11 || typej == 12)
    {
      // heavy quark jet !!
      //std::cout << "heavy \n";
      p0 = m_heavyquark_scale_pars [i].m_p0;
      p1 = m_heavyquark_scale_pars [i].m_p1;
      p2 = m_heavyquark_scale_pars [i].m_p2;
      //cout << "\n Using " << p0 << " " << p1 << " " << p2 << "\n";
    }
  else
    {
      // light quark jet, or ISR/FSR (let's treat them as light-quark too..)
      //std::cout << " light \n";
     p0 = m_lightquark_scale_pars [i].m_p0;
     p1 = m_lightquark_scale_pars [i].m_p1;
     p2 = m_lightquark_scale_pars [i].m_p2;
     //cout << "\n Using " << p0 << " " << p1 << " " << p2 << "\n";
    }

  double D = p1*p1 - 4.*p2*(p0-ejet);
  //assert (D > 0.);
  if(D < 0.) D=0.;

  double F = (-p1+sqrt(D))/(2.*p2*ejet);
  //  assert (F > 0.);
  if(F<0.) F=1.E-10;

  return F;
}





//************************************************************************
// Helper functions.
//

namespace {


#if 0
void fix_imag_jets (Lepjets_Event& ev, double jet_mass_thresh)
//
// Purpose: If a jet has a large mass, rescale the 3-momentum to match
//          the energy.  This to work around the cafix imaginary jet mass bug.
//
// Inputs:
//   ev -          The event to process.
//   jet_mass_thresh - Rescaling threshold.
//
//
{
  if (jet_mass_thresh <= 0)
    return;

  double msq_thresh = jet_mass_thresh * jet_mass_thresh;
  for (int i=0; i<ev.njets(); i++) {
    double m2 = ev.jet(i).p().m2 ();
    if (m2 > msq_thresh) {
      double scale = ev.jet(i).p().e() / ev.jet(i).p().mag();
      ev.jet(i).p()(Fourvec::X) *= scale;
      ev.jet(i).p()(Fourvec::Y) *= scale;
      ev.jet(i).p()(Fourvec::Z) *= scale;
    }
  }
}
#endif

/*
double correct_type (Fourvec& jet,
                     Fourvec& met,
                     const Fourvec& tag_lep,
                     double edep,
                     int jet_type,
                     double zvert)
//
// Purpose: Apply Frank's flavor-dependent jet corrections to a jet.
//
// Inputs:
//   jet -         The jet to correct.
//   met -         The current missing Et in the event.
//   tag_lap -     The SLT lepton, if jet_type == 3.
//   edep -        The SLT lepton's calorimeter energy deposition,
//                 if jet_type == 3.
//   jet_type -    1 -> light quark jet.
//                 2 -> untagged b-jet.
//                 3 -> tagged b-jet.
//   zvert -       The z-vertex of the event.
//
// Outputs:
//   jet -         The corrected jet.
//   met -         The updated missing Et.
//
// Returns:
//   The energy of the jet, excluding tagging leptons.
//   
{
  float ejet = jet.e();
  float etaj = jet.pseudoRapidity();
  float etag = tag_lep.e();
  float eres;
  float elep;
  int vers = 1;

  // Call Frank's routine.
  float rat = fcor_r5j_ (jet_type, ejet, etaj, zvert,
                         etag,     edep, eres, elep,
                         vers);

  // If the jet was tagged, we also need to correct the missing Et.
  if (jet_type == 3) {
    float thetaj = jet.theta();
    float phij   = jet.phi();
    int ier;
    float metdum[2];
    met = met + tag_lep;
    metdum[0] = met(1);
    metdum[1] = met(2);
    fcor_r5j_met_ (metdum, thetaj, phij, elep, edep, ier);
    assert (ier == 0);
    met(1) = metdum[0];
    met(2) = metdum[1];
    adjust_e_for_mass (met, 0);
  }

  // Adjust the jet.
  jet *= rat;

  if (elep > 0)
    return jet.e() - elep + edep;
  return jet.e();
}


void flfudge (Lepjets_Event& ev, vector<double>& enolep)
//
// Purpose: Apply Frank's flavor-dependent corrections to all jets
//          in an event.
//
// Inputs:
//   ev -          The event to correct.
//
// Outputs:
//   enolep -      Vector of jet energies excluding tag leptons
//                 (for the resolution calculation).
//
{
  for (int i=0; i < ev.njets(); i++) {
    int typ = 1;
    if (ev.jet(i).slt_tag())
      typ = 3;
    else if (ev.jet(i).type() == hadb_label ||
             ev.jet(i).type() == lepb_label ||
             ev.jet(i).type() == higgs_label)
      typ = 2;

    enolep.push_back (correct_type (ev.jet(i).p(),
                                    ev.met(),
                                    ev.jet(i).tag_lep(),
                                    ev.jet(i).slt_edep(),
                                    typ,
                                    ev.zvertex()));
  }
}
*/
#if 0
double Dennis_eta(const Fourvec& p, double vtx_z)

{

  // Try barrel surface first:

  double m_cft_zhalf = 126.0;
  double m_cft_r = 51.7290;

  double z = vtx_z + m_cft_r * p.pz() / p.perp();
  double r = m_cft_r;

  if( z > m_cft_zhalf ) {
    z = m_cft_zhalf;
    r = ( m_cft_zhalf - vtx_z ) * p.perp() / p.pz();
  }

  if( z < - m_cft_zhalf ) {
    z = -m_cft_zhalf;
    r = ( - m_cft_zhalf - vtx_z ) * p.perp() / p.pz();
  }

  const double cos_dtheta = z / sqrt( z * z + r * r );

  const double det_eta =
    std::log( ( 1. + cos_dtheta ) / ( 1. - cos_dtheta ) ) / 2.;

  return det_eta;
}
#endif


const Vector_Resolution& find_etadep_jet_res (const Run2_Partoncorr_Args& args,
                                          const Fourvec& p,
                                          double zvertex)
//
// Purpose: Find the proper eta-dependent resolution for a jet.
//
// Inputs:
//   args -        The parameter settings.
//   p -           The 4-momentum of the jet.
//   zvertex -     The z-vertex of the event.
{
  return args.find_etadep_jetres (deteta (p, zvertex));
}

const Vector_Resolution& find_etadep_muon_res (const Run2_Partoncorr_Args& args,
                                          const Fourvec& p,
                                          double zvertex)
//
// Purpose: Find the proper eta-dependent resolution for a lepton.
//
// Inputs:
//   args -        The parameter settings.
//   p -           The 4-momentum of the lepton.
//   zvertex -     The z-vertex of the event.
{
  return args.find_etadep_muonres (deteta (p, zvertex, 51.729, 126.0));
}

const Vector_Resolution& find_etadep_electron_res (const Run2_Partoncorr_Args& args,
                                          const Fourvec& p,
                                          double zvertex)
//
// Purpose: Find the proper eta-dependent resolution for a lepton.
//
// Inputs:
//   args -        The parameter settings.
//   p -           The 4-momentum of the lepton.
//   zvertex -     The z-vertex of the event.
{
  return args.find_etadep_electronres (deteta (p, zvertex));
}


void do_etadep_jet_res (const Run2_Partoncorr_Args& args,
                        Lepjets_Event& ev)
			//    , const vector<double>& enolep)
//
// Purpose: Assign eta-dependent jet resolutions to the jets.
//
// Inputs:
//   args -        The parameter settings.
//   ev -          The event on which to operate.
//   enolep -      Vector of jet energies excluding tag leptons.
//
// Outputs:
//   ev -          The updated event.
//
{
  for (int i=0; i < ev.njets(); i++) {
    // Look up the resolution.
    Lepjets_Event_Jet& jet = ev.jet(i);
    const Vector_Resolution& thisres = find_etadep_jet_res (args,
                                                        jet.p(), ev.zvertex());
    double eres;

    //    if (jet.slt_tag()) {
    //  // Special handling for SLT case.
    //  // From Frank's code.
    //  Fourvec jtmp = jet.p();
    //  if (enolep[i] > 0)
    //	jtmp *= (enolep[i] / jet.p().e());
    //  eres = thisres.p_sigma (jtmp);
    //  double emut = jet.tag_lep ().e();
    //  double lres2 = exp (args.lrespar0() + args.lrespar1() / emut) 
    //    + args.lrespar2();
    //  eres = sqrt (eres*eres + lres2);
    // }
    //else
    eres = thisres.p_sigma (jet.p());

    double phires = thisres.phi_sigma (jet.p());
    double etares = thisres.eta_sigma (jet.p());

    // Install the resolution.
    jet.res() = Vector_Resolution (Resolution (eres),
                                   Resolution (etares),
                                   Resolution (phires));
  }
}


void do_etadep_lep_res (const Run2_Partoncorr_Args& args,
                        Lepjets_Event& ev)

//
// Purpose: Assign eta-dependent jet resolutions to the lepton.
//
// Inputs:
//   args -        The parameter settings.
//   ev -          The event on which to operate.
//
// Outputs:
//   ev -          The updated event.
//
{
  for (int i=0; i < ev.nleps(); i++) {
    // Look up the resolution.
    Lepjets_Event_Lep& lep = ev.lep(i);
    double eres;
    double phires;
    double etares;

    if (lep.type() == muon_label) {
      const Vector_Resolution& thisres = 
	find_etadep_muon_res (args, lep.p(), ev.zvertex());
      eres = thisres.p_sigma (lep.p());
      phires = thisres.phi_sigma (lep.p());
      etares = thisres.eta_sigma (lep.p());
    } else if (lep.type() == electron_label) {
      const Vector_Resolution& thisres = 
	find_etadep_electron_res (args, lep.p(), ev.zvertex());
      eres = thisres.p_sigma (lep.p());
      phires = thisres.phi_sigma (lep.p());
      etares = thisres.eta_sigma (lep.p());
    } else {
      assert (lep.type() == lepton_label);
      std::cout << "Do not know how to set resolution for generic lepton! Treat as electron... \n";
      const Vector_Resolution& thisres = 
	find_etadep_electron_res (args, lep.p(), ev.zvertex());
      eres = thisres.p_sigma (lep.p());
      phires = thisres.phi_sigma (lep.p());
      etares = thisres.eta_sigma (lep.p());
    }

    // Install the resolution.
    lep.res() = Vector_Resolution (Resolution (eres),
                                   Resolution (etares),
                                   Resolution (phires));
  }
}

/*

void do_scale_match (const Run2_Partoncorr_Args& args,
                     Lepjets_Event& ev)



//
// Purpose: Apply Frank's eta-dependent corrections.
//
// Inputs:
//   args -        The parameter settings.
//   ev -          The event on which to operate.
//
// Outputs:
//   ev -          The updated event.
//
{
  static bool shown = false;
  char* typ = "none";
  float zvert = ev.zvertex ();

  Fourvec tot;

  for (int i=0; i<ev.njets(); i++) {
    float fac = 1;
    float eta = ev.jet(i).p().pseudoRapidity();

    // Note that data scalings override mc scalings.
    if (args.fh_scale_data_to_mc ()) {
      fac = scale_match_ (1, eta, zvert);
      typ = "scale_data_to_mc";
    }
    else if (args.fh_scale_data ()) {
      fac = scale_cor_ (1, eta, zvert);
      typ = "scale_data";
    }
    else if (args.fh_scale_mc ()) {
      fac = scale_cor_ (0, eta, zvert);
      typ = "scale_mc";
    }

    tot += ev.jet(i).p() * (fac - 1);

    ev.jet(i).p() *= fac;
  }

  if (args.fh_scale_corr_met ()) {
    ev.met() -= tot;
    adjust_e_for_mass (ev.met(), 0);
  }

  if ( ! shown ) {
    cout << "do_scale_match: " << typ << "\n";
    shown = true;
  }

}
*/

double deltaS_scaling(bool IsMC, double DETETA){

//==== additional eta-dependent deltaS correction from Ia, 02.08.04 ====
// IsMC   = true  for Monte Carlo
//        = false for Data
// DETETA = jet detector eta

  double parMC[5] = 
    {-2.34031e-02, -3.00000e-02, 5.33800e-02, 1.75412e+00, 3.00000e-01}; 

  double parData[2][9] =
    {{0.015, -0.20,  0.09, 0.041, -0.78, 0.14, 0.050, -1.53, 0.20},
     {0.020,  0.21,  0.10, 0.056,  0.78, 0.14, 0.048,  1.53, 0.20}};

  double par[9]={0};

  double ABS_DETETA = fabs(DETETA);

  double deltaS_corr = 0;

  // ... MC correction factor ...
  if(IsMC) {
    
    if(ABS_DETETA < 1.2) 
      deltaS_corr = parMC[0] + 
	parMC[2]*exp(-0.5*pow((ABS_DETETA-parMC[3])/parMC[4],2));
    if(ABS_DETETA >= 1.2)
      deltaS_corr = parMC[0] + parMC[1]*(ABS_DETETA-1.2) + 
	parMC[2]*exp(-0.5*pow((ABS_DETETA-parMC[3])/parMC[4],2)); 

  // ... Data correction factor ...    
  } else {

    double sum1 = -0.065+0.1*exp(-0.5*pow(DETETA/1.2,2));
    if(DETETA < 0){
      for(int i=0; i<9; ++i) par[i]=parData[0][i] ;
    } else {
      for(int i=0; i<9; ++i) par[i]=parData[1][i] ;
    }
    deltaS_corr = sum1 + 
      par[0]*exp(-0.5*pow((DETETA-par[1])/par[2],2))+
      par[3]*exp(-0.5*pow((DETETA-par[4])/par[5],2))+
      par[6]*exp(-0.5*pow((DETETA-par[7])/par[8],2));
   }

  double scale_factor = 1.0 / (1.0 + deltaS_corr);

  return scale_factor;
}


//=========================================================

float relative_jes_error(float jet_pt){

//=================================================== 
//  deta-vs-MC relative JES error after parton level and 
//  eta-dependant corrections 
//====================================================
// jet_pt = jet pT, which is already corrected for 
//          JES, parton level and eta-dependant corrections
  float par[3]={30., 0.05, 0.3};

  float err = par[1];
  if(jet_pt<par[0]){
   err = par[2] + (par[1]-par[2])/par[0] * jet_pt ;
  }

  return err;

}
//=========================================================



} // unnamed namespace




void run2_partoncorr (const Run2_Partoncorr_Args& args,
                      bool do_Parton, bool do_deltaS, int deltaS_nsigma,
                      bool do_Resolutions,
                      Lepjets_Event& ev)
//
// Purpose: Apply run-2 parton level corrections.
//
// Inputs:
//   args -        The parameter settings.
//   ev -          The event on which to operate.
//
// Outputs:
//   ev -          The updated event.
//
{
  if(do_Parton) {
    for (int i=0; i < ev.njets(); i++) {
      int typej     = ev.jet(i).type();
      double ejet   = ev.jet(i).p().e();
      //double thetaj = ev.jet(i).p().theta();
      double detector_eta = deteta (ev.jet(i).p(), ev.zvertex());
      double rat  = args.GetPartonCorr(typej, ejet, detector_eta);

      ev.jet (i).p() *= rat;

      if(do_deltaS) {
	ev.jet (i).p() *= deltaS_scaling(ev.isMC(), detector_eta);

	if(deltaS_nsigma !=0 ) {
	  ev.jet(i).p() *= 1 + deltaS_nsigma * relative_jes_error(ev.jet(i).p().perp());
	} // do systematic effect on top of deltaS correction

      }  // do deltaS corrections
    }   // loop over jets
  }    // do parton level corrections 
  if(do_Resolutions){
    do_etadep_jet_res (args, ev);
    do_etadep_lep_res (args, ev);
  }
}



} // namespace hitfit

