//
// $Id: Fit_Results.cpp,v 1.2 2007-07-03 16:19:52 erik Exp $
//
// File: src/Fit_Results.cpp
// Purpose: Hold the results from kinematic fitting.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//


#include "hitfit/Fit_Results.hpp"
#include "hitfit/Fit_Result.hpp"
#include <ostream>


using std::ostream;


namespace hitfit {


Fit_Results::Fit_Results (int max_len, int n_lists)
//
// Purpose: Constructor.
//
// Inputs:
//   max_len -     The maximum length of each list.
//   n_lists -     The number of lists.
//
  : m_v (n_lists, Fit_Result_Vec (max_len))
{
}


const Fit_Result_Vec& Fit_Results::operator[] (int i) const
{
  assert (i >= 0 && (size_t)i < m_v.size());
  return m_v[i];
}


void Fit_Results::push (double chisq,
                        const Lepjets_Event& ev,
                        const Column_Vector& pullx,
                        const Column_Vector& pully,
                        double umwhad,
                        double utmass,
                        double mt,
                        double sigmt,
                        const Column_Vector& pars,
                        const Column_Vector& errs,
                        const Column_Vector& steps,
                        const Column_Vector& chiterms,			
                        const std::vector<int>& list_flags)
//
// Purpose: Add a new fit result.
//
// Inputs:
//   chisq -       The fit chisq.
//   ev -          The event kinematics.
//   pullx -       The pull quantities for the well-measured variables.
//   pully -       The pull quantities for the poorly-measured variables.
//   umwhad -      The hadronic W mass before the fit.
//   utmass -      The top quark mass before the fit.
//   mt -          The top quark mass after the fit.
//   sigmt -       The top quark mass uncertainty after the fit.
//   pars  -       The fitted parameters, such as jet-scales, but also the
//                 lambda's which turn on/off the constraints.
//   errs  -       The errors (after the fit) on fitted parameters
//   steps -       Stepsizes for the to be fitted parameters:
//                 0 means fixed, non-0 belongs to a fitted parameter 
//   chiterms -    The different individual terms in the chi^2
//   list_flags -  Vector indicating to which lists the result should
//                 be added.
//                 This vector should have a length of N_SIZE.
//                 Each element should be either 0 or 1, depending
//                 on whether or not the result should be added
//                 to the corresponding list.
//
{
  assert (list_flags.size() == m_v.size());

  Fit_Result* res = new Fit_Result (chisq, ev, pullx, pully,
                                    umwhad, utmass, mt, sigmt, 
				    pars, errs, steps, chiterms);
  res->incref ();
  for (size_t i=0; i < m_v.size(); i++) {
    if (list_flags[i])
      m_v[i].push (res);
  }
  res->decref ();
}


ostream& operator<< (ostream& s, const Fit_Results& res)
//
// Purpose: Print the object to S.
//
// Inputs:
//   s -           The stream to which to write.
//   res -         The object to write.
//
// Returns:
//   The stream S.
//
{
  for (size_t i=0; i < res.m_v.size(); i++)
    s << "List " << i << "\n" << res.m_v[i];
  return s;
}


} // namespace hitfit
