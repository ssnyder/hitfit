//
// $Id: Lepjets_Event_Jet.cpp,v 1.1 2006-10-05 08:29:21 chriss Exp $
//
// File: src/Lepjets_Event_Jet.cpp
// Purpose: Represent a `jet' in a Lepjets_Event.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//


#include "hitfit/Lepjets_Event_Jet.hpp"


namespace hitfit {


Lepjets_Event_Jet::Lepjets_Event_Jet (const Fourvec& p,
                                      int type,
                                      const Vector_Resolution& res,
                                      bool svx_tag /*= false*/,
                                      bool slt_tag /*= false*/,
                                      const Fourvec& tag_lep /*= Fourvec()*/,
                                      double slt_edep /*= 0*/)
//
// Purpose: Constructor.
//
// Inputs:
//   p -           The 4-momentum.
//   type -        The type code.
//   res -         The resolution.
//   svx_tag -     SVX tag flag.
//   slt_tag -     SLT tag flag.
//   tag_lep -     SLT lepton 4-momentum.
//   slt_edep -    SLT lepton energy deposition.
//
  : Lepjets_Event_Lep (p, type, res),
    m_svx_tag (svx_tag),
    m_slt_tag (slt_tag),
    m_tag_lep (tag_lep),
    m_slt_edep (slt_edep),
    m_e0 (p.e())
{
}


bool Lepjets_Event_Jet::svx_tag () const
//
// Purpose: Access the SVX tag flag.
//
// Returns:
//   The SVX tag flag.
//
{
  return m_svx_tag;
}


bool& Lepjets_Event_Jet::svx_tag ()
//
// Purpose: Access the SVX tag flag.
//
// Returns:
//   The SVX tag flag.
//
{
  return m_svx_tag;
}


bool Lepjets_Event_Jet::slt_tag () const
//
// Purpose: Access the SLT tag flag.
//
// Returns:
//   The SLT tag flag.
//
{
  return m_slt_tag;
}


bool& Lepjets_Event_Jet::slt_tag ()
//
// Purpose: Access the SLT tag flag.
//
// Returns:
//   The SLT tag flag.
//
{
  return m_slt_tag;
}


const Fourvec& Lepjets_Event_Jet::tag_lep () const
//
// Purpose: Access the tag lepton 4-momentum.
//
// Returns:
//   The tag lepton 4-momentum.
//
{
  return m_tag_lep;
}


Fourvec& Lepjets_Event_Jet::tag_lep ()
//
// Purpose: Access the tag lepton 4-momentum.
//
// Returns:
//   The tag lepton 4-momentum.
//
{
  return m_tag_lep;
}


double Lepjets_Event_Jet::slt_edep () const
//
// Purpose: Access the tag lepton energy deposition.
//
// Returns:
//   The tag lepton energy deposition.
//
{
  return m_slt_edep;
}


double& Lepjets_Event_Jet::slt_edep ()
//
// Purpose: Access the tag lepton energy deposition.
//
// Returns:
//   The tag lepton energy deposition.
//
{
  return m_slt_edep;
}


double Lepjets_Event_Jet::e0 () const
//
// Purpose: Access the uncorrected jet energy.
//
// Returns:
//   The uncorrected jet energy.
//
{
  return m_e0;
}


double& Lepjets_Event_Jet::e0 ()
//
// Purpose: Access the uncorrected jet energy.
//
// Returns:
//   The uncorrected jet energy.
//
{
  return m_e0;
}


std::ostream& Lepjets_Event_Jet::dump (std::ostream& s,
                                       bool full /*= false*/) const
//
// Purpose: Dump out this object.
//
// Inputs:
//   s -           The stream to which to write.
//   full -        If true, dump all information for this object.
//
// Returns:
//   The stream S.
//
{
  Lepjets_Event_Lep::dump (s, full);
  if (m_svx_tag)
    s << " (svx)";
  if (m_slt_tag)
    s << " (slt)";
  if (full) {
    if (m_slt_tag) {
      s << "    tag lep: " << m_tag_lep;
      s << " edep: " << m_slt_edep;
    }
    s << "\n";
  }
  return s;
}


std::ostream& operator<< (std::ostream& s, const Lepjets_Event_Jet& l)
//
// Purpose: Dump out this object.
//
// Inputs:
//   s -           The stream to which to write.
//   l -           The object to dump.
//
// Returns:
//   The stream S.
//
{
  return l.dump (s);
}


} // namespace hitfit
