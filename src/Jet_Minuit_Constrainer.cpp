#if 0
// $Id$
/**
 * @file Jet_Minuit_Constrainer.cpp
 * @author scott snyder <snyder@bnl.gov>
 * @date Feb, 2013
 * @brief 
 */


#include "hitfit/Jet_Minuit_Constrainer.hpp"
#include "TFitterMinuit.h"
#include <sstream>


namespace {
inline double SQ(double x) { return x*x; }
}


namespace hitfit {


Jet_Minuit_Constrainer* Jet_Minuit_Constrainer::m_self = 0;


Jet_Minuit_Constrainer::Jet_Minuit_Constrainer (int masslabel,
                                                double mass,
                                                double width,
                                                int isrlabel /*= -1*/,
                                                bool domet /*= false*/)
  : m_masslabel (masslabel),
    m_isrlabel (isrlabel),
    m_mass (mass),
    m_width2 (SQ(width)),
    m_ev(0),
    m_domet(domet)
{
}


double Jet_Minuit_Constrainer::eval (const Fourvec_Event& ev,
                                     unsigned int njet,
                                     double* jet_E,
                                     double* m,
                                     Column_Vector* pull)
{
  double chisq = 0;
  Fourvec mtot;
  Fourvec kt;
  unsigned int ijet = 0;
  int nobjs = ev.nobjs();
  for (int i=0; i < nobjs; i++) {
    const FE_Obj& obj = ev.obj(i);
    if (obj.label == m_masslabel || obj.label == m_isrlabel) {
      if (ijet >= njet) abort();
      double e_orig = obj.p.e();
      double this_pull = SQ(e_orig - jet_E[ijet]) / SQ(obj.p_error);
      chisq += this_pull;
      if (pull)
        (*pull)[ijet] = this_pull;
      
      Fourvec p = obj.p * (jet_E[ijet] / e_orig);

      if (obj.label == m_masslabel)
        mtot += p;
      kt += p;
      ++ijet;
    }
    else
      kt += obj.p;
  }

  double this_m = mtot.m();
  if (m)
    *m = this_m;
  chisq += SQ(this_m - m_mass) / m_width2;

  if (m_domet) {
    chisq += SQ( (ev.kt().px() - kt.px()) / ev.kt_x_error() );
    chisq += SQ( (ev.kt().py() - kt.py()) / ev.kt_y_error() );
  }
  
  return chisq;
}


std::vector<double> Jet_Minuit_Constrainer::get_jets (const Fourvec_Event& ev)
{
  std::vector<double> jet_E;
  int nobjs = ev.nobjs();
  for (int i=0; i < nobjs; i++) {
    const FE_Obj& obj = ev.obj(i);
    if (obj.label == m_masslabel || obj.label == m_isrlabel)
      jet_E.push_back (obj.p.e());
  }
  return jet_E;
}


int Jet_Minuit_Constrainer::fit (const Fourvec_Event& ev,
                                 std::vector<double>& jet_E,
                                 double tol)
{
  TFitterMinuit fitter (jet_E.size());
  fitter.SetFCN (fcn);
  fitter.SetPrintLevel (-1);
  m_self = this;
  m_ev = &ev;

  for (size_t i = 0; i < jet_E.size(); i++) {
    std::ostringstream ss;
    ss << "jet" << i+1;
    fitter.SetParameter (i, ss.str().c_str(), jet_E[i], jet_E[i] * 0.05, 0, 0);
  }

  double args[2];
  args[0] = 0;
  args[1] = tol;
  int ret = fitter.ExecuteCommand ("MIGRAD", args, 2);
  //printf ("xxx %d\n", ret);

  m_self = 0;
  m_ev = 0;

  for (size_t i = 0; i < jet_E.size(); i++)
    jet_E[i] = fitter.GetParameter (i);

  //fitter.PrintResults (1, 1);

  return ret;
}


void Jet_Minuit_Constrainer::fcn (int& npar,
                                  double* /*grad*/,
                                  double& f,
                                  double* par,
                                  int /*iflag*/)
{
  f = m_self->eval (*m_self->m_ev, npar, par);
}


void Jet_Minuit_Constrainer::set_jets (Fourvec_Event& ev,
                                       const std::vector<double>& jet_E)
{
  unsigned int ijet = 0;
  int nobjs = ev.nobjs();
  for (int i=0; i < nobjs; i++) {
    const FE_Obj& obj = ev.obj(i);
    if (obj.label == m_masslabel || obj.label == m_isrlabel) {
      if (ijet >= jet_E.size()) abort();
      //printf ("www set jet %d %f\n", ijet, jet_E[ijet]);
      double rat = (jet_E[ijet] / obj.p.e());
      ev.set_obj_p (i, obj.p * rat);
      ++ijet;
    }
  }
}


double Jet_Minuit_Constrainer::constrain (Fourvec_Event& ev,
                                          double* m,
                                          Column_Vector& pull,
                                          double tol)
{
  std::vector<double> jet_E = get_jets (ev);
  assert (jet_E.size() >= 2);
  fit (ev, jet_E, tol);

  if (pull.num_row() != (int)jet_E.size())
    pull = Column_Vector(jet_E.size());
  double chisq = eval (ev, jet_E.size(), &*jet_E.begin(), m, &pull);
  set_jets (ev, jet_E);
  return chisq;
}


} // namespace hitfit

#endif
