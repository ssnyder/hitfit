//
// $Id: Run1_Jetcorr.cpp,v 1.2 2007-07-03 16:19:52 erik Exp $
//
// File: src/Run1_Jetcorr.cpp
// Purpose: Apply run 1 jet corrections.
// Created: Aug, 2000, sss, based on run 1 mass analysis code.
//


#include "hitfit/Run1_Jetcorr.hpp"
#include "hitfit/Defaults.hpp"
#include "hitfit/Lepjets_Event.hpp"
#include "hitfit/Constrained_Top.hpp"
#include "hitfit/Vector_Resolution.hpp"
#include <vector>
#include <cassert>
#include <cmath>
#include <iostream>


using std::abs;
using std::sqrt;
using std::vector;
using std::cout;


//************************************************************************
// Declare fortran routines that we call.
//
/*
extern "C" {
  float fcor_r5j_ (const int& ityp,   const float& engj,
                   const float& etaj, const float& zver,
                   const float& emut, const float& edep,
			 float& eres,       float& elep,
                   const int&   corrtyp);

  float fcor_r5j_met_ (float met[2],      const float& thetaj,
                       const float& phij, const float& elep,
                       const float& edep, int& ier);

  float scale_match_ (const int& is_data,
                      const float& etaj,
                      const float& zver);
  float scale_cor_ (const int& is_data, const float& etaj,
                    const float& zver);
}

*/
namespace hitfit {


//************************************************************************
// Argument handling.
//


Run1_Jetcorr_Args::Run1_Jetcorr_Args (const Defaults& defs)
//
// Purpose: Constructor.
//
// Inputs:
//   defs -        The Defaults instance from which to initialize.
//
  : m_jet_mass_thresh (defs.get_float ("jet_mass_thresh")),
    m_lrespar0 (defs.get_float ("lrespar0")),
    m_lrespar1 (defs.get_float ("lrespar1")),
    m_lrespar2 (defs.get_float ("lrespar2")),
    m_fh_scale_data_to_mc (defs.get_bool ("fh_scale_data_to_mc")),
    m_fh_scale_data (defs.get_bool ("fh_scale_data")),
    m_fh_scale_mc (defs.get_bool ("fh_scale_mc")),
    m_fh_scale_corr_met (defs.get_bool ("fh_scale_corr_met"))
{
  // Get the eta-dependent jet resolutions.
  int i = 1;
  for (;;) {
    char buf[64];
    sprintf (buf, "etadep_eta%d", i);
    if (!defs.exists (buf))
      break;
    double etamax = defs.get_float (buf);
    sprintf (buf, "etadep_res%d", i);
    m_etadep_jetres.push_back (Etadep_Jetres (etamax, defs.get_string (buf)));
    ++i;
  }
}


const Vector_Resolution&
Run1_Jetcorr_Args::find_etadep_jetres (double eta) const
//
// Purpose: Return the eta-dependent jet resolution for ETA.
//
// Inputs:
//   eta -         The jet pseudorapidity.
//
// Returns:
//   The eta-dependent jet resolution for ETA.
//
{
  assert (m_etadep_jetres.size() > 0);
  size_t i;
  eta = abs (eta);
  for (i=0; i < m_etadep_jetres.size()-1; i++)
    if (eta < m_etadep_jetres[i].etamax)
      break;
  return m_etadep_jetres[i].res;
}


double Run1_Jetcorr_Args::jet_mass_thresh () const
//
// Purpose: Return the jet_mass_thresh parameter.
//          See the header for documentation.
//
{
  return m_jet_mass_thresh;
}


double Run1_Jetcorr_Args::lrespar0 () const
//
// Purpose: Return the lrespar0 parameter.
//          See the header for documentation.
//
{
  return m_lrespar0;
}


double Run1_Jetcorr_Args::lrespar1 () const
//
// Purpose: Return the lrespar1 parameter.
//          See the header for documentation.
//
{
  return m_lrespar1;
}


double Run1_Jetcorr_Args::lrespar2 () const
//
// Purpose: Return the lrespar2 parameter.
//          See the header for documentation.
//
{
  return m_lrespar2;
}


bool Run1_Jetcorr_Args::fh_scale_data_to_mc () const
//
// Purpose: Return the fh_scale_data_to_mc parameter.
//          See the header for documentation.
//
{
  return m_fh_scale_data_to_mc;
}


bool Run1_Jetcorr_Args::fh_scale_data () const
//
// Purpose: Return the fh_scale_data parameter.
//          See the header for documentation.
//
{
  return m_fh_scale_data;
}


bool Run1_Jetcorr_Args::fh_scale_mc () const
//
// Purpose: Return the fh_scale_mc parameter.
//          See the header for documentation.
//
{
  return m_fh_scale_mc;
}


bool Run1_Jetcorr_Args::fh_scale_corr_met () const
//
// Purpose: Return the fh_scale_corr_met parameter.
//          See the header for documentation.
//
{
  return m_fh_scale_corr_met;
}


//************************************************************************
// Helper functions.
//

namespace {


void fix_imag_jets (Lepjets_Event& ev, double jet_mass_thresh)
//
// Purpose: If a jet has a large mass, rescale the 3-momentum to match
//          the energy.  This to work around the cafix imaginary jet mass bug.
//
// Inputs:
//   ev -          The event to process.
//   jet_mass_thresh - Rescaling threshold.
//
//
{
  if (jet_mass_thresh <= 0)
    return;

  double msq_thresh = jet_mass_thresh * jet_mass_thresh;
  for (int i=0; i<ev.njets(); i++) {
    double m2 = ev.jet(i).p().m2 ();
    if (m2 > msq_thresh) {
      double scale = ev.jet(i).p().e() / ev.jet(i).p().mag();
      ev.jet(i).p()(Fourvec::X) *= scale;
      ev.jet(i).p()(Fourvec::Y) *= scale;
      ev.jet(i).p()(Fourvec::Z) *= scale;
    }
  }
}

/*
double correct_type (Fourvec& jet,
                     Fourvec& met,
                     const Fourvec& tag_lep,
                     double edep,
                     int jet_type,
                     double zvert)
//
// Purpose: Apply Frank's flavor-dependent jet corrections to a jet.
//
// Inputs:
//   jet -         The jet to correct.
//   met -         The current missing Et in the event.
//   tag_lap -     The SLT lepton, if jet_type == 3.
//   edep -        The SLT lepton's calorimeter energy deposition,
//                 if jet_type == 3.
//   jet_type -    1 -> light quark jet.
//                 2 -> untagged b-jet.
//                 3 -> tagged b-jet.
//   zvert -       The z-vertex of the event.
//
// Outputs:
//   jet -         The corrected jet.
//   met -         The updated missing Et.
//
// Returns:
//   The energy of the jet, excluding tagging leptons.
//   
{
  float ejet = jet.e();
  float etaj = jet.pseudoRapidity();
  float etag = tag_lep.e();
  float eres;
  float elep;
  int vers = 1;

  // Call Frank's routine.
  float rat = fcor_r5j_ (jet_type, ejet, etaj, zvert,
                         etag,     edep, eres, elep,
                         vers);

  // If the jet was tagged, we also need to correct the missing Et.
  if (jet_type == 3) {
    float thetaj = jet.theta();
    float phij   = jet.phi();
    int ier;
    float metdum[2];
    met = met + tag_lep;
    metdum[0] = met(1);
    metdum[1] = met(2);
    fcor_r5j_met_ (metdum, thetaj, phij, elep, edep, ier);
    assert (ier == 0);
    met(1) = metdum[0];
    met(2) = metdum[1];
    adjust_e_for_mass (met, 0);
  }

  // Adjust the jet.
  jet *= rat;

  if (elep > 0)
    return jet.e() - elep + edep;
  return jet.e();
}


void flfudge (Lepjets_Event& ev, vector<double>& enolep)
//
// Purpose: Apply Frank's flavor-dependent corrections to all jets
//          in an event.
//
// Inputs:
//   ev -          The event to correct.
//
// Outputs:
//   enolep -      Vector of jet energies excluding tag leptons
//                 (for the resolution calculation).
//
{
  for (int i=0; i < ev.njets(); i++) {
    int typ = 1;
    if (ev.jet(i).slt_tag())
      typ = 3;
    else if (ev.jet(i).type() == hadb_label ||
             ev.jet(i).type() == lepb_label ||
             ev.jet(i).type() == higgs_label)
      typ = 2;

    enolep.push_back (correct_type (ev.jet(i).p(),
                                    ev.met(),
                                    ev.jet(i).tag_lep(),
                                    ev.jet(i).slt_edep(),
                                    typ,
                                    ev.zvertex()));
  }
}
*/

const Vector_Resolution& find_etadep_res (const Run1_Jetcorr_Args& args,
                                          const Fourvec& p,
                                          double zvertex)
//
// Purpose: Find the proper eta-dependent resolution for a jet.
//
// Inputs:
//   args -        The parameter settings.
//   p -           The 4-momentum of the jet.
//   zvertex -     The z-vertex of the event.
{
  return args.find_etadep_jetres (deteta (p, zvertex));
}


void do_etadep_jet_res (const Run1_Jetcorr_Args& args,
                        Lepjets_Event& ev,
                        const vector<double>& enolep)
//
// Purpose: Assign eta-dependent jet resolutions to the jets.
//
// Inputs:
//   args -        The parameter settings.
//   ev -          The event on which to operate.
//   enolep -      Vector of jet energies excluding tag leptons.
//
// Outputs:
//   ev -          The updated event.
//
{
  for (int i=0; i < ev.njets(); i++) {
    // Look up the resolution.
    Lepjets_Event_Jet& jet = ev.jet(i);
    const Vector_Resolution& thisres = find_etadep_res (args,
                                                        jet.p(), ev.zvertex());
    double eres;
    if (jet.slt_tag()) {
      // Special handling for SLT case.
      // From Frank's code.
      Fourvec jtmp = jet.p();
      if (enolep[i] > 0)
	jtmp *= (enolep[i] / jet.p().e());
      eres = thisres.p_sigma (jtmp);
      double emut = jet.tag_lep ().e();
      double lres2 = exp (args.lrespar0() + args.lrespar1() / emut) 
        + args.lrespar2();
      eres = sqrt (eres*eres + lres2);
    }
    else
      eres = thisres.p_sigma (jet.p());

    double phires = thisres.phi_sigma (jet.p());
    double etares = thisres.eta_sigma (jet.p());

    // Install the resolution.
    jet.res() = Vector_Resolution (Resolution (eres),
                                   Resolution (etares),
                                   Resolution (phires));
  }
}

/*
void do_scale_match (const Run1_Jetcorr_Args& args,
                     Lepjets_Event& ev)
//
// Purpose: Apply Frank's eta-dependent corrections.
//
// Inputs:
//   args -        The parameter settings.
//   ev -          The event on which to operate.
//
// Outputs:
//   ev -          The updated event.
//
{
  static bool shown = false;
  char* typ = "none";
  float zvert = ev.zvertex ();

  Fourvec tot;

  for (int i=0; i<ev.njets(); i++) {
    float fac = 1;
    float eta = ev.jet(i).p().pseudoRapidity();

    // Note that data scalings override mc scalings.
    if (args.fh_scale_data_to_mc ()) {
      fac = scale_match_ (1, eta, zvert);
      typ = "scale_data_to_mc";
    }
    else if (args.fh_scale_data ()) {
      fac = scale_cor_ (1, eta, zvert);
      typ = "scale_data";
    }
    else if (args.fh_scale_mc ()) {
      fac = scale_cor_ (0, eta, zvert);
      typ = "scale_mc";
    }

    tot += ev.jet(i).p() * (fac - 1);

    ev.jet(i).p() *= fac;
  }

  if (args.fh_scale_corr_met ()) {
    ev.met() -= tot;
    adjust_e_for_mass (ev.met(), 0);
  }

  if ( ! shown ) {
    cout << "do_scale_match: " << typ << "\n";
    shown = true;
  }
}
*/

} // unnamed namespace


void run1_jetcorr (const Run1_Jetcorr_Args& args,
                   Lepjets_Event& ev)
//
// Purpose: Apply run-1 jet corrections.
//
// Inputs:
//   args -        The parameter settings.
//   ev -          The event on which to operate.
//
// Outputs:
//   ev -          The updated event.
//
{
  fix_imag_jets (ev, args.jet_mass_thresh());
  vector<double> enolep;
//  flfudge (ev, enolep);
  do_etadep_jet_res (args, ev, enolep);
//  do_scale_match (args, ev);
}



} // namespace hitfit

