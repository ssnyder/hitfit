//
// $Id: Fit_Result_Vec.cpp,v 1.2 2009-02-12 10:36:03 chriss Exp $
//
// File: src/Fit_Result_Vec.cpp
// Purpose: Hold a set of Fit_Result structures.
// Created: Jul, 2000, sss, based on run 1 mass analysis code.
//

#include "hitfit/Fit_Result_Vec.hpp"
#include "hitfit/Fit_Result.hpp"
#include <cassert>
#include <ostream>
#include <algorithm>


using std::ostream;
using std::vector;
using std::lower_bound;


namespace hitfit {


Fit_Result_Vec::Fit_Result_Vec (int max_len)
//
// Purpose Constructor.
//
// Inputs:
//   max_len -     The maximum length of the vector.
//
  : m_max_len (max_len)
{
  assert (max_len > 0);
  m_v.reserve (max_len + 1);
}


Fit_Result_Vec::Fit_Result_Vec (const Fit_Result_Vec& vec)
//
// Purpose: Copy constructor.
//
// Inputs:
//   vec -         The vector to copy.
//
  : m_v (vec.m_v),
    m_max_len (vec.m_max_len)
{
  // Gotta increase the reference count on the contents.
  for (size_t i=0; i < m_v.size(); i++)
    m_v[i]->incref ();
}


Fit_Result_Vec::~Fit_Result_Vec ()
//
// Purpose: Destructor.
//
{
  for (size_t i=0; i < m_v.size(); i++)
    m_v[i]->decref ();
}


Fit_Result_Vec& Fit_Result_Vec::operator= (const Fit_Result_Vec& vec)
//
// Purpose: Assignment.
//
// Inputs:
//   vec -         The vector to copy.
//
// Returns:
//   This object.
//
{
  for (size_t i=0; i < m_v.size(); i++)
    m_v[i]->decref ();
  m_v = vec.m_v;
  m_max_len = vec.m_max_len;
  for (size_t i=0; i < m_v.size(); i++)
    m_v[i]->incref ();
  return *this;
}


int Fit_Result_Vec::size () const
//
// Purpose: Get back the number of results in the vector.
//
// Returns:
//   The number of results in the vector.
//
{
  return m_v.size ();
}


const Fit_Result& Fit_Result_Vec::operator[] (int i) const
//
// Purpose: Get back the Ith result in the vector.
//
// Inputs:
//   i -           The index of the desired result.
//
// Returns:
//   The Ith result.
//
{
  assert (i >= 0 && i < (int) m_v.size());
  return *m_v[i];
}


namespace {


struct Compare_Fitresptr
//
// Purpose: Helper for push().
//
{
  bool operator() (const Fit_Result* a, const Fit_Result* b) const
  {
    return *a < *b;
  }
};


} // unnamed namespace


void Fit_Result_Vec::push (Fit_Result* res)
//
// Purpose: Add a new result to the vector.
//
// Inputs:
//   res -         The result to add.
//
{
  // Find where to add it.
  vector<Fit_Result*>::iterator it = lower_bound (m_v.begin(),
                                                  m_v.end(),
                                                  res,
                                                  Compare_Fitresptr());

  // Insert it.
  m_v.insert (it, res);
  res->incref ();

  // Knock off the guy at the end if we've exceeded our maximum size.
  if ((int) m_v.size() > m_max_len) {
    m_v.back()->decref ();
    m_v.erase (m_v.end()-1);
  }
}


ostream& operator<< (ostream& s, const Fit_Result_Vec& resvec)
//
// Purpose: Print the object to S.
//
// Inputs:
//   s -           The stream to which to write.
//   resvec -      The object to write.
//
// Returns:
//   The stream S.
//
{
  for (size_t i=0; i < resvec.m_v.size(); i++)
    s << "Entry " << i << "\n" << *resvec.m_v[i];
  return s;
}


} // namespace hitfit
